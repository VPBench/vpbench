# A Benchmark Generator Framework for Evolving Variant-Rich Software #

Materials for the research paper: A Benchmark Generator Framework for Evolving Variant-Rich Software

### What is contained? ###

* Implementation - code: 
	* vpbench - benchmarkgenerator
		* contains .jar-files in lib/:
			* rits.cloning.edited.jar: Extended version from https://github.com/kostaskougios/cloning/
			* vp_2.12-0.1.jar: Virtual Platform (in paper referenced as Mahmood et al. (2021)) - check below
	* UnitTestScribe - unittestscribe
		* cloned from https://github.com/evoresearch/UnitTestScribe and extended from there
		* see unittestscribe.txt in code/ for information about forked version
	* Cloning library - cloning:
		* cloned from https://github.com/kostaskougios/cloning
		* see cloning.txt in code/ for information about forked version
	* Virtual Platform (Mahmood et al. (2021)):
		* available at https://bitbucket.org/easelab/vp/src/master/
		* used version at commit ID: 37ad4954a4d52885621df1c3fc5a0509e309ab21
* Evaluation Data - data:
	* RQ2: 
		* Seven generated version histories + statistics (check params.json in each folder for parametrization)
		* stored externally at: https://drive.google.com/file/d/1w5qOr3kcqUX6jtkldRkM7_b7vTU95hIJ/view?usp=sharing
	* RQ3:
		* stored in data/2023-02-24_10-42_transplantEval.zip
* Donor Systems for Transplantation - donor_repos
	* see README in donor_repos for more detailed information
* Initial Systems - initial_systems
	* see README in initial_systems for more detailed information
* Visualisation Code - visualisation

### Regarding params.json ###
The ordering of corresponding generators for probability distribution is (can be checked in code/benchmarkgenerator/src/main/scala/vp/bench/bg/RunEvaluation.scala): 

1. AddLineGenerator
2. ReplaceLineGenerator
3. DeleteLineGenerator
4. AddFeatureGenerator
5. RemoveFeatureGenerator
6. CloneRepositoryGenerator
7. CloneFeatureGenerator

### How do I get set up? ###
These instructions describe the setup, we used for conducting the experiments as reported in the paper.
Other setups (with differing tools or versions) might work, but we do not guarantee that.

Prerequisites:

- Windows 10

- Java 11

	- Originally we used openjdk 11.0.8+10
	
	- Since this version is not as easy to find anymore, we tested vpbench with openjdk 11.0.15+10, too: https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.15%2B10/OpenJDK11U-jdk_x64_windows_hotspot_11.0.15_10.msi

- Gradle 6.8.2

	- Download from: https://services.gradle.org/distributions/ 
	
	- Install as described here: https://gradle.org/install/
	
- sbt 1.4.0
	- automatically resolved by IntelliJ IDEA
- Scala 2.12.8
	- automatically resolved by sbt and IntelliJ IDEA
- .NET Framework 4.6.1
	- install together with Visual Studio

We used IntelliJ IDEA to automatically setup sbt and Scala and build vpbench:

- IntelliJ IDEA 2020.2.3 (https://www.jetbrains.com/idea/download/other.html)
	-> Install IntelliJ's Scala plugin.

We used Visual Studio for building the tool for testcase extraction (UnitTestScribe).

Setup instructions (post installation of necessary tools and programs):

1. 	git clone from https://bitbucket.org/VPBench/vpbench/src/master/ (note that the program may generate long paths, which may cause cause differences in results, if cloned and run in an already "deep" folder)

2. 	Open 'code\benchmarkgenerator\build.sbt' with IntelliJ IDEA as a new project.

3. 	Make sure, you have configured the correct project SDK.

4. 	The program should build.

Before you can execute the program, further preparation and configuration needs to be done:

5. 	Open 'code\unittestscribe\ScribeFramework.sln' as a solution in Visual Studio.

6. 	Build the project 'WM.UnitTestScribe' in Debug or Release configuration.

7. 	(Optional): Clone and preprocess the third-party initial systems JSON-java and RxJava as described (otherwise you can simply use the already preprocessed included versions).
8. 	Unzip the donor repositories or clone and preprocess them yourselves (as described).
9. 	Compile donor projects using the command 'gradle jar compileTestJava' in:

	a. 'donor_repos_unzipped\Algorithms\Algorithms'
	
	b. 'donor_repos_unzipped\LMAXDisruptor\disruptor'
	
	c. 'donor_repos_unzipped\skylot\jadx'
	
	d. 'donor_repos_unzipped\Structurizr\java'
	
10. Download additionally required third-party-tools:

	a. CLOC, Version 1.96.1: Download from https://github.com/AlDanial/cloc/releases/download/v1.96.1/cloc-1.96.1.exe
	
	b. SQLite3, Version 3.36: https://www.sqlite.org/2021/sqlite-tools-win32-x86-3360000.zip

11. Create a workingdirectory for vpbench somewhere, e.g.: at 'PathToLocalRepo\workingdirectory'.
12. Adjust absolute paths in 'code\benchmarkgenerator\src\main\scala\vp\bench\bg\TestConfigurationProvider.scala'
	- in function 'createStructurizrProj()': Initialize 'pathString' to point at your local structurizr repository, e.g., 'YourPathToRepPkg\\donor_repos_unzipped\\Structurizr\\java' (or the path you cloned and preprocessed it at yourself)
	- in function 'createDisruptorProj()': Initialize 'pathString' to point at your local disruptor repository, e.g., 'YourPathToRepPkg\\donor_repos_unzipped\\LMAXDisruptor\\disruptor' (or the path you cloned and preprocessed it at yourself)
	- in function 'createAlgorithmsProj()': Initialize 'pathString' to point at your local algorithms repository, e.g., 'YourPathToRepPkg\\donor_repos_unzipped\\Algorithms\\Algorithms' (or the path you cloned and preprocessed it at yourself)
	- in function 'createJadxProj()': Initialize 'pathString' to point at your local jadx repository, e.g., 'YourPathToRepPkg\\donor_repos_unzipped\\skylot\\jadx' (or the path you cloned and preprocessed it at yourself)

To execute the evaluation code for RQ3: main() in 'code\benchmarkgenerator\src\main\scala\vp\bench\bg\RunTransplantationEvaluation.scala':

13. Adjust paths in 'code\benchmarkgenerator\configTransplantEval.json':
	- targetPath: Some path to store the evaluation results.
	- workingDirectory: The path to the workingdirectory, created in step 11.
	- initialProjects: 
		- projectPath: Update the absolute path to point at 'initial_systems\VanillaTestbed'
	- generatorConfig:
		- unitTestScribePathString: Point at the .exe-file, created in step 6.
		- initialisationPathString: Update the absolute path to point at 'code\testcaseextractions' (the folder does not have to exist before)
		- srcmlPathString: Update the absolute path to point at 'code\unittestscribe\SrcML'
		- jdepsPathString: Point at the bin/ folder of your JDK (make sure, jdeps.exe is contained)
	- resrecConf:
		- clocPath: Point at the .exe, downloaded in step 10a.
		- sqlitePath: Point at sqlite3.exe, located in the unzipped folder, downloaded in step 10b.		
14. Run main() -> You might need to reload the project before you can execute it within IntelliJ.

To execute the evaluation code for RQ2: main() in 'code\benchmarkgenerator\src\main\scala\vp\bench\bg\RunEvaluation.scala':

15. Adjust paths in 'code\benchmarkgenerator\config.json':
	- targetPath: Some path to store the evaluation results.
	- workingDirectory: The path to the workingdirectory, created in step 11.
	- initialProjects: 
		- projectPath: Update the absolute paths to point at 'initial_systems\CalculatorRoot', 'initial_systems\JSON-javaRoot' and 'initial_systems\RxJava-Root'
	- generatorConfig:
		- unitTestScribePathString: Point at the .exe-file, created in step 6.
		- initialisationPathString: Update the absolute path to point at 'code\testcaseextractions' (the folder does not have to exist before)
		- srcmlPathString: Update the absolute path to point at 'code\unittestscribe\SrcML'
		- jdepsPathString: Point at the bin/ folder of your JDK (make sure, jdeps.exe is contained)
	- resrecConf:
		- clocPath: Point at the .exe, downloaded in step 10a.
		- sqlitePath: Point at sqlite3.exe, located in the unzipped folder, downloaded in step 10b.		
16. Choose a or b: 

	a. If you want to use the results of RQ3 to kickstart the evaluation (as done in the paper):
	
		- Adjust commonPath initialization in l. 266 in 'code\benchmarkgenerator\src\main\vp\bench\bg\RunEvaluation.scala': Point at the callability folder of your 'RunTransplantationEvaluation'-results inside your targetpath.

	b. If you do not want to perform pre-filtering of callable testcases:
		
		- Comment lines 266-282 in 'code\benchmarkgenerator\src\main\vp\bench\bg\RunEvaluation.scala'
		
		- In the same file: Switch the parameter value of 'initialiseTestcases' to true in l. 287

17. Run main() -> You might need to reload the project before you can execute it within IntelliJ.