name := "BenchmarkGenerator"

version := "0.1"

scalaVersion := "2.12.8"

resolvers += "Maven Repo" at "https://repo1.maven.org/maven2/"
resolvers += "Gradle Repo" at "https://repo.gradle.org/gradle/libs-releases-local/"
//resolvers += "Local Ivy Repository" at "file:///" + Path.userHome.absolutePath + "/.ivy2/local"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"
libraryDependencies += "com.googlecode.kiama" %% "kiama" % "1.8.0"
// libraryDependencies += "io.github.kostaskougios" % "cloning" % "1.10.3"

// https://mvnrepository.com/artifact/org.gradle/gradle-base-services
libraryDependencies += "org.gradle" % "gradle-base-services" % "6.1.1"// % "runtime"
// https://mvnrepository.com/artifact/org.gradle/gradle-core
libraryDependencies += "org.gradle" % "gradle-core" % "6.1.1"// % "provided"
// https://mvnrepository.com/artifact/org.gradle/gradle-tooling-api
libraryDependencies += "org.gradle" % "gradle-tooling-api" % "6.1.1"// % "runtime"
// https://mvnrepository.com/artifact/org.gradle/gradle-resources
libraryDependencies += "org.gradle" % "gradle-resources" % "6.1.1"// % "runtime"
// https://mvnrepository.com/artifact/org.gradle/gradle-messaging
libraryDependencies += "org.gradle" % "gradle-messaging" % "6.1.1"// % "runtime"
// https://mvnrepository.com/artifact/org.gradle/gradle-wrapper
libraryDependencies += "org.gradle" % "gradle-wrapper" % "6.1.1"// % "runtime"
// https://mvnrepository.com/artifact/com.google.guava/guava
libraryDependencies += "com.google.guava" % "guava" % "30.1-jre"
// https://mvnrepository.com/artifact/net.liftweb/lift-json
libraryDependencies += "net.liftweb" %% "lift-json" % "3.4.3"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"