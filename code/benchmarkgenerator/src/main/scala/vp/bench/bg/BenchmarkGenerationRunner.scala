package vp.bench.bg

import generators._
import Utilities.almostEqual
import se.gu.vp.model._
import se.gu.vp.operations.{RemoveFeature, _}
import java.nio.file
import java.nio.file.{Files, Path, Paths, StandardOpenOption}

import vp.bench.bg.evaluation.IterationResults
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.operations.Utilities.{extractFeatures, transformASTToList, transformFMToList}
import vp.bench.bg.transaction.exceptions.TransactionFailureException
import vp.bench.bg
import vp.bench.bg.evaluation.{IterationResults, RepositoryResults}
import vp.bench.bg.operations.AddExternalFeature
import vp.bench.bg.transaction.{TestExecCompilationTransaction, Transaction}
import vp.bench.bg.transaction.exceptions.{ASTPotentiallyCorruptException, TransactionFailureException}

import scala.util.Random
import scala.collection.mutable

// TODO: Giving BenchmarkGenerationRunner access to the targetPath basically defines the entire production to be compilation-safe from here (possibly introduce this into a subclass of the runner, which does not use CompilationSafeTransactions and thus does not need the targetpath)
class BenchmarkGenerationRunner(val rootAsset: Asset, val targetPathString: String, val operationGenerators: List[Generator], val compilationChecker: CompilationChecker, config: Option[Map[Generator,Double]] = None) {
  val targetPath = file.Paths.get(targetPathString)
  val rootName = rootAsset.name.split("\\\\").last

  // setup probability distribution to identify which generator will take action -> might need to be refactored to include parametrization issues
  var probThresholds = config match {
    case None =>
      val uniformProb = 1.0 / operationGenerators.length
      val probs = uniformList(uniformProb, operationGenerators.length)
      val thresholds = probs.scan(0.0)(_ + _)
      val tuples = operationGenerators zip thresholds
      val map :mutable.Map[Generator,Double] = mutable.Map[Generator,Double]()
      for((op,probThresh) <- tuples)
      { map += (op -> probThresh) }
      map
    case Some(map) =>
      // If config is set, all Generators need to be given probabilities
      assert(map.keySet.toList.sortWith(_.getClass.getName < _.getClass.getName) == operationGenerators.sortWith(_.getClass.getName < _.getClass.getName))
      // If config is set, all probabilities summed together need to equal 1
      assert(almostEqual(map.toSeq.map(tuple => tuple._2).sum, 1, 0.001))

      val threshMap :mutable.Map[Generator,Double] = mutable.Map[Generator,Double]()
      var currentThreshold = 0.0
      for {
        gen <- operationGenerators
        if map(gen) > 0
      } yield {
        threshMap += (gen -> currentThreshold)
        currentThreshold += map(gen)
      }
      threshMap
  }

  def uniformList[T](element: T, count:Int) : List[T] = {
    require(count >= 0)
    count match {
      case 0 => Nil
      case _ => element :: uniformList[T](element, count - 1)
    }
  }

  // use traits to uniformly access devOperations? -> cant since its companion objects -> use generators instead

  def run(numIterations: Int, targetPath: String, exitCondition: Option[Asset => Boolean] = None, maxAttempts: Int = 50, currentIteration: Int = 0): List[(IterationResults,Thread)] = {
    println(s"------------------------------------ Iteration $currentIteration ----------------------------------------------")

    val tgtPath = file.Paths.get(targetPath)
    exitCondition match {

      // Termination criteria: exitCondition returned true
      case Some(fun)
        if fun(rootAsset) => Nil

      // Termination criteria: Current number of iterations is higher than numIterations
      case _
        if currentIteration > numIterations => Nil

      // Output initial system without changes
      case _
        if currentIteration == 0 =>
          rootAsset.prettyprint

          val currDir = tgtPath.resolve(s"v$currentIteration")
          Files.createDirectories(currDir)
          SerialiseAssetTree(rootAsset, currDir.toAbsolutePath.toString)

          val iterationStatisticsPerRepo = for {
            repository <- transformASTToList(rootAsset).filter(a => a.assetType == RepositoryType)
            relativeRepoPath = file.Paths.get(repository.name.split("\\\\").takeRight(2).mkString("\\"))
            repoPath = currDir.toAbsolutePath.resolve(relativeRepoPath)
          } yield {
            val compiled = compilationChecker.checkCompile(repoPath.toString)

            val features = transformFMToList(repository.featureModel.get)
              .filter(feat => feat.getClass != classOf[RootFeature])
              .filterNot(feat => feat.name == "UnAssigned" && feat.parent.get.getClass == classOf[RootFeature])
            val numFeatures = features.length
            val scatteringDegrees = features.map(feat => (feat, feat.mappedAssets(repository).length))
            val tanglingDegrees = features.map(feat => (feat, feat.mappedAssets(repository).flatMap(assets => extractFeatures(assets.presenceCondition)).filter(otherFeat => feat != otherFeat).distinct.length))

            RepositoryResults(repository, compiled, numFeatures, scatteringDegrees, tanglingDegrees)
          }

        // This assumes unique names for each feature and excludes the RootFeature and UnAssigned feature which are set by default and starting with one repo are cloned from each other.
        val absoluteFeatureCount = rootAsset.children.map(a => transformFMToList(a.featureModel.get).filterNot(feat => feat.isInstanceOf[RootFeature] || (feat.name == "UnAssigned" && feat.parent.get.isInstanceOf[RootFeature]))).flatten.groupBy(feat => feat.name).size

        val locCountThread = new Thread {
          override def run: Unit = {
            println("Beginning calculating LoC in iteration " + currentIteration)
            RunEvaluation.saveLoCForIteration(experimentPath = Paths.get(targetPath), currentIteration, rootName)
            println("Finished calculating LoC in iteration " + currentIteration)
          }
        }
        locCountThread.start
        locCountThread.join

        val iterationStatisticsTotal = IterationResults(currentIteration, false, absoluteFeatureCount, iterationStatisticsPerRepo, None)

        (iterationStatisticsTotal, locCountThread) :: run(numIterations, targetPath, exitCondition, maxAttempts = maxAttempts, currentIteration = currentIteration + 1)

      // Execute normally
      case _ =>
        val taOption = generateNextOperation(maxAttempts)

        val currDir = tgtPath.resolve(s"v$currentIteration")
        Files.createDirectories(currDir)
        SerialiseAssetTree(rootAsset, currDir.toAbsolutePath.toString)

        val iterationStatisticsPerRepo = for {
          repository <- transformASTToList(rootAsset).filter(a => a.assetType == RepositoryType)
          relativeRepoPath = file.Paths.get(repository.name.split("\\\\").takeRight(2).mkString("\\"))
          repoPath = currDir.toAbsolutePath.resolve(relativeRepoPath)
        } yield {
          //val compiled = executeGradle(repoPath.toString, "compileJava")
          val compiled = compilationChecker.checkCompile(repoPath.toString)

          val features = transformFMToList(repository.featureModel.get)
            .filter(feat => feat.getClass != classOf[RootFeature])
            .filterNot(feat => feat.name == "UnAssigned" && feat.parent.get.getClass == classOf[RootFeature])
          val numFeatures = features.length
          val scatteringDegrees = features.map(feat => (feat, feat.mappedAssets(repository).length))
          val tanglingDegrees = features.map(feat => (feat, feat.mappedAssets(repository).flatMap(assets => extractFeatures(assets.presenceCondition)).filter(otherFeat => feat != otherFeat).distinct.length))

          RepositoryResults(repository, compiled, numFeatures, scatteringDegrees, tanglingDegrees)
        }

        // This assumes unique names for each feature and excludes the RootFeature and UnAssigned feature which are set by default and starting with one repo are cloned from each other.
        val absoluteFeatureCount = rootAsset.children.map(a => transformFMToList(a.featureModel.get).filterNot(feat => feat.isInstanceOf[RootFeature] || (feat.name == "UnAssigned" && feat.parent.get.isInstanceOf[RootFeature]))).flatten.groupBy(feat => feat.name).size

        val locCountThread = new Thread {
          override def run: Unit = {
            println("Beginning calculating LoC in iteration " + currentIteration)
            RunEvaluation.saveLoCForIteration(experimentPath = Paths.get(targetPath), currentIteration, rootName)
            println("Finished calculating LoC in iteration " + currentIteration)
          }
        }
        locCountThread.start

        val iterationStatisticsTotal = IterationResults(currentIteration, taOption.isDefined, absoluteFeatureCount, iterationStatisticsPerRepo, taOption)
        println(s"Change was generated: ${taOption.isDefined}")

        (iterationStatisticsTotal, locCountThread) :: run(numIterations, targetPath, exitCondition, maxAttempts = maxAttempts, currentIteration = currentIteration + 1)
    }
  }


  def generateNextOperation(maxAttempts: Int) : Option[Transaction] = {
    // just works on probDistribution, thus no general implementation
    def argmax(a: Generator, b: Generator) = {
      if (probThresholds.get(a).get >= probThresholds.get(b).get)
      { a }
      else
      { b }
    }

    val random = Random.nextDouble()

    // first: filter for all keys, whose threshold is beneath or equal to random
    // second: select the key with the maximum threshold, that was still passed -> that is the selected operation
    val opGenerator = probThresholds.keySet.
      filter(op => probThresholds.get(op).get <= random).
      reduce( (a,b) => argmax(a,b) )

    // can extract info here for changing probability distributions, if it resulted in None?
    tryToGenerate(opGenerator, maxAttempts)
  }

  def tryToGenerate(generator: Generator, remainingAttempts: Int) : Option[Transaction] = {
    generator.generate match {
      case None
        if remainingAttempts <= 0 => None
      case None
        if remainingAttempts > 0 => tryToGenerate(generator, remainingAttempts - 1)
      case Some(operation) =>
        val transaction = bg.transaction.TestExecCompilationTransaction(rootAsset, List(operation), targetPath, compilationChecker)
        try {
          transaction.execute
          Some(transaction)
        } catch {
          case ex: TransactionFailureException =>
            if (remainingAttempts > 0) {
              tryToGenerate(generator, remainingAttempts - 1)
            } else {
              None
            }
          case ex: ASTPotentiallyCorruptException =>
            println("AST is potentially corrupt. Exit program now.")
            System.exit(1)
            None
        }
    }
  }

  def updateGenerators = {
    //    operationGenerators.values.foreach(gen => gen.update)
  }
}