package vp.bench.bg

import java.io.{File, FileWriter}
import java.text.SimpleDateFormat
import java.util.Calendar
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}

import RunEvaluation.postprocessJavaParsing
import vp.bench.bg.generators.{AddFeatureGenerator, AddLineGenerator, DeleteLineGenerator, RemoveFeatureGenerator, ReplaceLineGenerator}
//import se.gu.bg.TestConfigurationProvider.{createAlgorithmsProj, createDisruptorProj, createEventbusProj, createJadxProj, createJsonJavaProj, createNLPProj, createReactiveXProj, createStructurizrProj, createTeammatesProj}
import TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import Utilities.{clearAndCopyInto, findRepoOnUpPath}
import vp.bench.bg.generators.RemoveFeatureGenerator
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.model.{Asset, FileType, FolderType}
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.getListOfSubDirectories
import se.gu.vp.operations.CalculatorSimulation._
import se.gu.vp.operations.JavaParser.JavaParserMain.{parseJavaFile, readInJavaAssetTree}
import se.gu.vp.operations.Replay.AssetHelpers
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import se.gu.vp.operations.{AddAsset, CloneFeature, SerialiseAssetTree}

import scala.::
import scala.io.Source

object CalculatorTest {

  def main(args: Array[String]) : Unit =
  {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm")
    val datetime = Calendar.getInstance().getTime()
    val targetpath =  "D:\\Dokumente\\_Studium\\masterarbeit\\test"
    val targetdatepath = targetpath + "\\" + dateFormat.format(datetime)
    val targetdatedir = new File(targetdatepath)
    targetdatedir.mkdir()

    val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot"
    val workingDirectory = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir"

    clearAndCopyInto(workingDirectory, readInPath)
    val relPathToRoot = readInPath.split("\\\\").last
    val ast = readInJavaAssetTree(workingDirectory + "\\" + relPathToRoot)

    ast match {
      case None => return
      case Some(astroot) =>

        SerialiseAssetTree(astroot, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir")

        val structurizrProject = createStructurizrProj()
        val disruptorProject = createDisruptorProj()
        val rootProjects = List(disruptorProject)

        val delLineGen = DeleteLineGenerator(astroot, 0.8)
        val addLineGen = AddLineGenerator(astroot, 0.8)
        val repLineGen = ReplaceLineGenerator(astroot, 0.8)

        val filterInsertionPoints = List(
          workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java"
        )

        val postProcessAssets = for (fIP <- filterInsertionPoints) yield {
          getassetbyname(fIP, astroot).get
        }
        postprocessJavaParsing(astroot, postProcessAssets)

        val relFilterInsertionPaths = for {
          filterInsPoint <- filterInsertionPoints
        } yield {
          val insPointAsset = getassetbyname(filterInsPoint, astroot).get
          val insPointPath  = file.Paths.get(filterInsPoint)
          val insPointRepoPath  = file.Paths.get(findRepoOnUpPath(insPointAsset).get.name)
          val relRepoPath = insPointRepoPath.relativize(insPointPath)
          relRepoPath
        }


        val remFeatureGen = RemoveFeatureGenerator(astroot)
        val generators = List(remFeatureGen)
        val compilationChecker = GradleCompilationChecker()
        val opGen = new BenchmarkGenerationRunner(astroot, workingDirectory, generators, compilationChecker)//, Some(config))
        opGen.run(20, targetdatepath, None)
        val a = 1
    }
  }

}