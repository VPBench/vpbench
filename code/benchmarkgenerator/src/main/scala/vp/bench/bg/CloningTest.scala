package vp.bench.bg

import Utilities.{clearAndCopyInto, findFolderParentAsset}
import se.gu.vp.model.{Asset, AssetType, BlockType}
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Operation
import se.gu.vp.operations.Utilities.transformASTToList
import java.nio.file

import vp.bench.bg.operations.AddLineToAsset
import vp.bench.bg.transaction.{TestExecCompilationTransaction, Transaction}

import scala.util.Random

object CloningTest {

  def generateChange(rootAsset: Asset) = {
    val assets = transformASTToList(rootAsset)
    val changeableAssets = assets
      .filter(a => a.assetType == BlockType)
    val selectedAsset = changeableAssets(Random.nextInt(changeableAssets.length))

    // for debugging
    println(s"ChangeAsset $selectedAsset")

    changeAsset(selectedAsset)
  }

  def changeAsset(selectedAsset: Asset) : Option[Operation]  = {
    if (selectedAsset.content == None) {
      None
    } else {
      val content = selectedAsset.content.get

      // addLineIndex is the linenumber, where the new line will be, + 1 allows it to be inserted at the end
      val addLineIndex = Random.nextInt(content.length + 1)
      // select new line from any block inside the same folder (package) -> intermediate point between FileType and RepositoryType -> tradeoff between applicability (due to visibility) and amount of possibilities
      val potentialStatements = transformASTToList(findFolderParentAsset(selectedAsset).get)
        .filter(elem => elem.assetType == BlockType)
        .flatMap(block => block.content)
        .flatten

      val addLine = potentialStatements(Random.nextInt(potentialStatements.length))

      Some(AddLineToAsset(selectedAsset, addLineIndex, addLine, false))
    }
  }

  def main(args: Array[String]): Unit = {
    val inputPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot"
    val workingDirectory = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir"
    val compilationChecker = GradleCompilationChecker()

    clearAndCopyInto(workingDirectory, inputPath)
    val relPathToRoot = inputPath.split("\\\\").last
    val ast = readInJavaAssetTree(workingDirectory + "\\" + relPathToRoot).get

    val ops = for {i <- Range(0,10)}
      yield {
        generateChange(ast)
      }
    val operations = ops.flatten.toList
    val ta = transaction.TestExecCompilationTransaction(ast, operations, file.Paths.get("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TempSerializationDir"), compilationChecker)
    ta.execute

    val b = 2
  }
}
