package vp.bench.bg

import vp.bench.bg.gradlehandler.GradleExecutor

abstract class CompilationChecker {
  def checkCompile(repoPath: String) : Boolean
}

case class GradleCompilationChecker() extends CompilationChecker {
  override def checkCompile(repoPath: String): Boolean = {
    GradleExecutor.executeGradle(repoPath, "compileJava")
  }
}
