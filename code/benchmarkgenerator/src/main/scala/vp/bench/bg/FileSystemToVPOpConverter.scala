package vp.bench.bg

import java.nio.file
import java.nio.file.{Files, Path}

import Utilities.{findRepoOnUpPath, getImmediateAncestorProjectInRepo}
import se.gu.vp.model.{Asset, FileType, FolderType}
import se.gu.vp.operations.AssetCloneHelpers.deepCloneAsset
import se.gu.vp.operations.Utilities.setNewNamesRecursively
import se.gu.vp.operations.{AddAsset, CloneAsset, Operation}
import vp.bench.bg.model.Project
import vp.bench.bg.operations.AddModifiedExternalFileAsset
import vp.operations.AddExternalAsset

object FileSystemToVPOpConverter {
  val ignoredPathNames = List(".gradle", "build", ".feature-to-file", ".feature-to-folder", ".feature-model", ".trace-db")

  // Idea: When converting files to VP-operations, only then check, if file already exists somewhere else -> dont clone entire folders, just files, as they are for now considered unmutable
  // but: is that even relevant for now? As I am not planning on doing anything with dependency-java-files anyways -> propagations will not be required?
  // pass parentRepoReroutePath as a workaround for an issue, where the lookup with the Asset reference does not work anymore, after new children were added to it -> idea: change structure to first recursive, then add to parent -> build bottom-up, this way repository gets a new child in the latest possible point
  // parentRepoReroutePath = targetPath of Repo
  // repo = findRepoOnUpPath, because nothing new can be added in AddFeature immediately beneath the RootAsset, afterwards you have to have a Repo, which is the first possible parameter for parentAsset
  // use of one project alone assumes that there can be no cross-cutting connections to projects beneath other rootProjects
  def createVPOpsWithCloning(currentPath: Path, parentAsset: Asset, parentRepoReroutePath: Path, project: Project, clonedAssetMap: Option[Map[Path, Asset]]): List[Operation] = {
    val repo = findRepoOnUpPath(parentAsset).get

    // Get immediate ancestor project in current repo
    val originalProject = getImmediateAncestorProjectInRepo(project.getRootProject, repo, currentPath).get

    // TODO: Difference between relativePath and relPath
    // relativePath = Path from ProjectFolder in Repository (excluding that)
    // is currentPath beneath or equal to project instance in repo? CurrentPath is at least one layer beneath repo, as parentAsset can maximally be Repo and CurrentPath is one layer beneath
    // All RootProject instances are one level beneath Repo
    // OriginalProject is defined as a project, with whose path currentPath starts with. Furthermore this is the project, where they share the longest common beginning
    // => currentPath is equal to the project root of originalProject beneath repo or deeper.
    // relativePath == "" iff currentPath == originalProject.targetPath (under corresponding repo)

    // Calculate relative path from immediate ancestor project folder in repository to actual object
    val relativePath = try {
      currentPath.subpath(originalProject.getAbsolutePathTarget(repo).get.getNameCount, currentPath.getNameCount)
    } catch {
      case ex: IllegalArgumentException => file.Paths.get("")
    }

    // Calculate original path using the ancestor projects original path and the relative path from therein
    val originalPath = originalProject.getAbsolutePathSrc.resolve(relativePath)

    val assetType =
      if (Files.isDirectory(currentPath)) {
        FolderType
      }
      else {
        FileType
      }

    assetType match {
      case FolderType =>
        // relativePath can only be "", if currentPath == project root beneath current repo
        val assetName = if (relativePath.toString == "") {
          Some(originalProject.name)
        } else {
          None
        }

        val aeaOp = AddExternalAsset(originalPath, parentAsset, assetName)

        val newAsset = aeaOp.newAsset.get
        aeaOp :: Files.walk(currentPath, 1)
          .filter(elem => !(elem == currentPath))
          .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
          .toArray
          .toList
          .map(obj => obj.asInstanceOf[Path])
          .flatMap(elem => createVPOpsWithCloning(elem, newAsset, parentRepoReroutePath, project, clonedAssetMap))
      case FileType
        if relativePath.toString == "build.gradle" =>
        val buildContent = Files.readString(currentPath).split('\n').toList
        AddModifiedExternalFileAsset(originalPath, parentAsset, buildContent) :: Nil
      case FileType =>
        if (clonedAssetMap.isDefined && clonedAssetMap.get.contains(currentPath)) {
          println(s"Cloning ${clonedAssetMap.get(currentPath)} to $currentPath")

          // Set deepCloneFeatures to false to avoid adding partial features
          CloneAsset(clonedAssetMap.get(currentPath), parentAsset, deepCloneFeature = false) :: Nil
        } else {
          AddExternalAsset(originalPath, parentAsset) :: Nil
        }
    }
  }

  // Recursively check all immediate children (of currentPath) and see if their path is contained in pathToAsset (i.e. the assetTree, as this is supposed to be generated from it). Is that not the case, the element and all its children are added to the AT.
  def checkCreateVPOpsWithCloning(currentPath: Path, pathToAsset: Map[Path, Asset], project: Project, clonedAssetMap: Option[Map[Path, Asset]]): List[Operation] = {
    // Problem, when everything exists already -> is the return type still List[Operation] in this case?
    Files.walk(currentPath, 1)
      .filter(elem => !(elem == currentPath))
      .filter(elem => !ignoredPathNames.contains(elem.getFileName.toString))
      .toArray
      .toList
      .map(obj => obj.asInstanceOf[Path])
      .flatMap(elem => {
        if (pathToAsset.keySet.contains(elem)) {
          checkCreateVPOpsWithCloning(elem, pathToAsset, project, clonedAssetMap)
        }
        else {
          val parentAsset = pathToAsset(currentPath)
          //          val assetToPath = pathToAsset.map(elem => elem.swap)
          val parentRepoReroutePath = file.Paths.get(findRepoOnUpPath(parentAsset).get.name)
          createVPOpsWithCloning(elem, parentAsset, parentRepoReroutePath, project, clonedAssetMap)
        }
      })
  }
}
