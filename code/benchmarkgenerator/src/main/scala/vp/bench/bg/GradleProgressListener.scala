package vp.bench.bg

import org.gradle.tooling.events.{FinishEvent, ProgressListener}
import org.gradle.tooling.events.task.{TaskFailureResult, TaskFinishEvent, TaskSkippedResult, TaskSuccessResult}
import org.gradle.tooling.{GradleConnector, ProgressEvent, ResultHandler, events}

class GradleProgressListener extends ProgressListener {
  override def statusChanged(event: events.ProgressEvent): Unit = {
    event match {
      case ev : FinishEvent =>
        println("Finish event here.")
        ev.getResult match {
          case res : TaskSuccessResult => println(ev.getClass.toString + " " + ev.getDisplayName)
          case res : TaskSkippedResult => println(ev.getClass.toString + " " + ev.getDisplayName)
          case res : TaskFailureResult => println(ev.getClass.toString + " " + ev.getDisplayName)
          case res => println(ev.getClass.toString + " " + ev.getDisplayName)
        }
      case ev => println(ev.getClass.toString + " " + ev.getDisplayName)
    }
  }
}
object GradleProgressListener {
  def apply() : GradleProgressListener = new GradleProgressListener()
}