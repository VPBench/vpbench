package vp.bench.bg

import java.io.File
import java.nio.file
import java.nio.file.{Files, Path, Paths, StandardOpenOption}

import scala.collection.JavaConversions._
import scala.sys.process
import scala.sys.process._
import Utilities.{copySafely, createDictIfNotExists, getAllIndicesOf, replaceStringSlice}
import org.gradle.api.tasks.TaskExecutionException
import TestConfigurationProvider.createStructurizrProj
import se.gu.vp.model.{BlockType, ClassType, FieldType, MethodType}
import vp.bench.bg.gradlehandler.GradleParsingUtilities.getClosureIndices
import vp.bench.bg.model.{Dependency, JavaProject, Project, ProjectDependency}
//import se.gu.bg.TestConfigurationProvider.{createDisruptorProj, createJadxProj, createReactiveXProj, createStructurizrProj, disruptorUtilProjTest, disruptorUtilTest, jadxCliRenameConverterProjTest, jadxCliRenameConverterTest, jadxProjTest, reactiveXDisposableObserverProjTest, reactiveXDisposableObserverTest, structurizrClientEncryptedJsonTests, structurizrClientStringUtilsTest}
//import TestConfigurationProvider.{disruptorUtilProjTest, disruptorUtilTest, structurizrClientEncryptedJsonTests, structurizrClientStringUtilsTest}
import vp.bench.bg.gradlehandler.GradleHandler.{adaptBuildGradle, getDependenciesAndClosureBorders, simplifyBuildGradle}
import vp.bench.bg.model
import vp.bench.bg.model.Project
import org.gradle.tooling.{GradleConnector, ProgressListener, ResultHandler}
import org.gradle.tooling.model.{GradleProject, ProjectModel}
import org.gradle.tooling.model.build.BuildEnvironment
import org.gradle.tooling.model.gradle.GradleBuild
import org.gradle.tooling.model.idea.IdeaDependency
import org.gradle.tooling.model.idea.IdeaModule
import org.gradle.tooling.model.idea.IdeaProject
import org.gradle.tooling.model.idea.IdeaSingleEntryLibraryDependency
import se.gu.vp.model.Asset
import se.gu.vp.operations.Utilities.getassetbyname

import scala.collection.immutable.HashMap
import scala.util.Random

object HandleJavaDependencies {
  def getDependenciesRecursively(rootProject: JavaProject, subProject: JavaProject, testClass: Path, pathEnv: String, depth: Int, recursivePath: List[String]) : List[Tuple2[String,String]] = {
    def twoElArrayToTuple[T](arr: Array[T]) : Tuple2[T,T] =
    {
      assert(arr.length == 2)
      (arr(0),arr(1))
    }

    def checkClassDependency(strings: Array[String]) : Option[Array[String]] = {
      strings.length match {
        case 1 => None
        case 2
          if strings(0) == "not" && strings(1) == "found" =>
          None
        case 2 => Some(strings)
        case 3
          if strings(1) == "not" && strings(2) == "found" =>
          Some(Array(strings(0),"not found"))
        case default => None
      }
    }

    if (depth > 0) {
      // TODO: Handling of recursive dependencies on other testclasses.
      return Nil
    }

    val absProjectPath = subProject.getAbsolutePathSrc()
    val absClassOrigin = absProjectPath.resolve(testClass)

    // depth is just for debugging purposes
    println(depth + ": " + absClassOrigin)
    recursivePath.contains(absClassOrigin.toString) match {
      // recursivePath serves the purpose to catch dependency circles -> can probably happen, when there are multiple dependencies in one file, e.g. SuppressUndeliverableRule.class and SuppressUndeliverableRule$SuppressUndeliverableRuleStatement.class
      case true =>
        println("Dependency circle.")
        Nil
      case false =>
        // Classpath needs to start so that it can look for entire packagepath
        val classpathParam = rootProject.calcClasspath()
        val depString = Process(Seq("jdeps", "--class-path", classpathParam, "-recursive", "-verbose:class", absClassOrigin.toString),None,"PATH" -> pathEnv).!!

        // Convert output string of jdeps into a list of tuples representing all class-dependencies
        val dependencyList = depString
          // Split output into lines
          .split("\r\n")
          // Split lines at "->"
          .map(line => line.split("->"))
          // Check if part after "->" can be split into two parts to make a valid class-dependency instead of a jar-dependency -> maps to Some, if valid class-dep, None otherwise
          .map(line => (line(0).trim, checkClassDependency (line(1).split(' ').filter(elem => elem != ""))))
          // Filters out jar-dependencies, i.e. lines with only two elements (e.g. A -> B)
          .filter(line => line._2 != None)
          // Map to Tuple3
          .map(line => (line._1, twoElArrayToTuple (line._2.get)._1, twoElArrayToTuple (line._2.get)._2))

        // Elements in java base library should be available anyways and are thus not important here
        val reducedDependencyList = dependencyList
          .filter(dep => dep._3 != "java.base")
          .map(tuple3 => (tuple3._2,tuple3._3))

        // Convert classpathParam to classpath (strip "" around it)
        val classpath = classpathParam.stripPrefix("\"").stripSuffix("\"")
        // Assuming only .jars in classpath of jdeps -> TODO: check for .class-files
        val classpathJars = classpath
          .split(';')
          .map(path => path.split('\\').last)

        // Filter for all required elements in donor-project (those are local elements, which need to be copied)
        val reqElements = reducedDependencyList
          .filter(dep => classpathJars.contains(dep._2))

        // Filter for all elements not available in donor-project or base-library
        val unavailableElements = reducedDependencyList
          .filter(dep => !classpathJars.contains(dep._2))
          .map(dep => dep._1)

        val distinctReqElements = reqElements.distinct

        // TODO: Different way to handle giving path to compiled tests? -> could probably be made a member of Project?
        // Checks if "not found"-elements can be found in tests and recursively calls function for these test.classes
        // Can not utilise mapping.txt, as it maps JavaElements to .java-files, not to required .class-files for recursive calls [can this behaviour be assumed? assumes 1-to-1 mapping between java-namespaces and filestructure (for tests)]
        // Not a problem, if tests require only one .class-file, i.e. max(depth) = 0
        val testBeginPath = absProjectPath.resolve("build\\classes\\java\\test")
        val recursiveReqElements = for {
          element <- unavailableElements
          elementPath = testBeginPath.resolve(element.replace('.','/').concat(".class"))
          if (Files.exists(elementPath))
          requiredElements = getDependenciesRecursively(rootProject, subProject, elementPath, pathEnv, depth+1, absClassOrigin.toString :: recursivePath)
        } yield {
          requiredElements
        }

        val flatRecReqElements = recursiveReqElements.flatten

        val result = distinctReqElements ++ flatRecReqElements
        result.distinct.toList
    }
  }

  // supports only resolution of .class-files in same subproject
  def mapClassToJava(reqElements : Seq[Tuple2[String,String]], absRootProjectDir : file.Path, project : Project) : Seq[file.Path] = {
    // gets the line indices mapping to each .java-line
    def getRanges(keyIndices : Seq[Int], lineNumber : Int) : List[Tuple2[Int,List[Int]]] =
    {
      keyIndices match {
        case hd +: hd2 +: ls =>
          (hd, ((hd+1) until hd2).toList) :: getRanges(hd2 +: ls, lineNumber)
        case hd +: _ =>
          (hd, ((hd+1) until lineNumber).toList) :: Nil
        case _ => Nil
      }
    }

    val absProjectDir = project.getAbsolutePathSrc()

    // assuming mapping-file exists
    val mappingFile = absProjectDir.resolve("build/tmp/compileJava/source-classes-mapping.txt")

    // mapping is used for incremental compilation: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/tasks/compile/JavaCompile.java#L163
    // reading mapping in gradle implementation: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/SourceClassesMappingFileAccessor.java#L54
    // sourceFileClassNameConverter: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/incremental/recomp/DefaultSourceFileClassNameConverter.java#L26
    // otherwise: fileNameDerivingClassNameConverter: https://github.com/gradle/gradle/blob/12a9731ad6415d0ae57d859b156f36a008d000a0/subprojects/language-java/src/main/java/org/gradle/api/internal/tasks/compile/incremental/recomp/FileNameDerivingClassNameConverter.java#L25
    val fileContent = Files.readAllLines(mappingFile)
    val indexedFileContent =
      for {
        index <- 0 until fileContent.length
        line = fileContent(index)
      } yield {(index,line)}
    // filter out all indices for lines representing .java-files (all other begin with " ")
    val keyIndices = indexedFileContent.filter(element => !element._2.startsWith(" ")).map(element => element._1)
    // get list of mapped lines for each .java-line
    val keyRanges = getRanges(keyIndices, fileContent.length)
    // generate dictionary with the JavaElements being the keys mapping to the .java-files
    val depMapList = for {
      keyRange <- keyRanges
      mappingLineInd <- keyRange._2

      mapElem = fileContent(mappingLineInd).stripLeading()
      mapFile = fileContent(keyRange._1)
    } yield {
      (mapElem -> mapFile)
    }
    val depDict = depMapList.toMap

    val reqFiles = for (reqEl <- reqElements) yield (depDict(reqEl._1))
    // use distinct to have every required .java-file only once
    reqFiles.distinct.map(str => file.Paths.get(str))
  }


  // Returns optional rootProject-name, if set and a list of all included subprojects
  def parseSettings(settingsContent : String) : Tuple2[Option[String],Seq[String]] =
  {
    // rootProject is of type ProjectDescriptor: https://docs.gradle.org/current/javadoc/org/gradle/api/initialization/ProjectDescriptor.html
    // can theoretically be set using setName
    val projectRootNameRegex = "rootProject.name[\\s]*=[\\s]*'([^']+)'".r
    val rootNameLine = projectRootNameRegex findFirstIn settingsContent
    val rootProjectName = rootNameLine match {
      case None =>
        None
      case Some(line) =>
        val projectRootNameRegex(rootProjectNameMatch) = line
        Some(rootProjectNameMatch)
    }

    // https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:include(java.lang.String[])
    // only matches empty space notation, not brackets (groovy, not kotlin)
    val subprojectIncludeRegex = "include[\\s]+'([^']+)'".r
    val includedSubprojectLineIterator = for {
      includedSubprojectMatch <- subprojectIncludeRegex findAllIn settingsContent
      subprojectIncludeRegex(includedSubproject) = includedSubprojectMatch
    } yield { includedSubproject }
    val subprojects = includedSubprojectLineIterator.toList
    (rootProjectName, subprojects)
  }

  def getProjectNameAndSubprojects(path : Path, settingsFile : Path) : Tuple2[String,Seq[String]] =
  {
    val (intmedProjName, subprojects) =
      if (Files.exists(settingsFile))
      {
        val settings = Files.readString(settingsFile)
        parseSettings(settings)
      }
      else
      {
        (None,Nil)
      }
    intmedProjName match {
      case None => (path.getFileName.toString,subprojects)
      case Some(name) => (name, subprojects)
    }
  }

  // When called for subproject, assumes that targetDir is set to absolute path of target root project
  def copyGradleProjectWrapper(project: Project, targetDir : Path, repo: Asset) : Unit =
    {
      project.pathToGenTargetInRepo.contains(repo) match {
        case true =>
        case false =>
          project.parent match {
            case None =>
              val targetPath = targetDir.resolve(project.name)
              copyGradleProject(project, targetPath, None, repo)
              project.pathToGenTargetInRepo = project.pathToGenTargetInRepo + ((repo, targetPath))
            case _ =>
              copyGradleProject(project,targetDir,None, repo)
              project.pathToGenTargetInRepo = project.pathToGenTargetInRepo + ((repo,file.Paths.get(project.name)))
          }
      }
    }

  // Does the actual project initialization -> Creates folder beneath repo, adapts and simplifies build.gradle and writes it back out -> Recursively calls copyGradleProjectWrapper for projects this project depends on
  // targetPath = Path to rootProject of project beneath current repo
  def copyGradleProject(project : Project, targetPath : Path, dirName : Option[String], repo: Asset) =
  {
      val projectPath = project.getAbsolutePathSrc()

      // Get path to project and create directory in case it does not exist already
      // Assume no settings.gradle in subprojects or at least no set rootProjectName different from folder <- still relevant after refactoring?
      val targetProject = targetPath.resolve(project.getRelativePathFromRoot())
      Files.createDirectories(targetProject)

      // Opens build.gradle, if it exists, simplifies it, adapts it to the new project structure and writes it to target location. Otherwise: nothing.
      // Assumes build.gradle to be in top-level dir of project -> TODO: CHECK: Can it in theory even be otherwise?
      val buildFile = projectPath.resolve("build.gradle")
      Files.exists(buildFile) match {
        case false =>
        case true =>
          val buildFileContent = Files.readString(buildFile)
          val simplifiedBuildFileContent = simplifyBuildGradle(buildFileContent)
          val adaptedBuildFileContent = adaptBuildGradle(simplifiedBuildFileContent, project)
          //val adaptedBuildFileContent = buildFileContent
          val file = targetProject.resolve("build.gradle")
          Files.writeString(file, adaptedBuildFileContent)

          // Extract project dependencies and initialises them
          val depRegEx = raw"dependencies\s*\Q{\E".r
          val depMatches = depRegEx.findAllMatchIn(adaptedBuildFileContent).toList
          //val depBegins = getAllIndicesOf(adaptedBuildFileContent,"dependencies")

          val dependencies = for {
            // Reverse the dependencies to not mess up the indexing of dependency-blocks with a higher identified starting index
            depMatch <- depMatches.reverse
            (dependencies,_,_) = getDependenciesAndClosureBorders(adaptedBuildFileContent,depMatch.start)
          } yield { dependencies }

          val dependentProjects = dependencies.flatten
            .filter(dep => dep match {
              case _ : ProjectDependency => true
              case _ => false
            })
            .map(dep => project.getRootProject().getSubprojectByName(dep.dependency.stripPrefix(s":${project.getRootProject().name}:")).get)

          for {
            dependentProject <- dependentProjects
          } yield {
            copyGradleProjectWrapper(dependentProject, targetPath, repo)
          }
      }
  }

  // rootProjectPath: Absolute path to root project
  // subProjectPath: Relative path from root project to subproject (assuming only one layer for now)
  // javaFiles: List of all java-files that need to be copied
  // targetPath: Path to corresponding rootProject-folder in target
  // srcPath: Project-specific path from project-folder to source-code
  // Plan:
  // 1. Resolve rootProjectPath and targetPath with subProjectPath to get the absolute paths to the subproject
  // 2. Use CopyGradleProject to initialise folder as gradle project with build-files and other gradle related files
  // 3. Attach srcPath to absolute subprojectpath in src and target
  // 4. Copy java-files
  // 5. ???
  // 6. PROFIT
  def copyProject(project: Project, javaFiles: Seq[Path], activeRepo: Asset, cloneFromOtherRepos: Boolean, genSeededRandom: Random) : Map[Path, Asset]  =
  {
    val rootProject = project.getRootProject()

    // copyGradleProjectWrapper assumes to get the rootProjects targetpath for any non-root project
    val rootTargetPath = rootProject.pathToGenTargetInRepo(activeRepo)

    val parentProjects = project.getParentProjects()
    parentProjects.foreach(proj => copyGradleProjectWrapper(proj, rootTargetPath, activeRepo))

    copyGradleProjectWrapper(project, rootTargetPath, activeRepo)
    // Assumes, that either no settings.gradle is present in subproject or rootProject.name therein is not set to a name, different from folder name in parent-folder
    // This is a weaker assumption, than that all sub(!)projects have identical name and path
    // TODO: Unify this for different projects (related to upper TODO) -> assertion is wrong due to case of empty subProjectPath (rootDir of GitProject) and nested project names, e.g. in jadx-case
    // assert(projName == subProjectPath.getFileName.toString)

    val clonedAssets = for {
      file <- javaFiles
      fileInProject = project.srcSetMain.resolve(file)
      targetPath = project.getAbsolutePathTarget(activeRepo).get.resolve(fileInProject)
      if !Files.exists(targetPath)
    } yield {
      // For each file, that does not yet exist in activeRepo -> find source path (either in other repo or project source) and copy it -> return copy source (if its an asset)

      // Tuples of repoAssets with their defined version of the required fileAsset
      // 1. Get all repos
      // 2. Filter to all repos, where project is defined
      // 3. Try to get the specified file in repo
      // 4. Filter down to all repos, that actually contain the specified file (in any version)
      //    The latter works, because we currently assume all repos to use the exact same structure and files cannot be renamed or moved at the moment.
      //    So if a file is found in any repo at the exact same location, that we aim for in the activeRepo, this has to be the same original file.
      //    While it might have been modified in the meantime, this should not matter a lot, as corrupted changes would still be caught by the compilation check afterwards.
      val candidateRepos = activeRepo.parent.get.children
        .filter(repo => project.getAbsolutePathTarget(repo).isDefined)
        .map(repo => (repo, getassetbyname(project.getAbsolutePathTarget(repo).get.resolve(fileInProject).toString, repo)))
        .filter(tuple => tuple._2.isDefined)

      // If clones should be used and a clone exists for this file -> select one, otherwise copy from source project
      val (originalAsset, copyFromPath) = if (cloneFromOtherRepos && candidateRepos.length > 0) {
        // TODO: Make selection of a cloning strategy configurable
        // Use latest version
        // val sortedRepos = candidateRepos.sortBy(tuple => - tuple._2.get.versionNumber)
        // val selectedRepo = sortedRepos(0)

        // Use candidate from random repo
        val selectedRepo = candidateRepos(genSeededRandom.nextInt(candidateRepos.length))

        val copyFromPath = Paths.get(selectedRepo._2.get.name)
        (selectedRepo._2, copyFromPath)
      } else {
        (None, project.getAbsolutePathSrc.resolve(fileInProject))
      }

      copySafely(copyFromPath, targetPath)
      // Passing the targetPath is technically not required here, as it can be inferred from originalAsset in the current setup, but it makes the code in AddExternalFeature more readable.
      (targetPath, originalAsset)
    }

    // Create map from the list of assets, that were selected as cloning sources together with their target paths (to where they were cloned => as keys)
    clonedAssets
      .filter(tuple => tuple._2.isDefined)
      .map(tuple => (tuple._1, tuple._2.get))
      .toMap
  }

  def addNewDependency(buildFile: Path, dependency: Dependency): Unit = {
    val buildFileContent = Files.readString(buildFile)

    val buildScriptRegEx = raw"buildscript\s*\Q{\E".r
    val buildScriptClosures = buildScriptRegEx.findAllMatchIn(buildFileContent)
      .map(bm => getClosureIndices(buildFileContent.substring(bm.start)))
      .toList

    // Assumes at least an empty dependencies block in systems build.gradle
    // No support for allprojects- or subprojects-dependencies here
    val depRegEx = raw"dependencies\s*\Q{\E".r
    val dependencyMatch = depRegEx.findAllMatchIn(buildFileContent)
      .filterNot(dm => buildScriptClosures.exists(bm => bm._1 < dm.start && bm._2 > dm.`end`))
      .toList.head

    val (dependencies, _, depClosureEndInd) = getDependenciesAndClosureBorders(buildFileContent, dependencyMatch.start)

    var editableDependencies = dependencies
    if (!dependencies.contains(dependency)) {
      editableDependencies = editableDependencies ++ List(dependency)
    }

    val newDependencyClosure = editableDependencies
      // create dependenciesString
      .map(dep => dep.toString)
      .mkString("\n\t")
    // put dependencies into identified block
    val dependenciesString = s"dependencies {\n\t$newDependencyClosure\n}"
    // replace current dependencies block with created one
    val newBuildFileContent = replaceStringSlice(buildFileContent, dependenciesString, dependencyMatch.start, dependencyMatch.start + depClosureEndInd)

    Files.writeString(buildFile, newBuildFileContent)
  }

  // https://docs.gradle.org/6.8.2/dsl/org.gradle.api.initialization.Settings.html#org.gradle.api.initialization.Settings:include(java.lang.String[])
  // last line of linesToAppend is just closed off with \r, not with \n
  // is this a future TODO? -> just using .mkString("\n") on linesToAppend and adding a \n if linesToAppend > 0 does not work
  def addSubprojectsToSettings(settingsFile : Path, projects : Seq[Project]) : Unit =
  {
    // TODO: Create settings.gradle, if it does not exist
    if (!Files.exists(settingsFile))
      {
        throw new Exception("settings.gradle does not exist on repository layer.")
      }

    val settingsContent = Files.readString(settingsFile)                                                                                                                                                                                                                                                         

    // Checks if projects are included via :a:b or a:b and includes them otherwise
    val linesToAppend = for {
      proj <- projects
      // Get complete project path -> probably needs to get adjusted for nesting donor-projects
      projPath = proj.getProjectPathIncludingRoot()
      // Check if project already exists (two notations: with or without leading ':')
      if !settingsContent.contains(s"include '$projPath'") && !settingsContent.contains(s"include ':$projPath'")
    } yield {
      s"\ninclude '$projPath'"
    }

    // Append lines for missing projects into settings.gradle
    Files.write(settingsFile, linesToAppend, StandardOpenOption.APPEND)
  }

  def findClosestAncestorFile(asset: Asset, fileName: String): Option[Path] = {
    if (List(ClassType, MethodType, FieldType, BlockType).contains(asset.assetType)) {
      if (asset.parent.isDefined) {
        findClosestAncestorFile(asset.parent.get, fileName)
      } else { None }
    } else {
      val path = Paths.get(asset.name).resolve(fileName)
      path.toFile.exists match {
        case true =>
          Some(path)
        case false =>
          if (asset.parent.isDefined) {
            findClosestAncestorFile(asset.parent.get, fileName)
          } else { None }
      }
    }
  }

  def main(args: Array[String]): Unit =
  {
    val target = file.Paths.get("..\\testEnvironment\\TestBase\\")
    assert(Files.exists(target))

    val rootProject = createStructurizrProj()

    // Get Map from jar-names to their corresponding projects, assumes unique jar-names.
    val jarPathToProjectMap = rootProject.getJarToProjMap()
    val jarNameToProjectMap = jarPathToProjectMap.map(tuple => (tuple._1.getFileName.toString,tuple._2))

    //Test Gradle Tooling API
    val connection = GradleConnector.newConnector.forProjectDirectory(new File(rootProject.getAbsolutePathSrc().toString)).connect

    // Seems to contain most things I needed apart from repositories, not sure about sourceSets
    val project = connection.getModel(classOf[IdeaProject])
    val listener = GradleProgressListener()

    connection.newBuild().forTasks("projects").run()
    connection.action().build().run()
    // when build fails -> exception is thrown -> Listener should not be necessary, just try catch and if caught -> compilation failed
    try {
      connection.newBuild().forTasks("clean", "compileJava").addProgressListener(listener).run()
    } catch {
      case ex : TaskExecutionException => println (s"TaskExecutionException: ${ex.getMessage}")
      case ex : Exception => println(s"Ordinary Exception: ${ex.getMessage}")
      case _ => println("Not an exception, still caught.")
    }
    connection.action().build().forTasks("jar","compileTest").run()
        val buildEnv = connection.getModel(classOf[BuildEnvironment])
    val gradBuild = connection.model[GradleBuild](classOf[GradleBuild])
    val gradProjBuilder = connection.model[GradleProject](classOf[GradleProject])
    val model = gradProjBuilder.get()
    val projModel = connection.getModel(classOf[ProjectModel])
    try connection.newBuild.forTasks("tasks").run
    finally {
      connection.close
    }
    Nil
  }
}
