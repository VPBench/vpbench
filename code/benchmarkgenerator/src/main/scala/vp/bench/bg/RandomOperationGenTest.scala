package vp.bench.bg

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations._
import se.gu.vp.operations.Replay.JavaScriptFileParser
import se.gu.vp.operations.Utilities._

object RandomOperationGenTest {
  def main(args: Array[String]) : Unit = {
    val rfleft = new RootFeature("FmLeftRoot")
    val fmleft = new FeatureModel(rfleft)
    val feature1 = Feature("feature1")
    val feature2 = Feature("feature2")
    val feature3 = Feature("feature3")
    val feature4 = Feature("feature4")
    val feature5 = Feature("feature5")
    val feature6 = Feature("feature6")
    AddFeatureToFeatureModel(feature1,rfleft)
    AddFeatureToFeatureModel(feature2,rfleft)
    AddFeatureToFeatureModel(feature3,feature1)
    AddFeatureToFeatureModel(feature4,feature2)
    AddFeatureToFeatureModel(feature5,feature1)
    AddFeatureToFeatureModel(feature6,feature2)

    val fmright = new FeatureModel(new RootFeature("FmRightRoot"))

    val root = Asset("Root",VPRootType)
    val repo1 = Asset("Repository 1",RepositoryType)
    val repo2 = Asset("Repository 2",RepositoryType)
    val folder1 = Asset("Folder 1",FolderType)
    val folder2 = Asset("Folder 2",FolderType)
    val folder3 = Asset("Folder 3",FolderType)
    val folder4 = Asset("Folder 4",FolderType)
    val file1 = Asset("File 1",FileType)
    val file2 = Asset("File 2",FileType)
    val file3 = Asset("File 3",FileType)
    val file4 = Asset("File 4",FileType)
    val file5 = Asset("File 5",FileType)
    val file6 = Asset("File 6",FileType)
    AddFeatureModelToAsset(repo1,fmleft)
    AddFeatureModelToAsset(repo2,fmright)

    AddAsset(repo1,root)
    AddAsset(repo2,root)
    AddAsset(folder1,repo1)
    AddAsset(folder2,repo1)
    AddAsset(folder3,repo2)
    AddAsset(folder4,repo2)
    AddAsset(file1,folder1)
    AddAsset(file2,folder1)
    AddAsset(file3,folder2)
    AddAsset(file4,folder3)
    AddAsset(file5,folder3)
    AddAsset(file6,folder4)

    MapAssetToFeature(folder2,feature1)
    MapAssetToFeature(folder2,feature3)
    MapAssetToFeature(folder2,feature4)

    MapAssetToFeature(folder1,feature1)

    MapAssetToFeature(file1,feature1)

    MapAssetToFeature(file2,feature3)
    MapAssetToFeature(file2,feature5)

    root.prettyprint

    val assets = transformASTToList(root)
    val featuresLeft = transformFMToList(fmleft)
    val featuresRight = transformFMToList(fmright)

    val finish = "yay"
  }
}