package vp.bench.bg

import java.io.{File, FileWriter}
import java.nio.file
import java.nio.file.{Files, Path, Paths}
import java.text.SimpleDateFormat
import java.util.Calendar

import vp.bench.bg.TestConfigurationProvider.{createAlgorithmsProj, createDisruptorProj, createJadxProj, createStructurizrProj}
import vp.bench.bg.generators.RemoveFeatureGenerator
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.SerialiseAssetTree
import se.gu.vp.operations.Utilities.{featureModelContainsFeature, getassetbyname, transformASTToList}
import net.liftweb.json._
import net.liftweb.json.Serialization.{read, write}
import net.liftweb.json.Xml.toXml
import vp.bench.bg.evaluation.AggregatedResults
import se.gu.vp.model.{Asset, BlockType, FeatureTraceDatabase, FileType, Relationship, TraceDatabase}
import vp.bench.bg.setup.ConfigurationReader
import vp.bench.bg.Utilities.{clearAndCopyInto, findRepoOnUpPath, isTransitiveChildOf}
import vp.bench.bg.metadata.GenerationMetadata
import se.gu.vp.operations.metadata.{Metadata, OperationMetadata}
import vp.bench.bg.evaluation.{AggregatedResults, Parameters}
import vp.bench.bg.generators.{AddFeatureGenerator, AddLineGenerator, CloneFeatureGenerator, CloneRepositoryGenerator, DeleteLineGenerator, Generator, RemoveFeatureGenerator, ReplaceLineGenerator}
import vp.bench.bg.metadata.{GenerationMetadata, IterationMetadata}
import vp.bench.bg.model.Project
import vp.bench.bg.operations.AddLineToAsset

import scala.collection.mutable
import scala.sys.process.Process

object RunEvaluation {

  // Read in configuration
  val configFile = file.Paths.get("config.json")
  val config = ConfigurationReader.readConfiguration(configFile)

  val clocPath = config.resrecConf.clocPath
  val sqlitePath = config.resrecConf.sqlitePath
  val sqliteMaxStatementLength = 7500 // TODO: more elaborate testing for maximum length

  def chunkDbScript(sqlScript: String, init: Boolean) : List[String] = {
    val sqlStatements = sqlScript.split(';')
    var sqlStatementsEditable = sqlStatements
    val initStatements = if (init) {
      val tableCreation = sqlStatements.take(2).mkString(";")
      sqlStatementsEditable = sqlStatementsEditable.drop(2)
      Some(tableCreation)
    } else { None }

    val transactionBegin = "begin transaction;\r\n"
    val transactionEnd = "commit;\r\n"
    val maxLengthPerChunk = sqliteMaxStatementLength - transactionBegin.size - transactionEnd.size

    sqlStatementsEditable = sqlStatementsEditable.drop(1).dropRight(2) // drop begin and end transaction
    val chunkableStatementsWSize = sqlStatementsEditable.map(stmt => (stmt, stmt.size))
    val statementChunks = chunkableStatementsWSize.foldRight(List.empty[List[(String, Int)]])((tuple, list) => {
      // + 1 as we need to readd the ';' after the statement, so it needs to fit in, too
      if(tuple._2 + 1 > maxLengthPerChunk) {
        throw new Exception("single line is too long")
      }
      if(list.isEmpty) {
        (tuple :: List.empty[Tuple2[String,Int]]) :: list
      } else {
        val currentChunk = list.head
        // the length of all statements in the chunk + one semicolon per chunk + the new statement + one semicolon needs to fit // the - 1 on the other side is for the semicolon that needs to be added between the last chunk entry and the commit command
        if (currentChunk.isEmpty || (currentChunk.map(tuple => tuple._2).sum + currentChunk.size) + (tuple._2 + 1) <= maxLengthPerChunk - 1) {
          (tuple :: currentChunk) :: list.drop(1)
        } else {
          (tuple :: List.empty[(String, Int)]) :: list
        }
      }
    }).map(list => list.map(tuple => tuple._1))
      .map(list => list.mkString(";"))
      .map(chunks => transactionBegin + chunks + ";" + transactionEnd)
    if (initStatements.isDefined) {
      initStatements.get :: statementChunks
    } else { statementChunks }
  }

  def accessDatabase(command: Seq[String]) = {
    synchronized {
      Process(command, None, "PATH" -> "").!
    }
  }

  def writeErrorToErrorFile(path: String, iteration: Int, e: Exception) = {
    synchronized {
      val fw = new FileWriter(path, true)
      try {
        fw.write("Sqlite Error occurred in iteration: " + iteration + "; exception: " + e.getMessage)
      } finally {
        fw.close
      }
    }
  }

  def saveLoCForIteration(experimentPath: Path, iteration: Int, rootName: String) = {
    if (iteration == 0) {
      val initFolder = experimentPath.resolve("v0").resolve(rootName).toFile.listFiles.toList.find(file => file.isDirectory).get.getName
      val dbInitScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", s"0-$initFolder", "--timeout", "0", experimentPath.resolve("v0").toString), None,"PATH" -> "").!!
      val initChunks = chunkDbScript(dbInitScript, true)
      accessDatabase(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + initChunks(0) + "\""))
      //Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + initChunks(0) + "\""), None, "PATH" -> "").!
      for (chunk <- initChunks.drop(1)) {
        try {
          accessDatabase(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""))
          //Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""), None, "PATH" -> "").!
        } catch {
          case e : Exception =>
            println("Chunk resulting in an exception during execution of sqlite command: " + chunk)
            writeErrorToErrorFile(experimentPath.resolve("sqliteErrors.txt").toString, iteration, e)
        }
      }
    } else {
      val folder = experimentPath.resolve(s"v$iteration").resolve(rootName)
      for {
        repoFolder <- folder.toFile.listFiles
        if repoFolder.isDirectory
      } yield {
        val additionScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", s"$iteration-${repoFolder.getName}", "--sql-append", "--timeout", "0", repoFolder.toString), None, "PATH" -> "").!!
        val appendChunks = chunkDbScript(additionScript, false)
        for (chunk <- appendChunks) {
          try {
            accessDatabase(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""))
            //Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""), None, "PATH" -> "").!
          } catch {
            case e: Exception =>
              println("Chunk resulting in an exception during execution of sqlite command: " + chunk)
              writeErrorToErrorFile(experimentPath.resolve("sqliteErrors.txt").toString, iteration, e)
              throw e
          }
        }
      }
    }
  }

  def saveLoCPerIterationAndRepo(experimentPath: Path, numIterations: Int, rootName: String) = {
    val initFolder = experimentPath.resolve("v0").resolve(rootName).toFile.listFiles.toList.find(file => file.isDirectory).get.getName
    val dbInitScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", s"0-$initFolder", "--timeout", "0", experimentPath.resolve("v0").toString), None,"PATH" -> "").!!
    val initChunks = chunkDbScript(dbInitScript, true)
    Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + initChunks(0) + "\""), None, "PATH" -> "").!
    for (chunk <- initChunks.drop(1)) {
      try {
        Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""), None, "PATH" -> "").!
      } catch {
        case e : Exception =>
          println("Chunk resulting in an exception during execution of sqlite command: " + chunk)
          throw e
      }
    }

    for {
      iteration <- 1 to numIterations
      folder = experimentPath.resolve(s"v$iteration").resolve(rootName)
      repoFolder <- folder.toFile.listFiles
      if repoFolder.isDirectory
    } yield {
      val additionScript = Process(Seq(clocPath, "--sql", "1", "--sql-project", s"$iteration-${repoFolder.getName}", "--sql-append", "--timeout", "0", repoFolder.toString), None,"PATH" -> "").!!
      val appendChunks = chunkDbScript(additionScript, false)
      for (chunk <- appendChunks) {
        try {
        Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "\"" + chunk + "\""), None, "PATH" -> "").!
        } catch {
          case e : Exception =>
            println("Chunk resulting in an exception during execution of sqlite command: " + chunk)
            throw e
        }
      }
    }
    val csvFile = experimentPath.resolve("loc.csv")
    val sqlQuery = "\"select project as iterationRepo, sum(nCode) as loc from t where t.language = 'Java' group by project, language\""
    val csvContent = Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "-header", "-csv", sqlQuery), None, "PATH" -> "").!!
    Files.writeString(csvFile, csvContent)
  }

  def saveLoCToCsv(experimentPath: Path) = {
    val csvFile = experimentPath.resolve("loc.csv")
    val sqlQuery = "\"select project as iterationRepo, sum(nCode) as loc from t where t.language = 'Java' group by project, language\""
    val csvContent = Process(Seq(sqlitePath, experimentPath.resolve("loc.db").toString, "-header", "-csv", sqlQuery), None, "PATH" -> "").!!
    Files.writeString(csvFile, csvContent)
  }

  def postprocessJavaParsing(ast: Asset, insertionPointLimitations: List[Asset]) : Unit = {
    val assetsToProcess = transformASTToList(ast)
      .filter(asset => asset.assetType == FileType)
      .filter(asset => isTransitiveChildOf(asset, insertionPointLimitations))

    for (asset <- assetsToProcess) {
      if (asset.children.length != 0) {
        val currentBlock = asset.children(0)
        val packageBlock = Asset("package", BlockType, content = Some(currentBlock.content.get(0) :: Nil))
        val secondBlock = Asset("block", BlockType, content = Some(currentBlock.content.get.drop(1)))
        packageBlock.parent = Some(asset)
        secondBlock.parent = Some(asset)

        asset.children = packageBlock :: secondBlock :: asset.children.drop(1)
      }
    }
  }

  def main(args: Array[String]) : Unit =
  {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm")
    val datetime = Calendar.getInstance().getTime()

    val targetpath = config.targetPath
    val targetdatepath = targetpath + "\\" + dateFormat.format(datetime)
    val targetdatedir = new File(targetdatepath)
    targetdatedir.mkdir()

    val workingDirectory = config.workingDirectory

    val iterations = 500
    val initialProjects = config.initialProjects.map(proj => (proj.projectPath, proj.insertionPointFilters.map(filter => Paths.get(workingDirectory).resolve(filter).toString)))

    val probDistributions = List((1, Array(0.1088886,0.1088886,0.1088886,0.3266671,0.3266671,0.01,0.01)),(2, Array(0.196,0.196,0.196,0.196,0.196,0.01,0.01)),(3, Array(0.2,0.2,0.2,0.29,0.09,0.01,0.01)))
    val maxAttempts = 50
    val cloneFromOtherRepos = true
    val maxMappedAssetsCount = None

    val results = for {
      (readInPath, filterInsertionPoints) <- initialProjects
      (probDistributionNumber, probDistribution) <- probDistributions
      numIterations = iterations
    } yield {
      TraceDatabase.traces = Nil
      FeatureTraceDatabase.traces = Nil

      // Move the initial system into the working directory and read in from there.
      // Required since AddFeatureGenerator implicitly assumes that it can write into the selected assets name (= path).
      clearAndCopyInto(workingDirectory, readInPath)
      val relPathToRoot = readInPath.split("\\\\").last
      val ast = readInJavaAssetTree(workingDirectory + "\\" + relPathToRoot).get
      val rootName = ast.name.split("\\\\").last

      val relFilterInsertionPaths = for {
        filterInsPoint <- filterInsertionPoints
      } yield {
        val insPointAsset = getassetbyname(filterInsPoint, ast).get
        val insPointPath  = file.Paths.get(filterInsPoint)
        val insPointRepoPath  = file.Paths.get(findRepoOnUpPath(insPointAsset).get.name)
        val relRepoPath = insPointRepoPath.relativize(insPointPath)
        relRepoPath
      }

      val postProcessingPathAssets = filterInsertionPoints.map(name => getassetbyname(name, ast).get)
      postprocessJavaParsing(ast, postProcessingPathAssets)

      val initialProjectName = readInPath.split("\\\\").last

      val relPath = s"numIt_${numIterations}_${initialProjectName}_$probDistributionNumber"
      val experimentPath = file.Paths.get(targetdatedir.getAbsolutePath).resolve(relPath)
      Files.createDirectories(experimentPath)
      val addFeatureLoggingFile = experimentPath.resolve("addFeatureLog.csv")
      Files.createFile(addFeatureLoggingFile)

      // Initialise Transplantation Projects
      val structurizrProject = createStructurizrProj
      val disruptorProject = createDisruptorProj
      val algorithmsProject = createAlgorithmsProj
      val jadxProject = createJadxProj
      val rootProjects = List(structurizrProject, disruptorProject, algorithmsProject, jadxProject)

      val commonPath = Paths.get("D:\\Dokumente\\Promotion\\vpbench\\evaluation\\2023-02-24_10-42_transplantEval\\callability")
      structurizrProject.loadInTestcasesFromFile(commonPath.resolve("structurizr\\structurizr_idtestcases_for_generation.txt"))
      structurizrProject.getSubprojectByName("structurizr-client").get.loadInTestcasesFromFile(commonPath.resolve("structurizr\\structurizr-client_idtestcases_for_generation.txt"))
      structurizrProject.getSubprojectByName("structurizr-core").get.loadInTestcasesFromFile(commonPath.resolve("structurizr\\structurizr-core_idtestcases_for_generation.txt"))
      structurizrProject.getSubprojectByName("structurizr-examples").get.loadInTestcasesFromFile(commonPath.resolve("structurizr\\structurizr-examples_idtestcases_for_generation.txt"))
      disruptorProject.loadInTestcasesFromFile(commonPath.resolve("disruptor\\disruptor_idtestcases_for_generation.txt"))
      algorithmsProject.loadInTestcasesFromFile(commonPath.resolve("Algorithms\\Algorithms_idtestcases_for_generation.txt"))
      jadxProject.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-cli").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-cli_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-core").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-core_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-gui").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-gui_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-samples").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-samples_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-plugins").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-plugins_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-plugins:jadx-dex-input").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-dex-input_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-plugins:jadx-java-convert").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-java-convert_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-plugins:jadx-plugins-api").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-plugins-api_idtestcases_for_generation.txt"))
      jadxProject.getSubprojectByName("jadx-plugins:jadx-smali-input").get.loadInTestcasesFromFile(commonPath.resolve("jadx\\jadx-smali-input_idtestcases_for_generation.txt"))

      val addLineGenerator = AddLineGenerator(ast, config.generatorConfig.discardUselessChangesProbAL)
      val repLineGenerator = ReplaceLineGenerator(ast, config.generatorConfig.discardUselessChangesProbRL)
      val delLineGenerator = DeleteLineGenerator(ast, config.generatorConfig.discardUselessChangesProbDL)
      val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString, initialiseTestcases = false, loggingFile = Some(addFeatureLoggingFile.toFile))
      val remFeatureGenerator = RemoveFeatureGenerator(ast)
      val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
      val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory, maxMappedAssetCount = config.generatorConfig.maxMappedAssetCount)
      val generators = List(addLineGenerator, repLineGenerator, delLineGenerator, addFeatureGenerator, remFeatureGenerator, clnRepoGenerator, clnFeatGenerator)

      val compilationChecker = GradleCompilationChecker()
      val generatorProbabilityDist : Map[Generator, Double] = generators.zip(probDistribution).toMap

      val runner = new BenchmarkGenerationRunner(ast, workingDirectory, generators, compilationChecker, Some(generatorProbabilityDist))

      implicit val formats = Serialization.formats(FullTypeHints(List(classOf[AddLineToAsset], classOf[String], classOf[Parameters], classOf[Metadata], classOf[Project], classOf[Relationship]))) + FieldSerializer[OperationMetadata]()

      val paramsFilePath = experimentPath.resolve("params.json")
      Files.createFile(paramsFilePath)
      val parameters = Parameters(numIterations, readInPath, filterInsertionPoints, probDistributionNumber, probDistribution, maxAttempts, config.generatorConfig.discardUselessChangesProbAL, config.generatorConfig.discardUselessChangesProbDL, config.generatorConfig.discardUselessChangesProbRL, cloneFromOtherRepos, workingDirectory, maxMappedAssetsCount)
      val jsonString = write(parameters)
      Files.writeString(paramsFilePath, jsonString)

      // Prepare for writing loc per iteration and variant
      val locdb = experimentPath.resolve("loc.db")
      Files.createFile(locdb)

      val sqliteErrors = experimentPath.resolve("sqliteErrors.txt")
      Files.createFile(sqliteErrors)
      val fw = new FileWriter(sqliteErrors.toString, true)
      fw.write("Writing down errors during execution of sqlite commands")
      fw.close

      val timestampBegin = System.nanoTime
      val resultTuples = runner.run(numIterations, experimentPath.toString, None, maxAttempts = maxAttempts)
      val timestampEnd = System.nanoTime
      val duration = timestampEnd - timestampBegin

      addFeatureGenerator.closeFileWriter

      val results = resultTuples.map(tup => tup._1)
      val threads = resultTuples.map(tup => tup._2)

      val executedOperations = results
        .map(elem => (elem.iteration, elem.appliedTransaction))
        .filter(tuple => tuple._2.isDefined)
        .map(tuple => IterationMetadata(tuple._1, tuple._2.get.operations(0).metadata.get))
      val history = GenerationMetadata(rootProjects, executedOperations)

      val executedOperationHistory = write(history)
      val historyPath = experimentPath.resolve("history.json")
      Files.createFile(historyPath)
      Files.writeString(historyPath, executedOperationHistory)

      val numFeaturesOverTime = results.map(iterationResult => (iterationResult.iteration, iterationResult.absoluteFeatureCount, iterationResult.repoResults.map(res => res.numFeatures)))
      val scatteredFeaturesOverTimePerRepo = results.map(iterationResult =>
        (iterationResult.iteration, iterationResult.repoResults.map(repoRes =>
          if (repoRes.scatterDegPerFeature.isEmpty) { List[Int]() }
          else { repoRes.scatterDegPerFeature.map(tuple => tuple._2) }
        )))
      val tangledFeaturesOverTimePerRepo = results.map(iterationResult =>
        (iterationResult.iteration, iterationResult.repoResults.map(repoRes =>
          if (repoRes.tangleDegPerFeature.isEmpty) { List[Int]() }
          else { repoRes.tangleDegPerFeature.map(tuple => tuple._2) }
        )))

      // Doesnt work like that, since this does not recognize whether a removed feature is still contained in another repo
      //val numFeaturesOverTime = results.scanLeft(0)((uniqueFeatCount, itRes) => StatisticsCalculationHelper.changeFeatureCount(uniqueFeatCount, itRes)).drop(1)
      //assert(numFeaturesOverTimePerRepo.length == numFeaturesOverTime.length)
      //val numFeaturesOverTimeMerged = numFeaturesOverTimePerRepo.zip(numFeaturesOverTime)
      //val numFeaturesOverTimeCsv = "iteration, numFeatures, numFeatPerRepo" :: numFeaturesOverTimeMerged.map(it => s"${it._1._1}, ${it._2}, ${it._1._2.mkString(";")}")
      val numFeaturesOverTimeCsv = "iteration, absoluteFeatureCount, numFeatPerRepo" :: numFeaturesOverTime.map(it => s"${it._1}, ${it._2}, ${it._3.mkString(";")}")
      val numFeaturesCsvPath = experimentPath.resolve("numFeatures.csv")
      Files.createFile(numFeaturesCsvPath)
      Files.writeString(numFeaturesCsvPath, numFeaturesOverTimeCsv.mkString("\n"))

      for {
        repoIndex <- 0 to ast.children.length - 1
        repo = ast.children(repoIndex)
        repoName = file.Paths.get(ast.name).relativize(file.Paths.get(repo.name))
      } yield {
        val scatteringDegreesOverTime = scatteredFeaturesOverTimePerRepo.dropWhile(iteration => !iteration._2.isDefinedAt(repoIndex))
          .filter(iteration => iteration._2(repoIndex).length != 0)
          .map(it => (it._1, (it._2(repoIndex).sum.asInstanceOf[Double] / it._2(repoIndex).length.asInstanceOf[Double]), it._2(repoIndex).mkString(";")))
        val scatteredFeaturesOverTimeCsv = "iteration, avgScatDeg, scatDegs" :: scatteringDegreesOverTime.map(it => s"${it._1}, ${it._2}, ${it._3}")
        val scatteredFeaturesCsvPath = experimentPath.resolve(s"scatFeatures-$repoName.csv")
        Files.createFile(scatteredFeaturesCsvPath)
        Files.writeString(scatteredFeaturesCsvPath, scatteredFeaturesOverTimeCsv.mkString("\n"))

        val tanglingDegreesOverTime = tangledFeaturesOverTimePerRepo.dropWhile(iteration => !iteration._2.isDefinedAt(repoIndex))
          .filter(iteration => iteration._2(repoIndex).length != 0)
          .map(it => (it._1, (it._2(repoIndex).sum.asInstanceOf[Double] / it._2(repoIndex).length.asInstanceOf[Double]), it._2(repoIndex).mkString(";")))
        val tangledFeaturesOverTimeCsv = "iteration, avgTangDeg, tangDegs" :: tanglingDegreesOverTime.map(it => s"${it._1}, ${it._2}, ${it._3}")
        val tangledFeaturesCsvPath = experimentPath.resolve(s"tangFeatures-$repoName.csv")
        Files.createFile(tangledFeaturesCsvPath)
        Files.writeString(tangledFeaturesCsvPath, tangledFeaturesOverTimeCsv.mkString("\n"))
      }

      for (thread <- threads) {
        thread.join
      }
      saveLoCToCsv(experimentPath)

      val resultsFilePath = experimentPath.resolve("aggregatedStatistics.json")
      val numCompiledVersions = results.count(it => it.repoResults.filter(repoResult => repoResult.compiled).length == it.repoResults.length)
      val numChanges = results.filter(it => it.changed).length
      val aggStats = AggregatedResults(duration, numberOfCompiledIterations = numCompiledVersions, numberOfAppliedChanges = numChanges)
      val resultsString = write(aggStats)
      Files.writeString(resultsFilePath, resultsString)
    }
  }
}
