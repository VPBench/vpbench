package vp.bench.bg

import java.io.File
import java.nio.file
import java.nio.file.{Files, Path, Paths, StandardOpenOption}
import java.text.SimpleDateFormat
import java.util.Calendar

import se.gu.vp.model._
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Utilities.{findAssetByName, getassetbyname, transformASTToList}
import vp.bench.bg.TestConfigurationProvider.{createAlgorithmsProj, createDisruptorProj, createJadxProj, createStructurizrProj}
import vp.bench.bg.Utilities.{clearAndCopyInto, copySafely, findRepoOnUpPath, isTransitiveChildOf}
import vp.bench.bg.generators._
import vp.bench.bg.model.TestCase.{parseDotRepresentationToTestcase, parseLineToTestcase}
import vp.bench.bg.model.{JavaProject, TestCase}
import vp.bench.bg.setup.ConfigurationReader

import scala.collection.JavaConversions._
import scala.collection.immutable.ListMap

object RunTransplantationEvaluation {

  // Read in configuration
  val configFile = file.Paths.get("configTransplantEval.json")
  val config = ConfigurationReader.readConfiguration(configFile)

  def postprocessJavaParsing(ast: Asset, insertionPointLimitations: List[Asset]) : Unit = {
    val assetsToProcess = transformASTToList(ast)
      .filter(asset => asset.assetType == FileType)
      .filter(asset => isTransitiveChildOf(asset, insertionPointLimitations))

    for (asset <- assetsToProcess) {
      if (asset.children.length != 0) {
        val currentBlock = asset.children(0)
        val packageBlock = Asset("package", BlockType, content = Some(currentBlock.content.get(0) :: Nil))
        val secondBlock = Asset("block", BlockType, content = Some(currentBlock.content.get.drop(1)))
        packageBlock.parent = Some(asset)
        secondBlock.parent = Some(asset)

        asset.children = packageBlock :: secondBlock :: asset.children.drop(1)
      }
    }
  }

  def createAddFeatureGenWLogging(targetDateDir: File, guaranteeCallability: Boolean, donorProject: JavaProject, ast: Asset, targetPathString : String, initialiseTestcases: Boolean, loggingPath: Path) : AddFeatureGenerator = {
    val fileSuffix = if (guaranteeCallability) {
      "callability"
    } else {
      "compilability"
    }
    val relPath = s"testTransplants_${donorProject.name}_$fileSuffix.csv"
    val experimentPath = loggingPath.resolve(relPath)
    val loggingFile = Files.createFile(experimentPath)

    AddFeatureGenerator(
      rootAsset = ast,
      rootProjects = List(donorProject),
      targetPathString = targetPathString,
      unitTestScribePathString = config.generatorConfig.unitTestScribePathString,
      initialisationPathString = config.generatorConfig.initialisationPathString,
      srcmlPathString = config.generatorConfig.srcmlPathString,
      jdepsPathString = config.generatorConfig.jdepsPathString,
      guaranteeCallability = guaranteeCallability,
      loggingFile = Some(loggingFile.toFile),
      initialiseTestcases = initialiseTestcases
    )
  }

  def addFeatureAndReset(generator: AddFeatureGenerator, insertionPoint: Tuple2[Asset, Int], parentFeature: Feature, subProject: JavaProject, testCase: TestCase, donorProject: JavaProject) = {
    val output = generator.addFeature(insertionPoint = insertionPoint, subProject = subProject, testCase = testCase, parentFeature = parentFeature)
    donorProject.listAllProjectsRecursively.foreach(proj => proj.pathToGenTargetInRepo = ListMap[Asset, Path]())
    output
  }

  def readAndParseTestcases(file: Option[Path]) : List[TestCase] = {
    if (file.isDefined) {
      Files.readAllLines(file.get)
        .map(line => parseLineToTestcase(line))
        .toList
    } else { Nil }
  }

  def main(args: Array[String]) : Unit =
  {
    val dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm")
    val datetime = Calendar.getInstance().getTime()

    val targetpath = config.targetPath
    val targetdatepath = targetpath + "\\" + dateFormat.format(datetime) + "_transplantEval"
    val targetdatedir = new File(targetdatepath)
    targetdatedir.mkdir

    val inputstatspath = Paths.get(targetdatepath).resolve("statistics")
    inputstatspath.toFile.mkdir

    val compilabilitypath = Paths.get(targetdatepath).resolve("compilability")
    compilabilitypath.toFile.mkdir

    val callabilitypath = Paths.get(targetdatepath).resolve("callability")
    callabilitypath.toFile.mkdir

    val workingDirectory = config.workingDirectory

    val initialProjects = config.initialProjects.map(proj => (proj.projectPath, proj.insertionPointFilters.map(filter => Paths.get(workingDirectory).resolve(filter).toString)))
    val (readInPath, filterInsertionPoints) = initialProjects(0)

    // Move the initial system into the working directory and read in from there.
    // Required since AddFeatureGenerator implicitly assumes that it can write into the selected assets name (= path).
    clearAndCopyInto(workingDirectory, readInPath)
    val relPathToRoot = readInPath.split("\\\\").last
    val ast = readInJavaAssetTree(workingDirectory + "\\" + relPathToRoot).get

    val relFilterInsertionPaths = for {
      filterInsPoint <- filterInsertionPoints
    } yield {
      val insPointAsset = getassetbyname(filterInsPoint, ast).get
      val insPointPath  = file.Paths.get(filterInsPoint)
      val insPointRepoPath  = file.Paths.get(findRepoOnUpPath(insPointAsset).get.name)
      val relRepoPath = insPointRepoPath.relativize(insPointPath)
      relRepoPath
    }

    val postProcessingPathAssets = filterInsertionPoints.map(name => getassetbyname(name, ast).get)
    postprocessJavaParsing(ast, postProcessingPathAssets)

    // Hardcoded for now
    val repoAsset = findAssetByName(workingDirectory + "\\" + "VanillaTestbed\\VanillaRepo", ast).get
    val fileAsset = findAssetByName(workingDirectory + "\\" + "VanillaTestbed\\VanillaRepo\\src\\main\\java\\transplantationEval\\TransplantationTarget.java", ast).get
    val insertionPoint = (fileAsset.children(2).children(0), 0)

    val featureModel = repoAsset.featureModel.get
    val parentFeature = featureModel.rootfeature

    // Initialise Transplantation Projects
    val structurizrProject = createStructurizrProj
    val disruptorProject = createDisruptorProj
    val algorithmsProject = createAlgorithmsProj
    val jadxProject = createJadxProj
    val rootProjects = List(structurizrProject, disruptorProject, algorithmsProject, jadxProject)

    val initialisationPath = Paths.get(config.generatorConfig.initialisationPathString)
    val unitTestScribePath = Paths.get(config.generatorConfig.unitTestScribePathString)
    val srcmlPath = Paths.get(config.generatorConfig.srcmlPathString)

    val results = for {
      donorProject <- rootProjects
      projStatsPath = inputstatspath.resolve(donorProject.name)
      projCompPath = compilabilitypath.resolve(donorProject.name)
      projCallPath = callabilitypath.resolve(donorProject.name)
    } yield {
      projStatsPath.toFile.mkdir
      projCompPath.toFile.mkdir
      projCallPath.toFile.mkdir

      // Store all testcases before running per project
      val testcaseFilePerProj = donorProject.listAllProjectsRecursively.map(p => (p, p.getTestcaseListFromProject(initialisationPath, unitTestScribePath, srcmlPath)))
      for (tuple <- testcaseFilePerProj) {
        val newPath = projStatsPath.resolve(tuple._1.name + "_alltestcases.txt").toFile
        if (tuple._2.isDefined) {
          val copiedToPath = projStatsPath.resolve(tuple._1.name + "_" + tuple._2.get.getFileName.toString)
          copySafely(tuple._2.get, copiedToPath)
        } else {
          newPath.createNewFile
        }
      }

      val testcasesPerProj = testcaseFilePerProj
        .map(tuple => (tuple._1, readAndParseTestcases(tuple._2))).toMap

      // Get a map from project to a map from testclass to testcases
      val testcasesPerClass = testcasesPerProj.map(kv => (kv._1, kv._2.groupBy(v => v.packageName + "." + v.className)))
      val testcaseCountPerClass = testcasesPerClass.map(kvv => (kvv._1, kvv._2.map(kv => (kv._1, kv._2.length))))

      // Get a map from project to a map from testclass to a single testcase
      val stcPerClass = testcasesPerClass.map(kv => (kv._1, kv._2.map(v => (v._1, v._2.head))))

      // Read in single testcase per testclass
      stcPerClass.foreach(kvv => kvv._1.testcases = kvv._2.values.toList)

      val addFeatureGeneratorNotCallable = createAddFeatureGenWLogging(targetDateDir = targetdatedir, guaranteeCallability = false, donorProject = donorProject, ast = ast, targetPathString = workingDirectory, initialiseTestcases = false, loggingPath = projCompPath)

      val transplantationCompChecked = donorProject.listAllProjectsRecursively
        .flatMap(subProject => subProject.testcases.map(
          tc => addFeatureAndReset(generator = addFeatureGeneratorNotCallable, insertionPoint = insertionPoint, parentFeature = parentFeature, subProject = subProject, testCase = tc, donorProject = donorProject))
        )

      addFeatureGeneratorNotCallable.closeFileWriter

      val successComp = transplantationCompChecked.filter(op => op.isDefined).length
      val failedComp = transplantationCompChecked.length - successComp

      val loggingFileComp = addFeatureGeneratorNotCallable.loggingFile.get.toPath
      val loggingFileCompParsedTestcases = Files.readAllLines(loggingFileComp)
        .map(line => line.split(';'))
        .map(line => (line(0), line(1), line(2)))
        .map(line => (line._1, parseDotRepresentationToTestcase(line._2), line._3))

      def mapToBoolean(input: String) : Boolean = {
        try {
          input.toBoolean
        } catch {
          case e: Exception =>
            false
        }
      }

      val loggingFileCompResults = loggingFileCompParsedTestcases
        .map(line => (line._1, line._2.packageName + "." + line._2.className, line._2, line._3))
        .map(line => (line._1, line._2, testcaseCountPerClass.map(kv => (kv._1.name, kv._2)).get(line._1).get.get(line._2).get, line._3, mapToBoolean(line._4)))

      val successCompTestcases = loggingFileCompResults.filter(line => line._5).map(line => line._3).sum
      val failedCompTestcases = loggingFileCompResults.filterNot(line => line._5).map(line => line._3).sum

      val loggingFileCompResultsFile = projCompPath.resolve(s"testTransplants_${donorProject.name}_compilability_enriched_results.csv")

      Files.write(loggingFileCompResultsFile, loggingFileCompResults.map(line => s"${line._1};${line._2};${line._3};${line._4};${line._5}"), StandardOpenOption.CREATE_NEW)
      val filterSuccessfulCompTransplantations = loggingFileCompResults.filter(line => line._5)

      // Prepare all testcases from compiling testclasses
      for (subProj <- donorProject.listAllProjectsRecursively) {
        val successfulTestClasses = filterSuccessfulCompTransplantations.filter(line => line._1 == subProj.name).map(line => line._2)
        val potentialTestcases = successfulTestClasses.flatMap(testclass => testcasesPerClass.get(subProj).get.get(testclass).get)
        subProj.testcases = potentialTestcases
      }

      val addFeatureGeneratorCallable = createAddFeatureGenWLogging(targetDateDir = targetdatedir, guaranteeCallability = true, donorProject = donorProject, ast = ast, targetPathString = workingDirectory, initialiseTestcases = false, loggingPath = projCallPath)
      val transplantationCallChecked = donorProject.listAllProjectsRecursively
        .flatMap(subProject => subProject.testcases.map(
          tc => addFeatureAndReset(generator = addFeatureGeneratorCallable, insertionPoint = insertionPoint, parentFeature = parentFeature, subProject = subProject, testCase = tc, donorProject = donorProject))
        )

      addFeatureGeneratorCallable.closeFileWriter

      val successCall = transplantationCallChecked.filter(op => op.isDefined).length
      val failedCall = transplantationCallChecked.length - successCall

      val loggingFileCall = addFeatureGeneratorCallable.loggingFile.get.toPath
      val loggingFileCallParsedTestcases = Files.readAllLines(loggingFileCall)
        .map(line => line.split(';'))
        .map(line => (line(0), line(1), line(2)))
        .map(line => (line._1, parseDotRepresentationToTestcase(line._2), line._3))

      val loggingFileCallResultsTotal = loggingFileCallParsedTestcases
        .map(line => (line._1, line._2.packageName + "." + line._2.className, line._2, line._3))
        .map(line => (line._1, line._2, line._3, mapToBoolean(line._4)))

      val loggingFileCallResultsPerClass = loggingFileCallResultsTotal
        .groupBy(line => (line._1, line._2))
        .map(tuple => (tuple._1, tuple._2.count(line => line._4)))
        .toList

      val loggingFileCallResultsFile = projCallPath.resolve(s"testTransplants_${donorProject.name}_callability_per_testclass_results.csv")

      Files.write(loggingFileCallResultsFile, loggingFileCallResultsPerClass.map(line => s"${line._1._1};${line._1._2};${line._2}"), StandardOpenOption.CREATE_NEW)

      // Prepare testcases for version history generation
      for (subProj <- donorProject.listAllProjectsRecursively) {
        val successfulTestCases = loggingFileCallResultsTotal.filter(line => line._4).filter(line => line._1 == subProj.name).map(line => line._3)
        val transplantableTestcasesString = successfulTestCases.map(tc => tc.packageName + "  " + tc.className + "  " + tc.testName)

        val projTestcaseFileForVHGen = projCallPath.resolve(s"${subProj.name}_idtestcases_for_generation.txt")
        Files.write(projTestcaseFileForVHGen, transplantableTestcasesString, StandardOpenOption.CREATE_NEW)
      }

      (donorProject.name, transplantationCompChecked.length, successComp, failedComp, successCompTestcases, failedCompTestcases, transplantationCallChecked.length, successCall, failedCall)
    }

    val resultSummaryPath = targetdatedir.toPath.resolve("transplantationSummary.csv")
    val resultLines = results.map(tuple => s"${tuple._1};${tuple._2};${tuple._3};${tuple._4};${tuple._5};${tuple._6};${tuple._7};${tuple._8};${tuple._9}")
    val prependedHeader = "donorProject;triedTestclasses;successClasses;failClasses;successCasesForComp;failedCasesForComp;triedTestcases;successCases;failCases" :: resultLines
    Files.writeString(resultSummaryPath, prependedHeader.mkString("\n"))
  }
}
