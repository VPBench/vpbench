package vp.bench.bg

import se.gu.vp.operations.{Operation, RemoveFeature}
import vp.bench.bg.evaluation.IterationResults
import vp.bench.bg.operations.AddExternalFeature
import vp.operations.AddExternalAsset

object StatisticsCalculationHelper {
  def convertOperationToUniqueFeatureCountChange(op: Operation) = {
    op match {
      case _ : AddExternalFeature => 1
      case _ : RemoveFeature => -1
      case _ => 0
    }
  }

  def changeFeatureCount(uniqueFeatureCount: Int, itResult: IterationResults) : Int = {
    if (itResult.appliedTransaction.isDefined) {
      uniqueFeatureCount + itResult.appliedTransaction.get.operations.map(op => convertOperationToUniqueFeatureCountChange(op)).sum
    } else {
      uniqueFeatureCount
    }
  }
}
