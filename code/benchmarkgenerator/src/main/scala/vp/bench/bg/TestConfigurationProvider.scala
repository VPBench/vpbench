package vp.bench.bg

import java.nio.file
import java.nio.file.{Path, Paths}

import vp.bench.bg.model.{JavaProject}

object TestConfigurationProvider {
  def createJadxPluginsSmaliInputProj() : JavaProject = {
    val pathString = "jadx-smali-input"
    val subprojects = Nil
    val name = "jadx-smali-input"
    val jar = Some(file.Paths.get("build\\libs\\jadx-smali-input-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsPluginsApiProj() : JavaProject = {
    val pathString = "jadx-plugins-api"
    val subprojects = Nil
    val name = "jadx-plugins-api"
    val jar = Some(file.Paths.get("build\\libs\\jadx-plugins-api-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsJavaConvertProj() : JavaProject = {
    val pathString = "jadx-java-convert"
    val subprojects = Nil
    val name = "jadx-java-convert"
    val jar = Some(file.Paths.get("build\\libs\\jadx-java-convert-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsDexInputProj() : JavaProject = {
    val pathString = "jadx-dex-input"
    val subprojects = Nil
    val name = "jadx-dex-input"
    val jar = Some(file.Paths.get("build\\libs\\jadx-dex-input-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxPluginsProj() : JavaProject = {
    val pathString = "jadx-plugins"
    val jadxPluginsDexInputProj = createJadxPluginsDexInputProj()
    val jadxPluginsJavaConvertProj = createJadxPluginsJavaConvertProj()
    val jadxPluginsPluginsApiProj = createJadxPluginsPluginsApiProj()
    val jadxPluginsSmaliInputProj = createJadxPluginsSmaliInputProj()
    val subprojects = Seq(jadxPluginsDexInputProj,jadxPluginsJavaConvertProj, jadxPluginsPluginsApiProj, jadxPluginsSmaliInputProj)
    val name = "jadx-plugins"
    val jar = Some(file.Paths.get("build\\libs\\jadx-plugins-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }


  def createJadxSamplesProj() : JavaProject = {
    val pathString = "jadx-samples"
    val subprojects = Nil
    val name = "jadx-samples"
    val jar = Some(file.Paths.get("build\\libs\\samples-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxGuiProj() : JavaProject = {
    val pathString = "jadx-gui"
    val subprojects = Nil
    val name = "jadx-gui"
    val jar = Some(file.Paths.get("build\\libs\\jadx-gui-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxCoreProj() : JavaProject = {
    val pathString = "jadx-core"
    val subprojects = Nil
    val name = "jadx-core"
    val jar = Some(file.Paths.get("build\\libs\\jadx-core-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxCliProj() : JavaProject = {
    val pathString = "jadx-cli"
    val subprojects = Nil
    val name = "jadx-cli"
    val jar = Some(file.Paths.get("build\\libs\\jadx-cli-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createJadxProj() : JavaProject = {
    val pathString = "D:\\Dokumente\\Promotion\\vpbench\\projects\\skylot\\jadx"
    val name = "jadx"
    val jar = Some(file.Paths.get("build\\libs\\jadx-dev.jar"))
    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    val jadxCliProj = createJadxCliProj()
    val jadxCoreProj = createJadxCoreProj()
    val jadxGuiProj = createJadxGuiProj()
    val jadxSamplesProj = createJadxSamplesProj()
    val jadxPluginsProj = createJadxPluginsProj()
    val subprojects = Seq(jadxCliProj,jadxCoreProj,jadxGuiProj,jadxSamplesProj,jadxPluginsProj)

    val project = JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
    project.subprojects.foreach(sub => sub.parent = Some(project))
    project
  }

  def createStructurizrExamplesProj() : JavaProject = {
    val pathString = "structurizr-examples"
    val subprojects = Nil
    val name = "structurizr-examples"
    val jar = Some(file.Paths.get("build/libs/structurizr-examples-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrCoreProj() : JavaProject = {
    val pathString = "structurizr-core"
    val subprojects = Nil
    val name = "structurizr-core"
    val jar = Some(file.Paths.get("build/libs/structurizr-core-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrClientProj() : JavaProject = {
    val pathString = "structurizr-client"
    val subprojects = Nil
    val name = "structurizr-client"
    val jar = Some(file.Paths.get("build/libs/structurizr-client-1.8.1.jar"))
    val srcPath = file.Paths.get("src")
    val testPath = file.Paths.get("test\\unit")
    new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
  }

  def createStructurizrProj() : JavaProject =
    {
      val pathString = "D:\\Dokumente\\Promotion\\vpbench\\projects\\Structurizr\\java"
      val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Promotion\\vpbench\\projects\\Structurizr\\java")
      val structurizrClientProj = createStructurizrClientProj()
      val structurizrCoreProj = createStructurizrCoreProj()
      val structurizrExamplesProj = createStructurizrExamplesProj()
      val name = "structurizr"
      val jar = None
      val srcPath = file.Paths.get("src")
      val testPath = file.Paths.get("test\\unit")

      val project = new JavaProject(pathString, Seq(structurizrClientProj,structurizrCoreProj,structurizrExamplesProj), name, jar, srcPath, testPath)      
      project.subprojects.foreach(sub => sub.parent = Some(project))
      project
    }

  def createDisruptorProj() : JavaProject = {
    val pathToGitRepo = file.Paths.get("D:\\Dokumente\\Promotion\\vpbench\\projects\\LMAXDisruptor\\disruptor")
    val pathString = "D:\\Dokumente\\Promotion\\vpbench\\projects\\LMAXDisruptor\\disruptor"
    val subprojects = Nil
    val name = "disruptor"
    val jar = Some(file.Paths.get("build/libs/disruptor-4.0.0-SNAPSHOT.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
  }

  def createAlgorithmsProj() : JavaProject = {
    val pathString = "D:\\Dokumente\\Promotion\\vpbench\\projects\\Algorithms\\Algorithms"
    val subprojects = Nil
    val name = "Algorithms"
    val jar = Some(file.Paths.get("build/libs/Algorithms.jar"))

    val srcPath = file.Paths.get("src\\main\\java")
    val testPath = file.Paths.get("src\\test\\java")

    new JavaProject(pathString, subprojects, name, jar, srcPath, testPath)
  }
}
