package vp.bench.bg

import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle

object TestExecuteGradle {
  def main(args: Array[String]): Unit = {
    val path = "D:\\Dokumente\\Entwicklung\\Eclipse_Workspace_J11\\GitClones\\Structurizr\\java"
    val all = executeGradle(path, "clean", "compileJava", "compileTestJava")
    val clean = executeGradle(path, "clean")
    val compileJ = executeGradle(path, "compileJava")
    val compileT = executeGradle(path, "compileTestJava")
    val debugStop = 1
  }
}
