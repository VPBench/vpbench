package vp.bench.bg

import java.nio.file
import java.nio.file.Paths

import vp.bench.bg.RunEvaluation.config
import vp.bench.bg.generators.{AddFeatureGenerator, RemoveFeatureGenerator}
import vp.bench.bg.setup.ConfigurationReader

//import se.gu.bg.TestConfigurationProvider.{createDisruptorProj, createJadxProj, createReactiveXProj, createStructurizrProj}
import TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.generators.RemoveFeatureGenerator
import se.gu.vp.model.TraceDatabase
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.readInAssetTree
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.SerialiseAssetTree
import se.gu.vp.operations.Utilities.findAssetByName

// Non-automated test: basically requires manual checking
object TestSerializeAfterTransplant {
  def main(args: Array[String]): Unit = {
    // Read in configuration
    val configFile = file.Paths.get("configTest.json")
    val config = ConfigurationReader.readConfiguration(configFile)

    val initialProjects = config.initialProjects.map(proj => (proj.projectPath, proj.insertionPointFilters.map(filter => Paths.get(config.workingDirectory).resolve(filter).toString)))
    val readInPath = initialProjects(0)._1

    val ast = readInJavaAssetTree(readInPath).get
    val featureModel = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\BasicCalculator", ast).get.featureModel.get
    val parentAsset1 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\BasicCalculator\\src\\main\\java\\org\\easelab\\calculatorexample\\basiccalculator\\BasicCalculator.java", ast).get.children(1).children(2)
    val parentAsset2 = findAssetByName("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\BasicCalculator\\src\\main\\java\\org\\easelab\\calculatorexample\\basiccalculator\\Main.java", ast).get.children(1).children(0)
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val structurizrProject = createStructurizrProj()
    val disruptorProject = createDisruptorProj()
    val rootProjects = List(structurizrProject, disruptorProject)

    // sets no filterInsertionPoints - TODO: Why? Can this be unified with the other tests?
    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = config.workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val removeFeatureGenerator = RemoveFeatureGenerator(ast)

    val structurizrCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val structurizrClient = structurizrProject.getSubprojectByName("structurizr-client").get
    //contains enum issue in EncryptionLocation.
    val encryptedJsonTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.io.json" && tc.className == "EncryptedJsonTests" && tc.testName == "test_write_and_read").get
    val decryptNotTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.encryption" && tc.className == "AesEncryptionStrategyTests" && tc.testName == "test_decrypt_doesNotDecryptTheCiphertext_WhenTheIncorrectKeySizeIsUsed").get
    // problem, that Model.java is not entirely written out (-> file is very long stopped after 27477 characters, file length is 50142 characters -> check if everything is part of assetTree?)
    val anotherTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.io.json" && tc.className == "EncryptedJsonWriterTests" && tc.testName == "test_write_ThrowsAnIllegalArgumentException_WhenANullWriterIsSpecified").get
    // reverted Vertex problem
    val vertexTest = structurizrCore.testcases.find(tc => tc.packageName == "com.structurizr.view" && tc.className == "VertexTests" && tc.testName == "test_equals").get
    val apiStructurizrClientTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "StructurizrClientTests" && tc.testName == "test_construction_WithTwoParameters").get
    val apiResponseTest = structurizrClient.testcases.find(tc => tc.packageName == "com.structurizr.api" && tc.className == "ApiResponseTests" && tc.testName == "test_parse_createsAnApiErrorObjectWithTheSpecifiedErrorMessage").get
    val viewSetTest = structurizrCore.testcases.find(tc => tc.packageName == "com.structurizr.view" && tc.className == "ViewSetTests" && tc.testName == "test_createContainerView_ThrowsAnException_WhenADuplicateKeyIsSpecified").get
    val disruptorTest = disruptorProject.testcases.find(tc => tc.packageName == "com.lmax.disruptor" && tc.className == "FixedSequenceGroupTest" && tc.testName == "shouldReturnMinimumOf2Sequences").get

    addFeatureGenerator.extractTestcase((parentAsset2,3),structurizrClient, encryptedJsonTest, parentFeature)
    addFeatureGenerator.extractTestcase((parentAsset1,0),disruptorProject, disruptorTest, parentFeature)
    addFeatureGenerator.extractTestcase((parentAsset2,1),structurizrClient, anotherTest, parentFeature)
    val listOfTraces = TraceDatabase.traces

    SerialiseAssetTree(ast, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
  }
}
