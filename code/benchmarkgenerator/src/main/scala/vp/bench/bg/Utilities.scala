package vp.bench.bg

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{findFileParentAsset, resolveAssetPath, resolveFeaturePath}
import java.nio.file.{DirectoryNotEmptyException, FileSystemException, Files, Paths, Path => FilePath}
import java.util.Comparator

import vp.bench.bg.operations.AddExternalFeature
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.getListOfSubDirectories
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Replay.AssetHelpers
import se.gu.vp.operations.{AddAsset, Operation, RemoveFeature, SerialiseAssetTree, Utilities}
import vp.bench.bg.model.Project

import scala.io.Source
import scala.io.Source.fromFile
import scala.reflect.io.Directory

object Utilities {

  def findEarlierOccurence(string: String, char1: Char, char2: Char) : Char = {
    val char1Ind = string.indexOf(char1) match {
      case -1 => string.length
      case value => value
    }
    val char2Ind = string.indexOf(char2) match {
      case -1 => string.length
      case value => value
    }

    if (char1Ind < char2Ind)
      {
        char1
      }
    else
      {
        char2
      }
  }

  def getAllIndicesOf(string: String, search: String) : List[Int] = {
    val index = string.lastIndexOf(search)
    index match {
      case -1 => List()
      case _ =>
        val nextSubstring = string.substring(0,index)
        getAllIndicesOf(nextSubstring,search) ++ List(index)
    }
  }

  def replaceStringSlice(string: String, replacement: String, startInd: Int, endInd: Int) : String = {
    assert(endInd >= startInd)

    val (begin,temp) = string.splitAt(startInd)
    val (_,end) = temp.splitAt(endInd - startInd)
    s"$begin$replacement$end"
  }

  // not really required
  def filterOutAssetType(assets: List[Asset], assetType:AssetType) : List[Asset] =
  {
    assets.filter(a => a.assetType != assetType)
  }

  def filterOutAssetTypes(assets: List[Asset], assetTypes:Set[AssetType]) : List[Asset] =
  {
    assets.filter(a => !assetTypes.contains(a.assetType))
  }

  // taken and split from CloneFeature-implementation in VP
  def resolveFeatureFromPath(path: Path) : Option[Feature] = {
    val fm = resolveFeatureModelFromPath(path)
    fm match {
      case None => None
      case Some(fm) =>
        val featurepath = path.path.collect { case a: FeaturePathElement => a }
        resolveFeaturePath(fm,featurepath)
    }
  }

  // taken and split from CloneFeature-implementation in VP
  def resolveFeatureModelFromPath(path: Path) : Option[FeatureModel] = {
    val assetpath = path.path.collect { case a: AssetPathElement => a }
    val sourceassetpath = resolveAssetPath(path.root,assetpath)
    sourceassetpath.last.featureModel
  }

  def createDictIfNotExists(path : FilePath) : Unit = {
    if (!Files.exists(path))
    { Files.createDirectories(path) }
  }

  // Copies src to target, if target does not yet exist
  def copySafely(src : FilePath, target : FilePath) = {
    createDictIfNotExists(target.getParent)
    if (!Files.exists(target))
    { Files.copy(src,target) }
  }

  // A Repository is a folder, too
  def findFolderParentAsset(asset: Asset): Option[Asset] = {
    asset.assetType match {
      case FolderType => Some(asset)
      case RepositoryType => Some(asset)
      case VPRootType => None
      case _ => asset.parent match {
        case None => None
        case Some(parent) => findFileParentAsset(parent)
      }
    }
  }

  def findRepoOnUpPath(asset: Asset) : Option[Asset] = {
    asset.assetType match {
      case RepositoryType => Some(asset)
      case VPRootType => None
      case _ => asset.parent match {
        case None => None
        case Some(parent) => findRepoOnUpPath(parent)
      }
    }
  }

  def almostEqual(a: Double, b: Double, precision: Double) : Boolean = {
    precision >= Math.abs(a - b)
  }

  def pathInFolder(currentPath: FilePath, RecursionEndPath: FilePath, folderName: String) : Boolean = {
    currentPath match {
      case null
        => throw new IllegalArgumentException("RecursionEndPath was not on path of currentPath")
      case RecursionEndPath
        if currentPath.getFileName.toString == folderName
          => true
      case RecursionEndPath
          => false
      case _
        if currentPath.getFileName.toString == folderName
          => true
      case _
          => pathInFolder(currentPath.getParent, RecursionEndPath, folderName)
    }
  }

  def tryDeleteIfEmpty(path: FilePath) : Unit = {
    try {
      Files.delete(path)
    } catch {
      case e : DirectoryNotEmptyException =>
    }
  }

  def cleanDirectory(target: FilePath, maintainBuild: Boolean, attempt: Int = 0) : Unit = {
    try {
      if (maintainBuild) {
        Files.walk(target)
          .filter(elem => !(elem == target))
          .filter(elem => !pathInFolder(elem, target, "build"))
          .filter(elem => !pathInFolder(elem, target, ".gradle"))
          .sorted(Comparator.reverseOrder())
          .forEach(elem => tryDeleteIfEmpty(elem))
      } else {
        Files.walk(target)
          .filter(elem => !(elem == target))
          .sorted(Comparator.reverseOrder())
          .forEach(elem => Files.delete(elem))
      }
      println("Delete succeeded: true")
    } catch {
      case ex: FileSystemException =>
        if (attempt % 100 == 0)
          println(s"Exception during cleanDirectory. attempt $attempt: ${ex.getMessage}")
        Thread.sleep(3000)
        cleanDirectory(target, maintainBuild, attempt + 1)
    }
  }

  def isTransitiveChildOf(asset: Asset, parentCandidates: List[Asset]) : Boolean = {
    if (parentCandidates.contains(asset)) {
      true
    } else {
      asset.parent match {
        case None => false
        case Some(parent) => isTransitiveChildOf(parent, parentCandidates)
      }
    }
  }

  def copyDirectoryContent(src: FilePath, target: FilePath): Unit = {
    Files.walk(src, 1)
      .filter(elem => elem != src)
      .forEach(elem => {
        val relPath = elem.toString.split("\\\\").last
        Files.copy(elem,target.resolve(relPath))
        copyDirectoryContent(elem,target.resolve(relPath))
      })
  }

  def copyDirectory(src: FilePath, target: FilePath): Unit = {
    val directoryName = src.toString.split("\\\\").last
    val targetDirPath = target.resolve(directoryName)
    Files.copy(src, targetDirPath)
    copyDirectoryContent(src, targetDirPath)
  }

  def clearAndCopyInto(workingDir: String, initSystem: String) = {
    val workingDirPath = Paths.get(workingDir)
    val initSystemPath = Paths.get(initSystem)

    cleanDirectory(Paths.get(workingDir), false)
    copyDirectory(initSystemPath, workingDirPath)
  }

  def getImmediateAncestorProjectInRepo(rootProject: Project, repo: Asset, currentPath: java.nio.file.Path) : Option[Project] = {
    try {
      val originalProject = rootProject.listAllProjectsRecursively
        .map(proj => (proj, proj.getAbsolutePathTarget(repo)))
        .filter(tuple => tuple._2.exists(path => currentPath.startsWith(path)))
        .map(tuple => (tuple._1, tuple._2.get))
        .reduce((A, B) => if (A._2.getNameCount >= B._2.getNameCount) {
          A
        } else {
          B
        })
        ._1

      Some(originalProject)
    } catch {
      case _ : Exception => None
    }
  }

  def getOptionalAssetContent(opt: Option[Asset]) : Option[String] = {
    opt match {
      case None => None
      case Some(asset) =>
        if (asset.content.isDefined) {
          Some(asset.content.get.mkString("\n"))
        } else {
          None
        }
    }
  }

  def rebaseAssetPath(ast: Asset, path: String) : String = {
    path + "\\" + ast.name.split('\\').last
  }
}
