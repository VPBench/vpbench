package vp.bench.bg.convertToSafecopy

import java.util

import se.gu.vp.operations.Operation

trait ConvertToSafeCopy[Operation] {
  def convertToSafeCopy(op: Operation, safeCopyMap: util.IdentityHashMap[Object, Object]) : Operation
}
