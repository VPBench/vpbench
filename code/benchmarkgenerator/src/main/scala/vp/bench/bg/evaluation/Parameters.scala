package vp.bench.bg.evaluation

case class Parameters(numIterations: Int, readInPath: String, filterInsertionPoints: List[String], probDistributionNumber: Int, probDistribution: Array[Double], maxAttempts: Int, discardUselessChangesProbAL: Double, discardUselessChangesProbDL: Double, discardUselessChangesProbRL: Double, cloneFromOtherRepos: Boolean, workingDirectory: String, maxMappedAssetsCount: Option[Int])
