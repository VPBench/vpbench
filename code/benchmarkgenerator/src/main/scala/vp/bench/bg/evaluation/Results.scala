package vp.bench.bg.evaluation

import se.gu.vp.model.{Asset, Feature}
import se.gu.vp.operations.Operation
import vp.bench.bg.transaction.Transaction

case class RepositoryResults(repo: Asset, compiled: Boolean, numFeatures: Int, scatterDegPerFeature: Seq[Tuple2[Feature, Int]], tangleDegPerFeature: Seq[Tuple2[Feature, Int]])
case class IterationResults(iteration: Int, changed: Boolean, absoluteFeatureCount: Int, repoResults: Seq[RepositoryResults], appliedTransaction: Option[Transaction])
case class AggregatedResults(duration: Long, numberOfCompiledIterations: Int, numberOfAppliedChanges: Int)