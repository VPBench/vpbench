package vp.bench.bg.exceptions

case class DependencyManagementException(message: String) extends Exception