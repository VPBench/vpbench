package vp.bench.bg.exceptions

case class TestcaseExtractionException(message: String) extends Exception
