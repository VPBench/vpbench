package vp.bench.bg.generators

import java.io.{File, FileWriter}
import java.lang.Thread.sleep

import se.gu.vp.model.{Asset, AssetType, BlockType, ClassType, Feature, FileType, MethodType}
import se.gu.vp.operations.{AddAsset, AddFeatureToFeatureModel, MapAssetToFeature, SerialiseAssetTree}

import scala.sys.process.Process
import java.nio.file
import java.nio.file.{Files, Path, Paths}
import java.util
import java.util.Optional
import java.util.stream.Collectors

import vp.bench.bg.HandleJavaDependencies.{addNewDependency, addSubprojectsToSettings, copyGradleProjectWrapper, copyProject, findClosestAncestorFile, getDependenciesRecursively, mapClassToJava}

import scala.collection.JavaConversions._
import vp.bench.bg.Utilities.{cleanDirectory, copyDirectory, createDictIfNotExists, findFolderParentAsset, findRepoOnUpPath, getOptionalAssetContent, isTransitiveChildOf, rebaseAssetPath}
import vp.bench.bg.model.TestCase.parseLineToTestcase
import com.rits.cloning.Cloner
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.operations.Utilities.{findFileParentAsset, getImmediateAncestorFeatureModel, getassetbyname, transformASTToList}
import vp.bench.bg.exceptions.{DependencyManagementException, TestcaseExtractionException}
import vp.bench.bg.generators.fwgens.TransplantFeatureFWGenerator
import vp.bench.bg.model.{JavaProject, Project, ProjectDependency, TestCase, TestcaseOrgan, TransplantationCache}
import vp.bench.bg.operations.{AddExternalFeature, ITransplantFeatureViaTest}

import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.util.Random

class AddFeatureGenerator(override val rootAsset: Asset, override val rootProjects: Seq[JavaProject], val targetPathString: String, val unitTestScribePathString: String, val initialisationPathString: String, val srcmlPathString: String, val filterInsertionPoints: Option[List[Path]], val cloneFromOtherRepos: Boolean, val jdepsPathString: String, val initialiseTestcases: Boolean, val guaranteeCallability: Boolean, val loggingFile: Option[File]) extends TransplantFeatureFWGenerator {
  val targetPath = file.Paths.get(targetPathString)
  val unitTestScribePath = file.Paths.get(unitTestScribePathString)
  val initialisationPath = file.Paths.get(initialisationPathString)
  val srcmlPath = file.Paths.get(srcmlPathString)
  val jdepsPath = file.Paths.get(jdepsPathString)
  val seededRandom = new Random()

  val fileWriter = if (loggingFile.isDefined) {
    Some(new FileWriter(loggingFile.get, true))
  } else None

  initialise

  def closeFileWriter = {
    if (fileWriter.isDefined) {
      fileWriter.get.close
    }
  }

  def initialise: Unit = {
    if (initialiseTestcases) {
      for {
        rootProject <- rootProjects
        project <- rootProject.listAllProjectsRecursively
      } yield {
        project.testcases = project.getTestcaseListFromProject(initialisationPath, unitTestScribePath, srcmlPath) match {
          case None => List()
          case Some(file) =>
            Files.readAllLines(file)
              .map(line => parseLineToTestcase(line))
        }
      }
    }

    val listOfAllTestcases = for {
      rootProject <- rootProjects
      project <- rootProject.listAllProjectsRecursively
      testcase <- project.testcases
    } yield {
      testcase
    }

    //val alltestcases = listOfAllTestcases.map(tc => tc.testName).length
    val duplicates = listOfAllTestcases.groupBy(tc => tc.toString).filter(tuple => tuple._2.length != 1).size
    assert(duplicates == 0)
  }

  def extractFileContent(subProject: Project, testCase: TestCase): Path = {
    // Extract testcase and relevant in-file dependencies
    val loc = subProject.getAbsolutePathSrc().resolve(subProject.srcSetTest).toString
    val outputLoc = initialisationPath.resolve(subProject.getProjectPathIncludingRoot.replace(':', '\\')).resolve(testCase.toString.replace('.', '\\'))
    createDictIfNotExists(outputLoc)

    val extractInput = Seq(unitTestScribePath.toString,
      "extractTestcase",
      "--loc", loc,
      "--srcmlPath", srcmlPath.toString,
      "--outputLoc", outputLoc.toString,
      "--namespace", testCase.packageName,
      "--class", testCase.className,
      "--test", testCase.testName
    ).mkString(" ")
    val result = Process(Seq(unitTestScribePath.toString,
      "extractTestcase",
      "--loc", loc,
      "--srcmlPath", srcmlPath.toString,
      "--outputLoc", outputLoc.toString,
      "--namespace", testCase.packageName,
      "--class", testCase.className,
      "--test", testCase.testName
    )).!

    outputLoc
  }

  def extractOutOfFileDependencies(repo: Asset, repoPath: Path, subProject: JavaProject, testCase: TestCase, genSeededRandom: Random): Tuple2[Set[Tuple2[JavaProject,Seq[Path]]], Map[Path,Asset]] = {
    // Get dependencies
    val rootProject = subProject.getRootProject()
    val classFile = subProject.getAbsolutePathSrc.resolve("build\\classes\\java\\test\\").resolve(testCase.getClassFilePath)

    // Get Map from jar-names to their corresponding projects, assumes unique jar-names.
    val jarPathToProjectMap = rootProject.getJarToProjMap()
    val jarNameToProjectMap = jarPathToProjectMap.map(tuple => (tuple._1.getFileName.toString, tuple._2))

    // Gets all required .class-files together with their containing .jar-files
    val elements = getDependenciesRecursively(rootProject, subProject, classFile, jdepsPath.toString, 0, Nil)

    // Maps the list of .class-files per .jar to a list of .java-files per Project
    val depsPerJar = elements.groupBy(el => el._2)
    val filesPerJar = for {
      jar <- depsPerJar.keySet
      proj = jarNameToProjectMap(jar)
    } yield {
      (proj, mapClassToJava(depsPerJar(jar), rootProject.getAbsolutePathSrc(), proj))
    }

    // Initialise root project in target directory, if it did not already happen (Wrapper checks for initialised-property)
    copyGradleProjectWrapper(rootProject, repoPath, repo)
    
    // Add project as subproject in target and copy corresponding .java-files for every project
    val listOfClonedAssetInfo = for {
      tuple <- filesPerJar
    } yield {
      copyProject(tuple._1, tuple._2, repo, cloneFromOtherRepos, genSeededRandom)
    }
    val flattedMaps = listOfClonedAssetInfo.flatten
    val clonedAssetMap = flattedMaps.toMap
    assert(flattedMaps.size == clonedAssetMap.size)

    // Identify initialised subprojects that need to be in settings.gradle
    val initProjects = rootProject.listAllProjectsRecursively().filter(proj => proj.pathToGenTargetInRepo.keySet.contains(repo))

    // Add new subprojects from GitProject to TargetRootProject -> needs to happen at every addition of a new feature (at least check for necessity)
    val settingsFile = repoPath.resolve("settings.gradle")
    addSubprojectsToSettings(settingsFile, initProjects)

    (filesPerJar, clonedAssetMap)
  }

  def extractAndImplantTestcase(subProject: Project, testCase: TestCase, insertionPoint: Tuple2[Asset, Int], originalToCloneMap: util.IdentityHashMap[Asset, Asset]): TestcaseOrgan = {
    def optionalToOption[T](input: Optional[T]) : Option[T] = {
      if (input.isPresent) {
        Some(input.get)
      } else {
        None
      }
    }

    // Extract required code from test set and write into extractLocation
    val extractLocation = extractFileContent(subProject, testCase)

    val importFile = extractLocation.resolve("imports.java")
    val testcaseFile = extractLocation.resolve("testcase.java")
    val attributeFile = extractLocation.resolve("attributes.java")
    val constructorFile = extractLocation.resolve("constructor.java")

    val beforeFileOpt = optionalToOption(Files.list(extractLocation).filter(p => p.getFileName.toString.startsWith("before_")).findAny)
    val beforeEachFileOpt = optionalToOption(Files.list(extractLocation).filter(p => p.getFileName.toString.startsWith("beforeEach_")).findAny)
    val beforeAllFileOpt = optionalToOption(Files.list(extractLocation).filter(p => p.getFileName.toString.startsWith("beforeAll_")).findAny)
    val beforeClassFileOpt = optionalToOption(Files.list(extractLocation).filter(p => p.getFileName.toString.startsWith("beforeClass_")).findAny)

    val importContent = Files.readString(importFile)
    val testcaseContent = Files.readString(testcaseFile)

    // Adapt code snippets
    val packageImport = s"import ${testCase.packageName}.*;\n"
    val adjustedImportContent = packageImport + importContent
    val adjustedTestcaseContent = s"try {\n$testcaseContent\n} catch (Exception e) {}\n"

    // Create code asset and to-be-imported feature for testing on the clone
    val feature = Feature(testCase.toString)
    val testcaseBlockAsset = Asset("testcase", BlockType, content = Some(adjustedTestcaseContent.split('\n').toList))

    val insertionPointClone = originalToCloneMap.get(insertionPoint._1)

    // Call VP-Operations on clone to add the testcase and map it to the feature
    AddAsset(testcaseBlockAsset, insertionPointClone, Some(insertionPoint._2), createMetadata = false)
    MapAssetToFeature(testcaseBlockAsset, feature, createMetadata = false)

    val attributes = addNMapAssetFromFile(feature = feature, file = Some(attributeFile), assetName = "attributes", assetType = BlockType, parent = insertionPointClone.parent.get, index = 0) //ideally random, but this needs to be recorded to be replayed 1-to-1 unless this can never make a difference
    val attributesContent = getOptionalAssetContent(attributes)

    val before = addNMapAssetFromFile(feature = feature, file = beforeFileOpt, assetName = "before", assetType = BlockType, parent = insertionPointClone, index = insertionPoint._2)
    val beforeEach = addNMapAssetFromFile(feature = feature, file = beforeEachFileOpt, assetName = "beforeEach", assetType = BlockType, parent = insertionPointClone, index = insertionPoint._2)
    val beforeAll = addNMapAssetFromFile(feature = feature, file = beforeAllFileOpt, assetName = "beforeAll", assetType = BlockType, parent = insertionPointClone, index = insertionPoint._2)
    val beforeClass = addNMapAssetFromFile(feature = feature, file = beforeClassFileOpt, assetName = "beforeClass", assetType = BlockType, parent = insertionPointClone, index = insertionPoint._2)

    val beforeContent = getOptionalAssetContent(before)
    val beforeEachContent = getOptionalAssetContent(beforeEach)
    val beforeAllContent = getOptionalAssetContent(beforeAll)
    val beforeClassContent = getOptionalAssetContent(beforeClass)

    // Insert as last asset, after the befores (independent of whether the function is inserted or just a function call)
    val constructor = addNMapAssetFromFile(feature = feature, file = Some(constructorFile), assetName = "constructor", assetType = BlockType, parent = insertionPointClone, index = insertionPoint._2)
    val constructorContent = getOptionalAssetContent(constructor)

    // find insertion point for import statements
    val fileParent = findFileParentAsset(insertionPoint._1).get

    // assuming first child of fileAsset contains package and import statements -> similar to above -> build asset, add it and map it to feature
    val importParent = originalToCloneMap(fileParent)
    val firstClassAsset = importParent.children.filter(ch => ch.assetType == ClassType).head
    val importPosition = importParent.children.indexOf(firstClassAsset)

    val importAsset = Asset("testcaseimport", BlockType, content = Some(adjustedImportContent.split('\n').toList))
    AddAsset(importAsset, importParent, Some(importPosition), createMetadata = false)
    MapAssetToFeature(importAsset, feature, createMetadata = false)

    // Return the imported code fragments as the testcaseOrgan
    TestcaseOrgan(adjustedImportContent, adjustedTestcaseContent, attributesContent, beforeContent, beforeEachContent, beforeAllContent, beforeClassContent, constructorContent) //, functionsContent)
  }

  def addNMapAssetFromFile(feature: Feature, file: Option[Path], assetName: String, assetType: AssetType, parent: Asset, index: Int): Option[Asset] = {
    if (file.isDefined && Files.exists(file.get)) {
      val code = Files.readString(file.get)
      val asset = Asset(assetName, assetType, content = Some(code.split('\n').toList))
      AddAsset(asset, parent, Some(index), createMetadata = false)
      MapAssetToFeature(asset, feature, createMetadata = false)
      Some(asset)
    } else { None }
  }

  def resetProjectsIfNecessary(rootProject: Project, insertionRepo: Asset, beginInitProjects: Seq[Project]): Unit = {
    val endInitProjects = rootProject.listAllProjectsRecursively.filter(proj => proj.pathToGenTargetInRepo.keySet.contains(insertionRepo))
    val deltaInitProjects = endInitProjects.filter(proj => !beginInitProjects.contains(proj))
    deltaInitProjects.foreach(proj => proj.pathToGenTargetInRepo = proj.pathToGenTargetInRepo.filterNot(elem => elem._1 == insertionRepo))
    println(s"resetProjectsIfNecessary was executed. These projects were reset: ${deltaInitProjects.mkString}")
  }

  def extractTestcase(insertionPoint: Tuple2[Asset, Int], subProject: JavaProject, testCase: TestCase, parentFeature: Feature): Option[AddExternalFeature] = {
    println(String.format("Attempting with: insertionPoint: %s, testCase: %s, from donor: %s", findFileParentAsset(insertionPoint._1), testCase.toString, subProject.name))

    val currentRandomSeed = seededRandom.nextInt()
    val genSeededRandom = new Random(currentRandomSeed)

    // Create working copy of AssetTree
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.registerImmutable(None.getClass)
    val cloneResult = cloner.deepClone(rootAsset)
    val (assetTreeCopy, originalToCloneAssetMap) = (cloneResult.getClone, cloneResult.getMap.asInstanceOf[util.IdentityHashMap[Asset, Asset]])

    // Find and get repository in which testcase is implanted
    val insertionRepo = findRepoOnUpPath(insertionPoint._1).get
    val insertionRepoPath = file.Paths.get(insertionRepo.name)

    // For cleanup: Reset previously not initialised projects, if something goes wrong -> put this in both faulty cases
    // Identify projects already initialised in implantation repository
    val rootProject = subProject.getRootProject
    val beginInitProjects = rootProject.listAllProjectsRecursively().filter(proj => proj.pathToGenTargetInRepo.keySet.contains(insertionRepo))

    // Try to extract and implant the testcase into the copy-"insertionpoint" -> Return the testcase organ, i.e. the elements of the testcase that need to be added
    val tcOrgan = if (guaranteeCallability) {
      try {
        extractAndImplantTestcase(subProject, testCase, insertionPoint, originalToCloneAssetMap)
      } catch {
        case ex: Exception => println(s"Something went wrong during testcase extraction and implantation: ${ex.getMessage}")
          if (loggingFile.isDefined) {
            // Write project, testcase, compiled, gradleOutput
            val testcaseEntry = s"${subProject.name};$testCase;${ex.getMessage};Crashed during extractAndImplantTestcase\n"
            fileWriter.get.append(testcaseEntry)
          }

          resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
          throw TestcaseExtractionException(ex.getMessage)
      }
    } else null

    cleanDirectory(targetPath, false)
    SerialiseAssetTree(assetTreeCopy, targetPath.toString)

    val (externalFileDependencies, clonedAssetMap) = try {
      val (temp1, temp2) = extractOutOfFileDependencies(insertionRepo, insertionRepoPath, subProject, testCase, genSeededRandom)
      (temp1.map(tuple => (tuple._1.asInstanceOf[Project],tuple._2)),temp2)
    } catch {
      case ex: Exception => println(s"Something went wrong during dependency management: ${ex.getMessage}")
        if (loggingFile.isDefined) {
          // Write project, testcase, compiled, gradleOutput
          val testcaseEntry = s"${subProject.name};$testCase;${ex.getMessage};Crashed during extractOutOfFileDependencies\n"
          fileWriter.get.append(testcaseEntry)
        }

        resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
        throw DependencyManagementException(ex.getMessage)
    }

    if (guaranteeCallability) {
      // Add subproject dependency to root build-gradle.
      val closestAncestorBuildFile = findClosestAncestorFile(insertionPoint._1, "build.gradle")
      val projectDependency = ProjectDependency("implementation", subProject.getProjectPathIncludingRoot(), "'")
      try {
        addNewDependency(closestAncestorBuildFile.get, projectDependency)
      } catch {
        case ex: Exception => println(s"Something went wrong during dependency management: ${ex.getMessage}")
          if (loggingFile.isDefined) {
            // Write project, testcase, compiled, gradleOutput
            val testcaseEntry = s"${subProject.name};$testCase;${ex.getMessage};Crashed during addNewDependency\n"
            fileWriter.get.append(testcaseEntry)
          }

          resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
          throw DependencyManagementException(ex.getMessage)
      }
    }

    val compiled = executeGradle(insertionRepoPath.toString, "compileJava")
    println(s"compileJava in extractFeature: $compiled")

    if (loggingFile.isDefined) {
      // Write project, testcase, compiled, gradleOutput
      val testcaseEntry = s"${subProject.name};$testCase;$compiled;No crash\n"
      fileWriter.get.append(testcaseEntry)
    }

    if (compiled) {
      // Convert everything to VP-Operations on original assetTree and return TransplantFeatureOperation with some data such as the targeted Asset, Testcase and perhaps even a list with all operations for unapply?

      // TODOs:
      // change containedPaths to be a dictionary of the adapted path and the corresponding asset
      // Implement FileWalk as Iteration through filesystem myself
      // OwnFilewalker should be recursive and iterate through each layer one-by-one (if no getChildren exists use walk with depth parameter set to 0 or 1)
      // Filewalker gets called with current path (starting with path of VPRootType-asset) and checks for every child, if child exists and if so -> recursive call, otherwise switch to recursiveAssetAdding with current corresponding Asset
      //    Check for current element: is element part of containedPaths.keys
      //      case yes => recursive call for next layer
      //      case no  => get parentasset by looking up asset in containedPaths-dictionary
      //                  call other function which recursively creates and adds Assets starting from this point

      // AssetAdding:
      // Access to current path and to-be-parentAsset
      // Create Asset
      // AddAsset to parentAsset
      // Call function recursively for children with freshly created Asset as new to-be-parentAsset

      val transplantCache = TransplantationCache(testcaseOrgan = tcOrgan, externalFileDependencies = externalFileDependencies, clonedAssetMap = clonedAssetMap)
      Some(AddExternalFeature(insertionPoint = insertionPoint, project = subProject, testCase = testCase, parentFeature = parentFeature, randomSeed = currentRandomSeed, transplantationCache = transplantCache, runImmediately = false))
    }
    else {
      // reset projects that were initialised in this process
      resetProjectsIfNecessary(rootProject, insertionRepo, beginInitProjects)
      None
    }
  }

  override def getPotentialInsertionPoints: List[Asset] = {
    val repoCandidates = rootAsset.children
    val selectedRepo = repoCandidates(Random.nextInt(repoCandidates.length))
    println(s"Attempt to add into repo: ${selectedRepo.name}")

    val methodAssetList = if (filterInsertionPoints.isDefined) {
      // gets concrete insertion point assets for selectedRepo
      val validInsertionPoints = for {
        insertionPoint <- filterInsertionPoints.get
        insPointAsset = getassetbyname(file.Paths.get(selectedRepo.name).resolve(insertionPoint).toString, rootAsset)
        if insPointAsset.isDefined
      } yield {
        insPointAsset.get
      }

      transformASTToList(selectedRepo)
        .filter(asset => asset.assetType == MethodType)
        .filter(method => isTransitiveChildOf(method, validInsertionPoints))
    } else {
      transformASTToList(selectedRepo)
        .filter(asset => asset.assetType == MethodType)
    }
    methodAssetList
  }

  override def selectParentFeature(parentAsset: Asset) : Feature = {
    val anc_fm = getImmediateAncestorFeatureModel(Some(parentAsset))
    anc_fm.get.rootfeature
  }

  override def addFeature(insertionPoint: (Asset, Int), subProject: Project, testCase: TestCase, parentFeature: Feature): Option[ITransplantFeatureViaTest] = {
    try {
      extractTestcase(insertionPoint, subProject.asInstanceOf[JavaProject], testCase, parentFeature)
    } catch {
      case ex: TestcaseExtractionException =>
        None
      case ex: DependencyManagementException =>
        val testClass = testCase.className
        subProject.testcases.filter(tc => tc.className == testClass)
          .foreach(test => blacklist.add((subProject, test)))
        None
    }
  }
}

object AddFeatureGenerator {
  def apply(rootAsset: Asset, rootProjects: Seq[JavaProject], targetPathString: String, unitTestScribePathString: String, initialisationPathString: String, srcmlPathString: String, filterInsertionPoints: Option[List[Path]] = None, cloneFromOtherRepos: Boolean = true, jdepsPathString: String = "C:\\Program Files\\AdoptOpenJDK\\jdk-11.0.8.10-hotspot\\bin", initialiseTestcases: Boolean = true, guaranteeCallability: Boolean = true, loggingFile: Option[File] = None): AddFeatureGenerator = new AddFeatureGenerator(rootAsset, rootProjects, targetPathString, unitTestScribePathString, initialisationPathString, srcmlPathString, filterInsertionPoints, cloneFromOtherRepos, jdepsPathString, initialiseTestcases, guaranteeCallability, loggingFile)
}