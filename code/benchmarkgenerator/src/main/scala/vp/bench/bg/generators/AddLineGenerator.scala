package vp.bench.bg.generators

import java.nio.file
import java.nio.file.{Files, Path}
import java.util
import java.util.Comparator

import com.rits.cloning.Cloner
import vp.bench.bg.Utilities.{cleanDirectory, findFolderParentAsset, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.operations.AddLineToAsset
import se.gu.vp.model.{Asset, AssetType, BlockType}
import se.gu.vp.operations.Utilities.transformASTToList
import se.gu.vp.operations.{ChangeAsset, SerialiseAssetTree}
import vp.bench.bg.generators.fwgens.MutateAssetFWGenerator
import vp.bench.bg.operations.AddLineToAsset

import scala.util.Random

class AddLineGenerator(override val rootAsset: Asset, uselessChangeDiscardProb: Double) extends MutateAssetFWGenerator{

  // identifies empty lines and single line comments
  val lineUsefulnessRegex = "\\s*(//.*)?".r

  override def changeAsset(asset: Asset): Option[ChangeAsset] = {
    if (asset.content == None) {
      None
    } else {
      val content = asset.content.get

      // addLineIndex is the linenumber, where the new line will be, + 1 allows it to be inserted at the end
      val addLineIndex = Random.nextInt(content.length + 1)
      // select new line from any block inside the same folder (package) -> intermediate point between FileType and RepositoryType -> tradeoff between applicability (due to visibility) and amount of possibilities
      val potentialStatements = transformASTToList(findFolderParentAsset(asset).get)
        .filter(elem => elem.assetType == BlockType)
        .flatMap(block => block.content)
        .flatten

      val addLine = potentialStatements(Random.nextInt(potentialStatements.length))
      if (addLine.matches(lineUsefulnessRegex.regex)) {
        if (Random.nextDouble <= uselessChangeDiscardProb) {
          return None
        }
      }

      Some(AddLineToAsset(asset, addLineIndex, addLine, runImmediately = false))
    }
  }
}

object AddLineGenerator {
  def apply(rootAsset:Asset, uselessChangeDiscardProb: Double = 0.0) : AddLineGenerator = new AddLineGenerator(rootAsset, uselessChangeDiscardProb)
}
