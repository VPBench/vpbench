package vp.bench.bg.generators

import vp.bench.bg.Utilities.{findRepoOnUpPath, resolveFeatureFromPath, resolveFeatureModelFromPath}
import vp.bench.bg.operations.CloneFeatureWProjectInitialization
import se.gu.vp.model._
import se.gu.vp.operations.{CloneFeature, Operation}
import se.gu.vp.operations.Utilities._
import vp.bench.bg.generators.fwgens.CloneFeatureFWGenerator
import vp.bench.bg.model.Project
import vp.bench.bg.operations.{CloneFeatureWProjectInitialization, ICloneFeatureWithIntegration}

import scala.util.Random

class CloneFeatureGenerator(override val rootAsset:Asset, val rootProjects: Seq[Project], val targetPathString: String, override val maxMappedAssetCount: Option[Int] = None) extends CloneFeatureFWGenerator {

  def cloneFeature(srcPath: Path, tgtPath: Path) : Option[ICloneFeatureWithIntegration] = {
    Some(CloneFeatureWProjectInitialization(sourceFPath = srcPath, targetFPath = tgtPath, rootProjects = rootProjects, targetPathString = targetPathString, runImmediately = false))
  }
}

object CloneFeatureGenerator{
  def apply(rootAsset: Asset, rootProjects: Seq[Project], targetPathString: String, maxMappedAssetCount: Option[Int] = None) : CloneFeatureGenerator = new CloneFeatureGenerator(rootAsset, rootProjects, targetPathString, maxMappedAssetCount)
}