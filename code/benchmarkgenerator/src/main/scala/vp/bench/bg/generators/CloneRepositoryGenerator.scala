package vp.bench.bg.generators

import se.gu.vp.model.Asset
import se.gu.vp.operations.{CloneRepository, Operation}
import vp.bench.bg.generators.fwgens.CloneVariantFWGenerator
import vp.bench.bg.model.Project
import vp.bench.bg.operations.CloneRepositoryWProjectInitialization

import scala.util.Random

// Ideally, rootProjects should not be part of other generators apart from AddFeatureGenerator, but synchronization of project information is required here (could perhaps be solved better using Event and listener system?) -> too much effort for now
class CloneRepositoryGenerator (override val rootAsset: Asset, val rootProjects: Seq[Project]) extends CloneVariantFWGenerator {

  override def cloneRepository(selectedRepo: Asset, cloneName: String): Option[CloneRepository] = {
    Some(CloneRepositoryWProjectInitialization(selectedRepo, cloneName, rootProjects, runImmediately = false))
  }
}

object CloneRepositoryGenerator {
  def apply(rootAsset: Asset, rootProjects: Seq[Project]) : CloneRepositoryGenerator = new CloneRepositoryGenerator(rootAsset, rootProjects)
}
