package vp.bench.bg.generators

import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util
import java.util.Comparator

import com.rits.cloning.Cloner
import vp.bench.bg.Utilities.{cleanDirectory, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.operations.DeleteLineFromAsset
import se.gu.vp.model.{Asset, AssetType}
import se.gu.vp.operations.{ChangeAsset, RemoveFeature, SerialiseAssetTree}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformASTToList}
import vp.bench.bg.generators.fwgens.MutateAssetFWGenerator
import vp.bench.bg.operations.DeleteLineFromAsset

import scala.util.Random
import scala.collection.JavaConversions

class DeleteLineGenerator(override val rootAsset: Asset, uselessChangeDiscardProb: Double) extends MutateAssetFWGenerator{

  // identifies empty lines and single line comments
  val lineUsefulnessRegex = "\\s*(//.*)?".r

  override def changeAsset(asset: Asset): Option[ChangeAsset] = {
    if (asset.content == None || asset.content.get.length == 0) {
      None
    } else {
      val content = asset.content.get

      val deleteLineIndex = Random.nextInt(content.length)
      val deleteLine = content(deleteLineIndex)
      if (deleteLine.matches(lineUsefulnessRegex.regex)) {
        if (Random.nextDouble <= uselessChangeDiscardProb) {
          return None
        }
      }
      Some(DeleteLineFromAsset(asset, deleteLineIndex, runImmediately = false))
    }
  }
}

object DeleteLineGenerator {
  def apply(rootAsset:Asset, uselessChangeDiscardProb: Double = 0.0) : DeleteLineGenerator = new DeleteLineGenerator(rootAsset, uselessChangeDiscardProb)
}