package vp.bench.bg.generators

import se.gu.vp.model.Asset
import se.gu.vp.operations.Operation

trait Generator {
  val rootAsset: Asset

  def generate: Option[Operation]
}