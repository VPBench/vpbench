package vp.bench.bg.generators

import com.rits.cloning.Cloner
import se.gu.vp.model.{Asset, AssetType, Feature, FeatureModel, Path, RootFeature}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformASTToList, transformFMToList}
import se.gu.vp.operations.{Operation, RemoveFeature, SerialiseAssetTree}
import java.nio.file
import java.nio.file.{Files, Path, StandardOpenOption}
import java.util
import java.util.Comparator

import vp.bench.bg.Utilities.{cleanDirectory, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.generators.fwgens.RemoveFeatureFWGenerator

import scala.util.Random

class RemoveFeatureGenerator(override val rootAsset: Asset) extends RemoveFeatureFWGenerator {

  override def removeFeature(featurePath: se.gu.vp.model.Path): Option[RemoveFeature] = {
    Some(RemoveFeature(featurePath, runImmediately = false))
  }
}

object RemoveFeatureGenerator{
  def apply(rootAsset: Asset):RemoveFeatureGenerator = new RemoveFeatureGenerator(rootAsset)
}