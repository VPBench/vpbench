package vp.bench.bg.generators

import java.nio.file
import java.nio.file.{Files, Path}
import java.util
import java.util.Comparator

import com.rits.cloning.Cloner
import vp.bench.bg.Utilities.{cleanDirectory, findFolderParentAsset, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.operations.ReplaceLineInAsset
import se.gu.vp.model.{Asset, AssetType, BlockType}
import se.gu.vp.operations.Utilities.transformASTToList
import se.gu.vp.operations.{ChangeAsset, SerialiseAssetTree}
import vp.bench.bg.generators.fwgens.MutateAssetFWGenerator
import vp.bench.bg.operations.ReplaceLineInAsset

import scala.util.Random

class ReplaceLineGenerator(override val rootAsset: Asset, uselessChangeDiscardProb: Double) extends MutateAssetFWGenerator {

  // identifies empty lines and single line comments
  val lineUsefulnessRegex = "\\s*(//.*)?".r

  override def changeAsset(asset: Asset): Option[ChangeAsset] = {
    if (asset.content == None || asset.content.get.length == 0) {
      None
    } else {
      val content = asset.content.get

      // repLineIndex is the linenumber of the old line, replaced with the new one
      val repLineIndex = Random.nextInt(content.length)
      // select new line from any block inside the same folder (package) -> intermediate point between FileType and RepositoryType -> tradeoff between applicability (due to visibility) and amount of possibilities
      val potentialStatements = transformASTToList(findFolderParentAsset(asset).get)
        .filter(elem => elem.assetType == BlockType)
        .flatMap(block => block.content)
        .flatten

      val repLine = potentialStatements(Random.nextInt(potentialStatements.length))
      val curLine = content(repLineIndex)
      if (repLine.matches(lineUsefulnessRegex.regex) || repLine.trim == curLine.trim) {
        if (Random.nextDouble <= uselessChangeDiscardProb) {
          return None
        }
      }

      Some(ReplaceLineInAsset(asset, repLineIndex, repLine, runImmediately = false))
    }
  }
}

object ReplaceLineGenerator {
  def apply(rootAsset:Asset, uselessChangeDiscardProb: Double = 0.0) : ReplaceLineGenerator = new ReplaceLineGenerator(rootAsset, uselessChangeDiscardProb)
}