package vp.bench.bg.generators.fwgens

import vp.bench.bg.Utilities.findRepoOnUpPath
import se.gu.vp.model._
import se.gu.vp.operations.Operation
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformFMToList}
import vp.bench.bg.generators.Generator
import vp.bench.bg.operations.ICloneFeatureWithIntegration

import scala.util.Random

abstract class CloneFeatureFWGenerator extends Generator {
  //override val rootAsset : Asset
  val maxMappedAssetCount: Option[Int]

  def cloneFeature(srcPath: Path, tgtPath: Path) : Option[ICloneFeatureWithIntegration]

  def findCloneableFeatures : List[Tuple3[Asset, Asset, Feature]] = {
    val repoCloneTraces = TraceDatabase.traces.filter(tr => tr.source.assetType == RepositoryType && tr.target.assetType == RepositoryType)
    val clonableFeaturePerRepoClone = repoCloneTraces.map(trace => (trace.source, trace.target))
      .map(tuple =>
        (tuple._1, tuple._2,
          transformFMToList(tuple._1.featureModel.get)
            // filter out rootFeatures and UnAssigned features
            .filterNot(feat => feat.isInstanceOf[RootFeature] || (feat.name == "UnAssigned" && feat.parent.get.isInstanceOf[RootFeature]))
            // only take the features, that were not cloned before between source and target
            // -> only take feature f, when no feature trace between source and target cloned f
            .filterNot(feat =>
              // try/catch is required in case a trace is tested, that references a feature, which has been removed -> causing an error during get-calls
              // it returns false, since all traces are tested for all feats inside the feature model
              // a trace should only have an effect, if it describes the particular feature being cloned between the two repositories
              // all features that are checked currently exist in the source repos fm -> it is only filtered down from currently available features
              FeatureTraceDatabase.traces.exists(ft => try {
                findRepoOnUpPath(ft.source.getFeatureModelRoot.get.featureModel.get.containingAsset.get).get == tuple._1 &&
                  findRepoOnUpPath(ft.target.getFeatureModelRoot.get.featureModel.get.containingAsset.get).get == tuple._2 &&
                  ft.source == feat
              } catch {
                case ex: Exception => false
              }))))

    clonableFeaturePerRepoClone.filterNot(triple => triple._3.isEmpty)
      .flatMap(triple => triple._3.map(feat => (triple._1, triple._2, feat)))
      .filter(triple => maxMappedAssetCount.isEmpty || triple._3.mappedAssets(triple._1).length <= maxMappedAssetCount.get)
  }

  def selectSourceFeature(possibleCandidates : List[Tuple3[Asset, Asset, Feature]]) : Tuple3[Asset, Asset, Feature] = {
    possibleCandidates(Random.nextInt(possibleCandidates.length))
  }

  def selectTargetFeature(targetAsset: Asset) = {
    List(targetAsset.featureModel.get.rootfeature, targetAsset.featureModel.get.rootfeature.UNASSIGNED)(Random.nextInt(2))
  }

  def selectFeatures(possibleCandidates : List[Tuple3[Asset, Asset, Feature]]) : Tuple4[Asset, Asset, Feature, Feature] = {
    val (sourceAsset, targetAsset, featureToClone) = selectSourceFeature(possibleCandidates)
    val targetParentFeature = selectTargetFeature(targetAsset)
    (sourceAsset, targetAsset, featureToClone, targetParentFeature)
  }

  override def generate: Option[ICloneFeatureWithIntegration] = {
    println("Attempt to clone feature")
    val possibleCandidates = findCloneableFeatures
    if(possibleCandidates.length == 0) {
      return None
    }

    val (sourceAsset, targetAsset, featureToClone, targetParentFeature) = selectFeatures(possibleCandidates)

    val srcAsPath = computeAssetPath(sourceAsset)
    val srcFePath = computeFeaturePath(featureToClone)
    val srcPath = Path(rootAsset, srcAsPath ++ srcFePath)

    val tgtAsPath = computeAssetPath(targetAsset)
    val tgtFePath = computeFeaturePath(targetParentFeature)
    val tgtPath = Path(rootAsset, tgtAsPath ++ tgtFePath)

    // for debugging
    println(s"Attempting to clone $srcPath into $tgtPath")

    cloneFeature(srcPath, tgtPath)
  }
}
