package vp.bench.bg.generators.fwgens

import se.gu.vp.model.Asset
import se.gu.vp.operations.{CloneRepository, Operation}
import vp.bench.bg.generators.Generator

import scala.util.Random

abstract class CloneVariantFWGenerator extends Generator {
  //override val rootAsset : Asset
  var uniqueNameCounter = 0

  def cloneRepository(selectedRepo: Asset, cloneName: String) : Option[CloneRepository]

  def setNewName(selectedRepo: Asset) : String = {
    val cloneName = selectedRepo.name.split('\\').last + s"_$uniqueNameCounter"
    uniqueNameCounter = uniqueNameCounter + 1
    cloneName
  }

  def selectRepository(clonableRepos: List[Asset]) : Asset = {
    val selectedRepo = clonableRepos(Random.nextInt(clonableRepos.length))
    selectedRepo
  }

  def getClonableRepositories : List[Asset] = {
    rootAsset.children
  }

  override def generate: Option[CloneRepository] = {
    val clonableRepos = getClonableRepositories
    val selectedRepo = selectRepository(clonableRepos)
    val cloneName = setNewName(selectedRepo)

    cloneRepository(selectedRepo, cloneName)
  }
}
