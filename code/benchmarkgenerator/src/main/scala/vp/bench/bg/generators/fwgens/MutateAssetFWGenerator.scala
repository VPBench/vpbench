package vp.bench.bg.generators.fwgens

import se.gu.vp.model._
import se.gu.vp.operations.ChangeAsset
import se.gu.vp.operations.Utilities._
import vp.bench.bg.generators.Generator

import scala.util.Random

abstract class MutateAssetFWGenerator extends Generator {
  //override val rootAsset : Asset

  def getChangeableAssets : List[Asset] = {
    val assets = transformASTToList(rootAsset)
    assets.filter(a => a.assetType == BlockType)
  }

  def selectAsset(changeableAssets : List[Asset]) : Asset = {
    changeableAssets(Random.nextInt(changeableAssets.length))
  }

  override def generate: Option[ChangeAsset] = {
    val changeableAssets = getChangeableAssets
    val selectedAsset = selectAsset(changeableAssets)

    // for debugging
    println(s"ChangeAsset $selectedAsset")

    changeAsset(selectedAsset)
  }

  def changeAsset(asset: Asset) : Option[ChangeAsset]
}