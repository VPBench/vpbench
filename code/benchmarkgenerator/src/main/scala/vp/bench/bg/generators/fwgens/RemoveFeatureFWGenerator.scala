package vp.bench.bg.generators.fwgens

import se.gu.vp.model.{Asset, Feature, RootFeature}
import se.gu.vp.operations.{Operation, RemoveFeature}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, transformASTToList, transformFMToList}
import vp.bench.bg.generators.Generator

import scala.util.Random

abstract class RemoveFeatureFWGenerator extends Generator {
  //override val rootAsset : Asset

  def removeFeature(featurePath: se.gu.vp.model.Path) : Option[RemoveFeature]

  def selectFeature(removableFeatures: List[Tuple2[Feature,Asset]]) : Tuple2[Feature,Asset] = {
    removableFeatures(Random.nextInt(removableFeatures.length))
  }

  def getAllFeaturesInAsset(asset: Asset) : List[Tuple2[Feature, Asset]] = {
    val featureModels = transformASTToList(asset).map(elem => (elem.featureModel,elem))
    val filteredFeatureModels = featureModels
      .filter(elem => elem._1.isDefined)
      .map(tuple => (tuple._1.get,tuple._2))
    val featuresWithAsset = filteredFeatureModels.flatMap(tuple => transformFMToList(tuple._1).map(feat => (feat, tuple._2)))
    featuresWithAsset
    //featuresWithAsset.map(elem => elem._1.map(feature => (feature,elem._2))).flatten
  }

  def getRemovableFeatures : List[Tuple2[Feature, Asset]] = {
    val features = getAllFeaturesInAsset(rootAsset)
    features
      .filter(feat => feat._1.getClass != classOf[RootFeature])
      .filter(feat => feat._1.name != "UnAssigned")
  }

  //  def getFeatureModelsInAsset(asset: Asset) : List[Tuple2[Option[FeatureModel], Asset]] = {
  //    val featureModel = asset.featureModel
  //    val childrenFMs = asset.children
  //      .flatMap(child => getFeatureModelsInAsset(child))
  //    (featureModel, asset) :: childrenFMs
  //  }

  override def generate: Option[RemoveFeature] = {
    val removableFeatures = getRemovableFeatures
    if (removableFeatures.length == 0) {
      None
    } else {
      val (featureToRemove, containingAsset) = selectFeature(removableFeatures)

      val originalAssetPath = computeAssetPath(containingAsset)
      val originalFeaturePath = computeFeaturePath(featureToRemove)
      val featurePath = se.gu.vp.model.Path(rootAsset, originalAssetPath ++ originalFeaturePath)
      removeFeature(featurePath)
    }
  }
}
