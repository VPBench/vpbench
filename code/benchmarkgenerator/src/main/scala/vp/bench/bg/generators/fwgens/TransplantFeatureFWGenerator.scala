package vp.bench.bg.generators.fwgens

import java.nio.file
import java.nio.file.Path

import vp.bench.bg.Utilities.isTransitiveChildOf
import vp.bench.bg.model.Project
import se.gu.vp.model.{Asset, Feature, MethodType}
import se.gu.vp.operations.Operation
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import vp.bench.bg.generators.Generator
import vp.bench.bg.model.{Project, TestCase}
import vp.bench.bg.operations.ITransplantFeatureViaTest

import scala.collection.mutable
import scala.util.Random

abstract class TransplantFeatureFWGenerator extends Generator {
  //override val rootAsset : Asset
  val rootProjects : Seq[Project]
  val filterInsertionPoints : Option[List[Path]]
  val blacklist: mutable.HashSet[Tuple2[Project, TestCase]] = mutable.HashSet[Tuple2[Project, TestCase]]()

  def addFeature(insertionPoint: Tuple2[Asset, Int], subProject: Project, testCase: TestCase, parentFeature: Feature) : Option[ITransplantFeatureViaTest]

  def selectParentFeature(parentAsset: Asset) : Feature

  def selectInsertionPoint(potentialInsertionPoints: List[Asset]): (Asset, Int) = {
    val insertionParent = potentialInsertionPoints(Random.nextInt(potentialInsertionPoints.length))
    val insertionNumber = if (insertionParent.children.length == 0) {
      0
    } else {
      Random.nextInt(insertionParent.children.length)
    }
    // insertionNumber is not yet used
    val insertionPoint = (insertionParent, insertionNumber)
    insertionPoint
  }

  def getPotentialInsertionPoints : List[Asset] = {
    val repositories = rootAsset.children

    val methodAssetList = if (filterInsertionPoints.isDefined) {
      // gets concrete insertion point assets for all repos
      val validInsertionPoints = for {
        repo <- repositories
        insertionPoint <- filterInsertionPoints.get
        insPointAsset = getassetbyname(file.Paths.get(repo.name).resolve(insertionPoint).toString, rootAsset)
        if insPointAsset.isDefined
      } yield {
        insPointAsset.get
      }

      transformASTToList(rootAsset)
        .filter(asset => asset.assetType == MethodType)
        .filter(method => isTransitiveChildOf(method, validInsertionPoints))
    } else {
      transformASTToList(rootAsset)
        .filter(asset => asset.assetType == MethodType)
    }
    methodAssetList
  }

  def selectTestCase(projTestTuples : Seq[Tuple2[Project, TestCase]]) : Tuple2[Project, TestCase] = {
    val (subProject, testCase) = projTestTuples(Random.nextInt(projTestTuples.length))

    // could in theory still work somewhere else, if it doesnt work
    // if it does, add it somewhere else, too? -> if same feature is added to some repo, is that still AddFeature? Or rather AddAsset, CloneFeature or CloneAsset?
    blacklist.add((subProject, testCase))

    (subProject, testCase)
  }

  def getPotentialTestCases : Seq[Tuple2[Project, TestCase]] = {
    rootProjects.map(proj => proj.listAllProjectsRecursively).flatten
      .map(proj => proj.testcases.map(test => (proj, test))).flatten
      .filter(elem => !blacklist.contains(elem))
  }

  def generate: Option[ITransplantFeatureViaTest] = {
    val potentialInsertionPoints = getPotentialInsertionPoints
    val insertionPoint = selectInsertionPoint(potentialInsertionPoints)
    val potentialTestCases = getPotentialTestCases
    if (potentialTestCases.length == 0) {
      None
    } else {
      val (project, testCase) = selectTestCase(potentialTestCases)
      val parentFeature = selectParentFeature(insertionPoint._1)
      addFeature(insertionPoint, project, testCase, parentFeature)
    }
  }
}
