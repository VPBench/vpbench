package vp.bench.bg.gradlehandler

import java.io.File

import org.gradle.tooling.GradleConnector

object GradleExecutor {
  def executeGradle(projectPath: String, tasks: String*) : Boolean = {
    val connection = GradleConnector
      .newConnector
      .useGradleVersion("6.8.2") // TODO: TEST!
      .forProjectDirectory(new File(projectPath))
      .connect

    // when build fails -> exception is thrown -> Listener should not be necessary, just try catch and if caught -> compilation failed
    try {
      connection
        .newBuild()
        .forTasks(tasks : _*)
        //.setStandardOutput(outputStream)
        .run()
      connection.close
      true
    } catch {
      case ex : Exception =>
        connection.close
        println(s"Execution failed with Exception: ${ex.getMessage}")
        false
      case _ =>
        connection.close
        println("Execution failed.")
        false
    }
  }
}
