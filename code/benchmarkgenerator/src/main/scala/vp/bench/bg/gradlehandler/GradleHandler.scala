package vp.bench.bg.gradlehandler

import vp.bench.bg.Utilities.{getAllIndicesOf, replaceStringSlice}
import vp.bench.bg.model.Project
import GradleParsingUtilities.{getClosure, getClosureIndices, identifyDependencies}
import vp.bench.bg.model.{Dependency, ExternalDependency, Project, ProjectDependency}

object GradleHandler {

  // configurations on java-library testCompileClasspath -> all configurations that need to be available for using testcases
  val javaLibraryPluginTestCompileClasspathConfigs = Set("testCompileOnly","compileOnlyApi","testImplementation","implementation","api","compile","testCompile")
  // configurations on java-library compileClasspath
  val javaLibraryPluginCompileClasspathConfigs = Set("compileOnly","compileOnlyApi","api","implementation","compile")
  // disjunction of compileClasspath and testCompileClasspath of java-library plugin -> all relevant configurations
  val javaLibraryPluginRelevantConfigs = javaLibraryPluginTestCompileClasspathConfigs ++ javaLibraryPluginCompileClasspathConfigs

  // map configurations from testCompileClasspath onto configurations in apiElements (this is a specialisation in most cases)
  // see graphs https://docs.gradle.org/6.8.2/userguide/java_library_plugin.html#sec:java_library_configurations_graph
  val transformConfigMap = Map(
    "testCompileOnly" -> "compileOnlyApi",
    "compileOnlyApi" -> "compileOnlyApi",
    "testImplementation" -> "api",
    "implementation" -> "api",
    "api" -> "api",
    // compile is mapped to api as implementation extends compile and implementation is mapped to api (see: https://docs.gradle.org/6.8.2/userguide/java_plugin.html#sec:java_plugin_and_dependency_management)
    "compile" -> "api",
    // testCompile is mapped to api as testImplementation extends testCompile and testImplementation is mapped to api (see: https://docs.gradle.org/6.8.2/userguide/java_plugin.html#sec:java_plugin_and_dependency_management)
    "testCompile" -> "api")


  def mapTestCompileToApiElements(dependency: Dependency) : Dependency = {
    if (javaLibraryPluginTestCompileClasspathConfigs.contains(dependency.configuration)) {
      dependency.mapToNewConfiguration(transformConfigMap(dependency.configuration))
    }
    else {
      dependency
    }
  }

  // Takes any dependency and matches project dependencies to target structure (prepend path from target root to source root project)
  def mapProjectDepsToNewStructure(dependency : Dependency, project: Project) : Dependency = {
    dependency match {
      case dep : ProjectDependency => dep.mapToNewStructure(project)
      case _ => dependency
    }
  }

  def getDependenciesAndClosureBorders(content: String, depBegin: Int) : Tuple3[Seq[Dependency], Int, Int] = {
    val dependencyClosureBegin = content.substring(depBegin)
    // Get indices of closure brackets to extract dependencies and to be able to replace entire block by uniquely identifying it with indices
    // Indices are given relative to substring
    val (depClosureBeginInd, depClosureEndInd) = getClosureIndices(dependencyClosureBegin)
    // Get the actual closure (without identifier)
    val depClosure = dependencyClosureBegin.substring(depClosureBeginInd,depClosureEndInd)
    // Identify all dependencies in the closure
    (identifyDependencies(depClosure), depClosureBeginInd, depClosureEndInd)
  }

  def adaptBuildGradle(content: String, project: Project) : String = {
    // use var instead of val for easier editing
    var editableContent = content

    // Get all dependencies-blocks. While we currently assume only one dependencies-block on the same hierarchical level, there may be blocks in subprojects{} or allprojects{}
    // As all dependencies-blocks are replaced 1-to-1, the hierarchical structure of simplifyBuildGradle is not required here.
    val depRegEx = raw"dependencies\s*\Q{\E".r
    val depMatches = depRegEx.findAllMatchIn(editableContent).toList

    for {
      // Reverse the dependencies to not mess up the indexing of dependency-blocks with a higher identified starting index
      depMatch <- depMatches.reverse
      depBegin = depMatch.start
      (dependencies,_,depClosureEndInd) = getDependenciesAndClosureBorders(editableContent,depBegin)
      adaptedDependencies = dependencies
        // filter dependencies to only contain relevant dependencies, i.e. dependencies on java-libraries compileClasspath or testCompileClasspath
        .filter(dep => javaLibraryPluginRelevantConfigs.contains(dep.configuration))
        // filters down to project- and external dependencies, i.e. what is supported at the moment
        // TODO: could try to copy fileDependencies where filtered
        .filter(dep => dep.isInstanceOf[ProjectDependency] || dep.isInstanceOf[ExternalDependency])
        .map(dep => mapTestCompileToApiElements(dep))
        // map projectDependencies to new structure, e.g. project(':jadx-core') to project(':jadx:jadx-core')
        .map(dep => mapProjectDepsToNewStructure(dep, project))
        // create dependenciesString
        .map(dep => dep.toString)
        .mkString("\n\t")
      // put dependencies into identified block
      dependenciesString = s"dependencies {\n\t$adaptedDependencies\n}"
    } yield {
      // replace current dependencies block with created one
      editableContent = replaceStringSlice(editableContent, dependenciesString, depBegin, depBegin + depClosureEndInd)
    }
    editableContent
  }

  def simplifyBuildGradle(content: String) : String = {
    // use var instead of var for easier editing
    var editableContent = content

    // Removes all buildscript blocks on all control structure-levels in the entire buildscript
    val buildScriptRegEx = raw"buildscript\s*\Q{\E".r
    val buildScriptMatches = buildScriptRegEx.findAllMatchIn(editableContent).toList
    for {
      buildScriptMatch <- buildScriptMatches.reverse
      buildScriptBegin = buildScriptMatch.start
      buildScriptClosureBegin = editableContent.substring(buildScriptBegin)
      (bldScrClosureBegInd,bldScrClosureEndInd) = getClosureIndices(buildScriptClosureBegin)
    } yield {
      editableContent = replaceStringSlice(editableContent,"", buildScriptBegin, buildScriptBegin + bldScrClosureEndInd)
    }

    // Only works on a single allprojects block per build.gradle
    val allProjectsRegEx = raw"allprojects\s*\Q{\E".r
    val allProjectsMatches = allProjectsRegEx.findAllMatchIn(editableContent).toList
    val allProjectsMatch = allProjectsMatches.length > 0 match {
      case true => Some(allProjectsMatches(0))
      case _ => None
    }
    val allProjectsContent = allProjectsMatch match {
      case None => ""
      case Some(allProjMatch) =>
        // Extract allprojects block
        val allProjectsClosureBegin = editableContent.substring(allProjMatch.start)
        val (allProjClosureBeginInd,allProjClosureEndInd) = getClosureIndices(allProjectsClosureBegin)
        val allProjectsIdentifiedClosure = allProjectsClosureBegin.substring(0,allProjClosureEndInd)
        val allProjectsClosure = allProjectsClosureBegin.substring(allProjClosureBeginInd,allProjClosureEndInd)
        // Simplify allprojects block
        val simplifiedAllProjectsClosure = simplifyBuildGradle(allProjectsClosure)
        // Remove block from content to not infer with extracting other blocks afterwards
        editableContent = editableContent.replace(allProjectsIdentifiedClosure,"")
        s"allprojects {\n$simplifiedAllProjectsClosure}\n"
    }

    // Only works on a single subprojects block per build.gradle
    // Potentially problematic: parsing name into block? before -> e.g. see Structurizr main build.gradle (look up nomenclature for this problem)
    // Improved, but still not perfect, as e.g. a subprojects {} block in quotes or in a string would fool the parser
    val subProjectsRegEx = raw"subprojects\s*\Q{\E".r
    val subProjectsMatches = subProjectsRegEx.findAllMatchIn(editableContent).toList
    val subProjectsMatch = subProjectsMatches.length > 0 match {
      case true => Some(subProjectsMatches(0))
      case _ => None
    }

    val subProjectsContent = subProjectsMatch match {
      case None => ""
      case Some(subProjMatch) =>
        // Extract subprojects block
        val subProjectsClosureBegin = editableContent.substring(subProjMatch.start)
        val (subProjClosureBeginInd,subProjClosureEndInd) = getClosureIndices(subProjectsClosureBegin)
        val subProjectsIdentifiedClosure = subProjectsClosureBegin.substring(0,subProjClosureEndInd)
        val subProjectsClosure = subProjectsClosureBegin.substring(subProjClosureBeginInd,subProjClosureEndInd)
        // Simplify subprojects block
        val simplifiedSubprojectsClosure = simplifyBuildGradle(subProjectsClosure)
        // Remove block from content to not infer with extracting other blocks afterwards
        editableContent = editableContent.replace(subProjectsIdentifiedClosure,"")
        s"subprojects {\n$simplifiedSubprojectsClosure}\n"
    }

    val nonEditableContent = editableContent

    val javaLibPluginApply = "apply plugin: 'java-library'\n"

    // Only works on a single dependencies block per build.gradle
    // Extracts dependencies as is
    val depRegEx = raw"dependencies\s*\Q{\E".r
    val depMatches = depRegEx.findAllMatchIn(nonEditableContent).toList
    val depMatch = depMatches.length > 0 match {
      case true => Some(depMatches(0))
      case _ => None
    }
    val dependencies = depMatch match {
      case None => ""
      case Some(dep) =>
        val dependencyClosureBegin = nonEditableContent.substring(dep.start)
        val depClosure = getClosure(dependencyClosureBegin)
        s"dependencies $depClosure\n"
    }

    // Only works on a single repositories block per build.gradle
    // Extracts repositories as is
    val repRegEx = raw"repositories\s*\Q{\E".r
    val repMatches = repRegEx.findAllMatchIn(nonEditableContent).toList
    val repMatch = repMatches.length > 0 match {
      case true => Some(repMatches(0))
      case _ => None
    }
    val repositories = repMatch match {
      case None => ""
      case Some(rep) =>
        val repositoryClosureBegin = nonEditableContent.substring(rep.start)
        val repClosure = getClosure(repositoryClosureBegin)
        s"repositories $repClosure\n"
    }

    // Only works on a single sourceSets block per build.gradle
    // Extracts sourceSets as is
    val srcSetRegEx = raw"sourceSets\s*\Q{\E".r
    val srcSetMatches = srcSetRegEx.findAllMatchIn(nonEditableContent).toList
    val srcSetMatch = srcSetMatches.length > 0 match {
      case true => Some(srcSetMatches(0))
      case _ => None
    }
    val srcSetClosure = srcSetMatch match {
      case None => ""
      case Some(srcSet) =>
        val sourceSetClosureBegin = nonEditableContent.substring(srcSet.start)
        val srcSetClosure = getClosure(sourceSetClosureBegin)
        s"sourceSets $srcSetClosure\n"
    }

    // Concat all relevant blocks (can be empty strings) and return
    javaLibPluginApply ++ subProjectsContent ++ allProjectsContent ++ dependencies ++ repositories ++ srcSetClosure
  }
}