package vp.bench.bg.gradlehandler

import vp.bench.bg.Utilities.findEarlierOccurence
import vp.bench.bg.model.UnsupportedDependency
import vp.bench.bg.model.{Dependency, ExternalDependency, FileDependency, ProjectDependency, UnsupportedDependency}

object GradleParsingUtilities {
  def getClosureIndices(content: String): Tuple2[Int,Int] = {
    def bracketTypeToInt(bracket: Char): Int = {
      bracket match {
        case '{' => 1
        case '}' => -1
        case _ => 0
      }
    }

    // fold iterates over all chars in content and stores only the brackets with their corresponding index in content
    val (_, brackets) = content.foldLeft((0, List[Tuple2[Char, Int]]()))((iterator, elem) => elem match {
      case '{' => (iterator._1 + 1, iterator._2 ++ List((elem, iterator._1)))
      case '}' => (iterator._1 + 1, iterator._2 ++ List((elem, iterator._1)))
      case _ => (iterator._1 + 1, iterator._2)
    })
    val closureBeginIndex = brackets(0)._2
    val bracketTypes = brackets.map(bracketTuple => bracketTypeToInt(bracketTuple._1))
    val depthScan = bracketTypes.scan(0)(_ + _).tail
    val closureEndBracketIndex = depthScan.indexOf(0)
    val closureEndIndex = brackets(closureEndBracketIndex)._2

    (closureBeginIndex, closureEndIndex + 1)
  }

  def getClosure(content: String, includeIdentifier: Boolean = false): String = {
    val (beginIndex,endIndex) = getClosureIndices(content)
    val substringBegin = if (includeIdentifier)
    { 0 }
    else
    { beginIndex}
    content.substring(substringBegin,endIndex)
  }

  // https://docs.gradle.org/6.8.2/dsl/org.gradle.api.artifacts.dsl.DependencyHandler.html#N1740B
  // Shows example of configurationName(dependencyNotation) in combination with {} -> jadx uses it without attached closure
  def stringToDependency(line: String): Dependency = {
    val splitSymbol = findEarlierOccurence(line,' ','(')
    val suffixAddOn = if (splitSymbol == '(') { ")" } else { "" }

    val (configurationName: String, toBeResolved: String) = line.splitAt(line.indexOf(splitSymbol))
    val toBeResolvedPrepared = toBeResolved.substring(1).stripLeading()
    toBeResolvedPrepared match {
      case dep if dep.startsWith("project") =>
        val dependentOnElement = dep.stripPrefix("project('").stripSuffix("')" + suffixAddOn)
        ProjectDependency(configurationName, dependentOnElement, "'")
      case dep if dep.startsWith("files") =>
        val dependentOnElement = dep.stripPrefix("files('").stripSuffix("')" + suffixAddOn)
        FileDependency(configurationName, dependentOnElement, "'")
      case dep if dep.startsWith("'") =>
        val dependentOnElement = dep.drop(1).dropRight(1 + suffixAddOn.length)
        ExternalDependency(configurationName, dependentOnElement, "'")
      case dep if dep.startsWith("\"") =>
        val dependentOnElement = dep.drop(1).dropRight(1 + suffixAddOn.length)
        ExternalDependency(configurationName, dependentOnElement, "\"")
      case dep if dep.matches("group[\\s]*:[\\s]*('[^']+')[\\s]*,[\\s]*name[\\s]*:[\\s]*('[^']+')[\\s]*,[\\s]*version[\\s]*:[\\s]*('[^']+')") =>
        val dependentOnElement = dep.split(',').map(el => el.split(':')(1).stripLeading.drop(1).stripTrailing.dropRight(1)).mkString(":")
        ExternalDependency(configurationName, dependentOnElement, "'")
      case dep if dep.matches("group[\\s]*:[\\s]*(\\\"[^\\\"]+\\\")[\\s]*,[\\s]*name[\\s]*:[\\s]*(\\\"[^\\\"]+\\\")[\\s]*,[\\s]*version[\\s]*:[\\s]*(\\\"[^\\\"]+\\\")") =>
        val dependentOnElement = dep.split(',').map(el => el.split(':')(1).stripLeading.drop(1).stripTrailing.dropRight(1)).mkString(":")
        ExternalDependency(configurationName, dependentOnElement, "\"")
      case dep => // default used quotation sign
        UnsupportedDependency(configurationName, dep, "'")
    }
  }

  def identifyDependencies(closure: String) = {
    // Takes in a closure with brackets on -> remove brackets and leading and trailing whitespace characters
    val dependencyClosureContent = closure.stripPrefix("{").stripSuffix("}").stripLeading().stripTrailing()
    // Split into non-empty lines
    val dependencyLines = dependencyClosureContent.split('\n')
      .map(line => line.stripLeading().stripTrailing())
      .filterNot(line => line.isEmpty)
    // Map each non-empty line to a dependency and return
    val dependencies = dependencyLines.map(line => stringToDependency(line))
    dependencies
  }
}
