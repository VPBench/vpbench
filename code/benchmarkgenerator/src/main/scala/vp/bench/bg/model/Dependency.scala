package vp.bench.bg.model

// Missing: Configuration Dependencies, Gradle Distribution Specific Dependencies, Client Module Dependencies, Platform, ...
// https://docs.gradle.org/6.8.2/dsl/org.gradle.api.artifacts.dsl.DependencyHandler.html
// Check methods in link above to see missing dependency types.
// TODO: How to add dependencies to other configurations? Which method is responsible for that?
// TODO: What function is reponsible for adding external dependencies? add?
abstract class Dependency {
  val configuration : String
  val dependency : String
  val quotationSign : String

  def mapToNewConfiguration(newConfig: String) : Dependency
}
case class ExternalDependency(configuration : String, dependency : String, quotationSign : String) extends Dependency {
  override def toString: String = s"$configuration $quotationSign$dependency$quotationSign"

  override def mapToNewConfiguration(newConfig: String): Dependency = {
    copy(newConfig, dependency)
  }
}
case class ProjectDependency(configuration : String, dependency : String, quotationSign : String) extends Dependency {
  override def toString: String = s"$configuration project($quotationSign$dependency$quotationSign)"
  def mapToNewStructure(project : Project) : ProjectDependency = {
    // TODO: this is not correct. need to integrate previous dependency -> figure out whether leading ':' is mandatory -> it is
    copy(configuration, project.getTargetRootPath() ++ dependency)
  }

  override def mapToNewConfiguration(newConfig: String): Dependency = {
    copy(newConfig, dependency)
  }
}
case class FileDependency(configuration : String, dependency : String, quotationSign : String) extends Dependency {
  override def mapToNewConfiguration(newConfig: String): Dependency = {
    copy(newConfig, dependency)
  }
}
case class UnsupportedDependency(configuration : String, dependency : String, quotationSign : String) extends Dependency {
  override def mapToNewConfiguration(newConfig: String): Dependency = {
    copy(newConfig, dependency)
  }
}
