package vp.bench.bg.model

import java.nio.file.{Files, Path}

import vp.bench.bg.Utilities.createDictIfNotExists

import scala.sys.process.Process

case class JavaProject(override val pathString : String, override val subprojects : Seq[JavaProject], override val name : String, jar : Option[Path], override val srcSetMain : Path, override val srcSetTest : Path) extends Project(pathString, subprojects, name, srcSetMain, srcSetTest) {

  override def getRootProject(): JavaProject = {
    parent match {
      case None => this
      case Some(parent)
        if !parent.isInstanceOf[JavaProject] =>
          throw new Exception("JavaProject needs to have a JavaProject as parent.")
      case Some(parent) =>
        parent.asInstanceOf[JavaProject].getRootProject()
    }
  }

  // Returns a list of all subprojects including this.
  override def listAllProjectsRecursively(): Seq[JavaProject] = {
    val subprojects = listSubprojectsRecursively()
    Seq(this) ++ subprojects
  }

  // Returns a list of all subprojects excluding this.
  override def listSubprojectsRecursively(): Seq[JavaProject] = {
    val recursiveProjects = for {
      proj <- subprojects
    } yield {
      proj.listSubprojectsRecursively()
    }
    subprojects ++ recursiveProjects.flatten
  }

  // Returns a map from the relative jar-paths (from corresponding project) to the projects itself. Skips projects, that do not set the jar-property.
  def getJarToProjMap(): Map[Path, JavaProject] = {
    val allSubprojects = listAllProjectsRecursively()
    val projToJarMap = allSubprojects
      .filter(proj => proj.jar match {
        case None => false
        case _ => true
      }).map(proj => (proj, proj.jar.get)).toMap
    projToJarMap.map(_.swap)
  }

  // Returns the classpath for compiling against the project by collecting the absolute paths to all jars, stored by any subproject including self.
  def calcClasspath(): String = {
    val subprojClasspath = for {
      subp <- subprojects
    } yield {
      subp.calcClasspath()
    }

    jar match {
      case None
        if subprojClasspath.length == 0 =>
        ""
      case None =>
        subprojClasspath.mkString(";")
      case Some(elem)
        if subprojClasspath.length == 0 =>
        getAbsolutePathSrc().resolve(elem).toString
      case Some(elem) =>
        s"${getAbsolutePathSrc().resolve(elem).toString};${subprojClasspath.mkString(";")}"
    }
  }

  // Searches for a subproject with given projectpath (gradle-syntax, e.g. a:b)
  override def getSubprojectByName(namepath: String): Option[JavaProject] = {
    val splitNamePath = namepath.split(':').toList
    getSubprojectByName(splitNamePath)
  }

  // Searches for a subproject by following a list of projectnames.
  override def getSubprojectByName(path: List[String]): Option[JavaProject] = {
    path match {
      case Nil => Some(this)
      case l :: ls
        if l == "" => Some(this)
      case l :: ls =>
        subprojects.find(sub => sub.name == l) match {
          case None => None
          case Some(p) => p.getSubprojectByName(ls)
        }
    }
  }

  def getTestcaseListFromProject(initialisationPath: Path, unitTestScribePath: Path, srcmlPath: Path): Option[Path] = {
    val testcasesDir = initialisationPath.resolve(this.getProjectPathIncludingRoot.replace(':', '\\'))
    val testcasesFile = testcasesDir.resolve("idtestcases.txt")
    createDictIfNotExists(testcasesDir)
    try {
      val getTestcaseInput = Seq(unitTestScribePath.toString,
        "fid_testcase_save",
        "--loc", this.getAbsolutePathSrc.resolve(this.srcSetTest).toString,
        "--srcmlPath", srcmlPath.toString,
        "--outputLoc", testcasesFile.toString
      )
      // Comment for faster testing
      val result = Process.apply(Seq(unitTestScribePath.toString,
        "fid_testcase_save",
        "--loc", this.getAbsolutePathSrc.resolve(this.srcSetTest).toString,
        "--srcmlPath", srcmlPath.toString,
        "--outputLoc", testcasesFile.toString
      )).!
    } catch {
      case _ =>
    }

    if (Files.exists(testcasesFile)) {
      Some(testcasesFile)
    }
    else {
      None
    }
  }
}
