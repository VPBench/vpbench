package vp.bench.bg.model

import java.nio.file
import java.nio.file.{Files, Path}

import se.gu.vp.model.Asset
import vp.bench.bg.model.TestCase.parseLineToTestcase

import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.collection.immutable.{HashMap, ListMap}
import scala.collection.mutable

// TODO: Differentiate normal and rootProjects in a more explicit manner
// path:  If it is a rootProject, path = absolute path to project dir in source
//        If it is a normal Project, path = relative path from parent project to dir in source
// subprojects: List of all subprojects -> take care of setting parent in subprojects
// name:  Name of the project, assumes name and path is identical for normal projects, can be different for rootProjects (due to name being defined in settings.gradle)
// jar:   Relative path from project dir to corresponding jar, if it exists in source project after calling "gradle jar"
// srcSetMain/srcSetTest: relative path from project dir to main and test directory (specified in build.gradle -> sourceSets), default otherwise
class Project(val pathString : String, val subprojects : Seq[Project], val name : String, val srcSetMain : Path, val srcSetTest : Path) {
  val path: Path = file.Paths.get(pathString)

  // If parent == None, a Project is considered a RootProject
  var parent: Option[Project] = None

  // Set to true, once copyGradleProject is called for the project
  //var initialisedInRepo : mutable.LinkedHashMap[Asset, Boolean] = mutable.LinkedHashMap()
  //var initialised: Boolean = false

  // Relative path from parent project to the project directory in the target system
  var pathToGenTargetInRepo : ListMap[Asset, Path] = ListMap[Asset, Path]()
  //var pathToGenTargetInRepo : mutable.LinkedHashMap[Asset, Path] = mutable.LinkedHashMap[Asset,Path]()
  //var pathToGenTarget: Option[Path] = None

  // List of all fully identifiable testcases in this project
  var testcases: Seq[TestCase] = Seq()

  def getTargetRootPath(): String = {
    parent match {
      case None => s":$name"
      case Some(parent) => parent.getTargetRootPath()
    }
  }

  def getRootProject(): Project = {
    parent match {
      case None => this
      case Some(parent) => parent.getRootProject()
    }
  }

  def getParentProjects(): Seq[Project] = {
    parent match {
      case None => Nil
      case Some(parent) => parent.getParentProjects() ++ Seq(parent)
    }
  }

  def getProjectPathIncludingRoot(): String = {
    parent match {
      case None => name
      case Some(proj) => s"${proj.getProjectPathIncludingRoot()}:$name"
    }
  }

  // Returns relative path from RootProject to ProjectDir including all intermediate subprojects, if called on RootProject returns empty path ("")
  def getRelativePathFromRoot(): Path = {
    parent match {
      case None => file.Paths.get("")
      case Some(proj) => proj.getRelativePathFromRoot().resolve(path)
    }
  }

  // Returns absolute path to this project in target, if set. Otherwise, returns None.
  def getAbsolutePathTarget(repository: Asset): Option[Path] = {
    if (pathToGenTargetInRepo.keySet.contains(repository)) {
      parent match {
        case None => Some(pathToGenTargetInRepo(repository))
        case Some(proj) =>
          val recPathTarget = proj.getAbsolutePathTarget(repository)
          recPathTarget match {
            case None => None
            case Some(p) => Some(p.resolve(path))
        }
      }
    } else {
      None
    }
  }

  // Returns absolute path to this project in src.
  def getAbsolutePathSrc(): Path = {
    parent match {
      case None => path
      case Some(proj) => proj.getAbsolutePathSrc().resolve(path)
    }
  }

  def loadInTestcasesFromFile(file: Path): Unit = {
    testcases = Files.readAllLines(file)
      .map(line => parseLineToTestcase(line))
      .toList
  }

  // Returns a list of all subprojects including this.
  def listAllProjectsRecursively(): Seq[Project] = {
    val subprojects = listSubprojectsRecursively()
    Seq(this) ++ subprojects
  }

  // Returns a list of all subprojects excluding this.
  def listSubprojectsRecursively(): Seq[Project] = {
    val recursiveProjects = for {
      proj <- subprojects
    } yield {
      proj.listSubprojectsRecursively()
    }
    subprojects ++ recursiveProjects.flatten
  }

  // Searches for a subproject with given projectpath (gradle-syntax, e.g. a:b)
  def getSubprojectByName(namepath: String): Option[Project] = {
    val splitNamePath = namepath.split(':').toList
    getSubprojectByName(splitNamePath)
  }

  // Searches for a subproject by following a list of projectnames.
  def getSubprojectByName(path: List[String]): Option[Project] = {
    path match {
      case Nil => Some(this)
      case l :: ls
        if l == "" => Some(this)
      case l :: ls =>
        subprojects.find(sub => sub.name == l) match {
          case None => None
          case Some(p) => p.getSubprojectByName(ls)
        }
    }
  }
}

object Project {
}