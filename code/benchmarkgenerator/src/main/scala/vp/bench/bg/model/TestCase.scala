package vp.bench.bg.model

import java.nio.file
import java.nio.file.Path

case class TestCase(packageName: String, className: String, testName: String) {
  def getClassFilePath : Path = {
    val packagePath = file.Paths.get(packageName.replace('.','/'))
    packagePath.resolve(s"$className.class")
  }

  def toXML : String = {
    val packageXML = s"<packageName>$packageName</packageName>"
    val classXML = s"<className>$className</className>"
    val testXML = s"<testName>$testName</testName>"
    val xml = List(
      "<testcase>",
      packageXML,
      classXML,
      testXML,
      "</testcase>"
    ).mkString("\n")

    xml
  }

  override def toString: String = s"$packageName.$className.$testName"
}

object TestCase {
  def parseLineToTestcase(line: String) : TestCase = {
    val splitLine = line.split("  ")
    TestCase(splitLine(0), splitLine(1), splitLine(2))
  }

  def parseDotRepresentationToTestcase(line: String) : TestCase = {
    val splitLine = line.split('.')
    TestCase(splitLine.slice(0,splitLine.length - 2).mkString("."), splitLine(splitLine.length - 2), splitLine.last)
  }
}