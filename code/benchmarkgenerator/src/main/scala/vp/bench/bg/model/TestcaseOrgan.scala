package vp.bench.bg.model

case class TestcaseOrgan(importStatements: String, testcaseStatements: String, attributesContent: Option[String], beforeContent: Option[String], beforeEachContent: Option[String], beforeAllContent: Option[String], beforeClassContent: Option[String], constructorContent: Option[String])//, localFunctions: List[List[String]])
