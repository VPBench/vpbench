package vp.bench.bg.model

import java.nio.file.Path
import se.gu.vp.model.Asset

// Records precalculated values for transplanting test cases (for performance reasons). These values can be recreated using insertionPoint, testCase and donorProject. Exception is clonedAssetMap as this is calculated semi-randomly, but this can also be recreated if one reuses the same random seed within a transaction. Could be a todo.
case class TransplantationCache(testcaseOrgan: TestcaseOrgan, externalFileDependencies: Set[Tuple2[Project,Seq[Path]]], clonedAssetMap: Map[Path, Asset])
