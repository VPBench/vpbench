package vp.bench.bg.operations

import java.nio.file
import java.nio.file.{Files, Path}
import java.util

import vp.bench.bg.FileSystemToVPOpConverter.checkCreateVPOpsWithCloning
import vp.bench.bg.Utilities.{findRepoOnUpPath, getImmediateAncestorProjectInRepo}
import vp.bench.bg.model.{Project, TestCase, TestcaseOrgan, TransplantationCache}
import se.gu.vp.model.{Asset, AssetType, BlockType, ClassType, Feature, FileType, FolderType, MethodType, RepositoryType, True, VPRootType}
import se.gu.vp.operations.{AddAsset, AddFeatureToFeatureModel, CloneAsset, CreateMetadata, MapAssetToFeature, Operation, RunImmediately}
import se.gu.vp.operations.Utilities.{addToOptionalSeq, findAssetByName, findFileParentAsset, getassetbyname, transformASTToList}
import se.gu.vp.operations.metadata.MetadataConverter
import se.gu.vp.operations.metadata.MetadataConverter.getPathToAsset
import vp.bench.bg.HandleJavaDependencies.findClosestAncestorFile
import vp.bench.bg.metadata.AddExternalFeatureMetadata
import vp.operations.AddExternalAsset

import scala.collection.mutable
import scala.util.Random

class AddExternalFeature(val insertionPoint: Tuple2[Asset, Int], val project: Project, val testCase: TestCase, val parentFeature: Feature, val randomSeed: Int, val transplantationCache: TransplantationCache) extends ITransplantFeatureViaTest {
  // These files should not be added to the asset-tree, as they contain build-information (.gradle and build) or metadata, that was generated from the asset tree in the first place (featuremodel, feature-to-file and feature-to-folder)
  val rootAsset = insertionPoint._1.getRoot().get
  val tcOrgan = transplantationCache.testcaseOrgan
  val externalFileDependencies = transplantationCache.externalFileDependencies
  val clonedAssetMap = transplantationCache.clonedAssetMap

  def replayOperations(name: String, contentOpt: Option[String], feat: Feature, assetType: AssetType, parent: Asset, index: Int) = {
    contentOpt match {
      case None => Nil // What to do?
      case Some(content) =>
        val asset = Asset(name = name, assetType = assetType, content = Some(content.split("\n").toList))
        val addOp = AddAsset(asset, parent, Some(index))
        val mapAF = MapAssetToFeature(asset = asset, feature = feat)
        addOp :: mapAF :: Nil
    }
  }

  def replayOperationsEntireOrgan(feature: Feature): List[Operation] = {
    // TODO: Method is not parsed in multiple assets + what are the repercussions of adding methods for adding and later removing features -> would need to be tested
    replayOperations("attributes", tcOrgan.attributesContent, feature, BlockType, insertionPoint._1.parent.get, 0) ++
    replayOperations("before", tcOrgan.beforeContent, feature, BlockType, insertionPoint._1, insertionPoint._2) ++
    replayOperations("beforeEach", tcOrgan.beforeEachContent, feature, BlockType, insertionPoint._1, insertionPoint._2) ++
    replayOperations("beforeAll", tcOrgan.beforeAllContent, feature, BlockType, insertionPoint._1, insertionPoint._2) ++
    replayOperations("beforeClass", tcOrgan.beforeClassContent, feature, BlockType, insertionPoint._1, insertionPoint._2) ++
    replayOperations("constructor", tcOrgan.constructorContent, feature, BlockType, insertionPoint._1, insertionPoint._2)
  }

  override def perform(): Boolean = {
    var operationListBuilder = mutable.ListBuffer[Operation]()

    // Find and get repository in which testcase is implanted
    val insertionRepo = findRepoOnUpPath(insertionPoint._1).get
    val insertionRepoPath = file.Paths.get(insertionRepo.name)

    // find insertion point for import statements
    val fileParent = findFileParentAsset(insertionPoint._1).get

    val testcaseAsset = Asset("testcase", BlockType, content = Some(tcOrgan.testcaseStatements.split('\n').toList))
    val testcaseImportAsset = Asset("testcaseImport", BlockType, content = Some(tcOrgan.importStatements.split('\n').toList))
    val feature = Feature(testCase.toString)

    operationListBuilder += AddFeatureToFeatureModel(feature = feature, parent = parentFeature)
    operationListBuilder += AddAsset(testcaseAsset, insertionPoint._1, Some(insertionPoint._2))
    operationListBuilder += MapAssetToFeature(testcaseAsset, feature)

    // assuming first child of fileAsset contains package and import statements -> similar to above (including copying the asset)
    val firstClassAsset = fileParent.children.filter(ch => ch.assetType == ClassType).head
    val importPosition = fileParent.children.indexOf(firstClassAsset)
    operationListBuilder += AddAsset(testcaseImportAsset, fileParent, Some(importPosition))
    operationListBuilder += MapAssetToFeature(testcaseImportAsset, feature)

    replayOperationsEntireOrgan(feature).map(op => operationListBuilder += op)

    val targetPathsToAssets = transformASTToList(rootAsset)
      .filter(asset => List(VPRootType, RepositoryType, FolderType, FileType).contains(asset.assetType))
      .map(asset => {
        val absPath = file.Paths.get(asset.name)
        (absPath, asset)
      }).toMap
    val assetToTargetPath = targetPathsToAssets.map(elem => elem.swap)

    val rootPath = assetToTargetPath(rootAsset)
    // this assumes no cross-root-project dependencies (which should be a reasonable constraint) <- related to passing only the selected project here
    val assetTreeAdditions = checkCreateVPOpsWithCloning(rootPath, targetPathsToAssets, project, Some(clonedAssetMap))
    assetTreeAdditions.foreach(elem => operationListBuilder += elem)

    mapFileDependenciesToFeature(insertionRepo, externalFileDependencies, feature).foreach(elem => operationListBuilder += elem)

    val closestAncestorBuildFile = findClosestAncestorFile(insertionPoint._1, "build.gradle").get
    val repoRootBuildFileAsset = getassetbyname(closestAncestorBuildFile.toString, insertionRepo).get
    val buildFileContent = Files.readString(closestAncestorBuildFile).split('\n').toList
    if (buildFileContent != repoRootBuildFileAsset.content.get) {
      operationListBuilder += ChangeAssetContent(repoRootBuildFileAsset, buildFileContent)
    }

    val repoRootSettingsFileAsset = insertionRepo.children.filter(asset => asset.name.endsWith("settings.gradle")).head
    val repoRootSettingsFile = insertionRepoPath.resolve("settings.gradle")
    val settingsFileContent = Files.readString(repoRootSettingsFile).split('\n').toList
    if (settingsFileContent != repoRootSettingsFileAsset.content.get) {
      operationListBuilder += ChangeAssetContent(repoRootSettingsFileAsset, settingsFileContent)
    }

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  def createMetadata = {
    try {
      val insertionPointMetadata = (MetadataConverter.convertAssetToMetadata(insertionPoint._1),insertionPoint._2)
      val clonedAssetMapMetadata = for {
        (path, asset) <- clonedAssetMap
      } yield {
        (path.toString, MetadataConverter.convertAssetToMetadata(asset))
      }

      val parentFeatureMetadata = MetadataConverter.convertFeatureToMetadata(parentFeature)
      val createdMetadata = AddExternalFeatureMetadata(insertionPoint = insertionPointMetadata, donor = project.name, testCase = testCase, parentFeature = parentFeatureMetadata, randomSeed = randomSeed, clonedAssetMap = clonedAssetMapMetadata)
      metadata = Some(createdMetadata)
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }

  def mapFileDependenciesToFeature(insertionRepo: Asset, externalFileDependencies: Set[(Project, Seq[Path])], feature: Feature) : List[MapAssetToFeature] = {
    val assetToFeatureMaps = for {
      filesPerProj <- externalFileDependencies
      project = filesPerProj._1
      fileDeps = filesPerProj._2
      filePath <- fileDeps
      asset = mapPathToAsset(project, filePath, insertionRepo)
    } yield {
      MapAssetToFeature(asset, feature)
    }
    assetToFeatureMaps.toList
  }

  def mapPathToAsset(project: Project, filePath: Path, repo: Asset) : Asset = {
    val projPathInRepo = project.getAbsolutePathTarget(repo)
    val assetPath = project.getAbsolutePathTarget(repo).get.resolve(project.srcSetMain).resolve(filePath)
    val asset = getassetbyname(assetPath.toString, repo)
    if (asset.isEmpty) {
      println(s"Asset ${assetPath.toString} is missing.")
    }
    asset.get
  }
}

object AddExternalFeature {
  def apply(insertionPoint: Tuple2[Asset, Int], project: Project, testCase: TestCase, parentFeature: Feature, randomSeed: Int, transplantationCache: TransplantationCache, runImmediately: Boolean = true, createMetadata: Boolean = true): AddExternalFeature = {
    if (runImmediately && createMetadata) {
      new AddExternalFeature(insertionPoint, project, testCase, parentFeature, randomSeed, transplantationCache) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddExternalFeature(insertionPoint, project, testCase, parentFeature, randomSeed, transplantationCache) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddExternalFeature(insertionPoint, project, testCase, parentFeature, randomSeed, transplantationCache) with CreateMetadata
    } else {
      new AddExternalFeature(insertionPoint, project, testCase, parentFeature, randomSeed, transplantationCache)
    }
  }
}