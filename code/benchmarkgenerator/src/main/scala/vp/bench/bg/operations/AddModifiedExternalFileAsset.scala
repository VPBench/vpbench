package vp.bench.bg.operations

import java.nio.file.{Files, Path}

import se.gu.vp.model.{Asset, FileType, FolderType}
import se.gu.vp.operations.{AddAsset, CreateMetadata, Operation, RunImmediately}
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Utilities.{addToOptionalSeq, getImmediateAncestorFeatureModel, getassetbyname}
import se.gu.vp.operations.metadata.MetadataConverter
import vp.bench.bg.metadata.AddModifiedExternalFileAssetMetadata
import vp.operations.AddExternalAsset

import scala.collection.mutable

class AddModifiedExternalFileAsset(pathToAsset: Path, parentAsset: Option[Asset], content: List[String], rootAsset: Option[Asset] = None, parentAssetPath: Option[String] = None) extends AddExternalAsset(pathToAsset: Path, parentAsset: Option[Asset], None, rootAsset: Option[Asset], parentAssetPath: Option[String]){

  override def perform(): Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    val parent = if (parentAsset.isDefined) {
      parentAsset.get
    } else {
      getassetbyname(parentAssetPath.get, rootAsset.get).get
    }

    val createdAsset = Asset(pathToAsset.toString, FileType, content = Some(content))

    val fileName = createdAsset.name.split('\\').last
    createdAsset.name = parent.name + "\\" + fileName

    // Not 100%-sure, how to best manage the situation, as this operation is basically a conceptual extension of AddAsset, but the added asset is only created in here
    operationListBuilder += AddAsset(createdAsset, parent, None, createMetadata = false)

    newAsset = Some(createdAsset)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    // Should not work in case of lazy evaluations, that depend on each other, as the parent asset will not be able to be found using getassetbyname, before previous operation is executed
    // But lazy evaluations are not used currently, when introducing them -> take care of metadata
    try {
      val parent = if (parentAsset.isDefined) {
        parentAsset.get
      } else {
        getassetbyname(parentName.get, rootAsset.get).get
      }

      val createdMetadata = AddModifiedExternalFileAssetMetadata(pathToAsset = pathToAsset.toString, parentAsset = MetadataConverter.convertAssetToMetadata(parent), content = content)
      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object AddModifiedExternalFileAsset {
  def apply(pathToAsset: Path, parentAsset: Asset, content: List[String], runImmediately: Boolean = true, createMetadata: Boolean = true): AddModifiedExternalFileAsset = {
    if (runImmediately && createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, Some(parentAsset), content) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, Some(parentAsset), content) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, Some(parentAsset), content) with CreateMetadata
    } else {
      new AddModifiedExternalFileAsset(pathToAsset, Some(parentAsset), content)
    }
  }

  def apply(pathToAsset: Path, rootAsset: Asset, parentAsset: String, content: List[String], runImmediately: Boolean, createMetadata: Boolean): AddModifiedExternalFileAsset = {
    if (runImmediately && createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, None, content, Some(rootAsset), Some(parentAsset)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, None, content, Some(rootAsset), Some(parentAsset)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddModifiedExternalFileAsset(pathToAsset, None, content, Some(rootAsset), Some(parentAsset)) with CreateMetadata
    } else {
      new AddModifiedExternalFileAsset(pathToAsset, None, content, Some(rootAsset), Some(parentAsset))
    }
  }
}