package vp.bench.bg.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.Asset
import se.gu.vp.operations.{ChangeAsset, CreateMetadata, Operation, RunImmediately}
import se.gu.vp.operations.Utilities.addToOptionalSeq
import se.gu.vp.operations.metadata.MetadataConverter
import vp.bench.bg.metadata.ChangeAssetContentMetadata

import scala.collection.mutable

class ChangeAssetContent (val assetToChange:Asset, val newContent:List[String]) extends ChangeAsset {

  override def perform(): Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    assetToChange.content = Some(newContent)

    operationListBuilder += ChangeAsset(assetToChange)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val createdMetadata = ChangeAssetContentMetadata(assetToChange = MetadataConverter.convertAssetToMetadata(assetToChange), newContent = newContent)
      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object ChangeAssetContent{
  def apply(assetToChange: Asset, newContent: List[String], runImmediately: Boolean = true, createMetadata: Boolean = true): ChangeAssetContent = {
    if (runImmediately && createMetadata) {
      new ChangeAssetContent(assetToChange, newContent) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new ChangeAssetContent(assetToChange, newContent) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new ChangeAssetContent(assetToChange, newContent) with CreateMetadata
    } else {
      new ChangeAssetContent(assetToChange, newContent)
    }
  }
}

