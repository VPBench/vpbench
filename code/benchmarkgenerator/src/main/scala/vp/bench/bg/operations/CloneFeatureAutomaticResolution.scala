package vp.bench.bg.operations

import se.gu.vp.model.{Asset, AssetPathElement, AssetType, BlockType, ClassType, Feature, FeaturePathElement, FieldType, MethodType, Path, TraceDatabase, VPRootType}
import se.gu.vp.operations.Utilities.{resolveAssetPath, resolveFeaturePath}
import se.gu.vp.operations.metadata.MetadataConverter
import se.gu.vp.operations.{CloneAsset, CloneFeature, CreateMetadata, MapAssetToFeature, Operation, RunImmediately, Utilities}
import vp.bench.bg.metadata.CloneFeatureAutomaticResolutionMetadata

import scala.collection.mutable

class CloneFeatureAutomaticResolution(sourceFeature: Path, targetParentFeature: Path) extends CloneFeature(sourceFeature, targetParentFeature) {
  var testCounter = 0

  override def findSuitableInsertionPoint(sourceAsset: Asset, cloneType: AssetType, sourceParent: Asset, targetParent: Asset) : Option[Asset] = {
    // Add to specific position in case slice top-asset is code-level!
    if (List(ClassType, MethodType, BlockType, FieldType).contains(cloneType)) {
      // Inside of a file
      val preSiblings = sourceParent.children.takeWhile(child => child != sourceAsset).zipWithIndex
      val insertionIndex = calculateInsertionPoint(preSiblings, targetParent)
      insertionIndex match {
        case 0 => None
        case _ => Some(targetParent.children(insertionIndex-1))
      }
    } else {
      // Outside of files
      None
    }
  }

  override def cloneASTSlice(asset:Asset, target:Asset,feature:Feature):List[Operation]={
    println("Cloning asset: " + asset.name)
    val timestampBegin = System.nanoTime
    val listBuilder = mutable.ListBuffer[Operation]()

    val assetClone = tryGetAssetClone(asset, target)
    if(assetClone.isEmpty) {
      val ancestors = Utilities.getAncestors(asset).filterNot(_.assetType == VPRootType)
      if (ancestors.length == 1) { // Repository is not cloned into target, asset is immediately cloned without slicing
        // TODO: insertAfter does not yet exist for CloneFeature -> after which asset should the slice be inserted?
        listBuilder += CloneAsset(source = asset, targetContainer = target, insertAfter = None, cloneName = None, deepCloneFeature = false) //returned. To be added in sub-assets
        val clone = target.children.find(ch => TraceDatabase.traces.find(t => t.target == ch).isDefined).get
        listBuilder += MapAssetToFeature(clone,feature)
        updateVersion(target)
      }

      else {
        var cloned = false
        for (i <- (0 to ancestors.length - 1).reverse) {
          // && !cloned for breaking out of loop
          if (!cloned) {
            val ancestorCloneOpt = tryGetAssetClone(ancestors(i), target)
            if (ancestorCloneOpt.isDefined) {
              val ancestorClone = ancestorCloneOpt.get
              cloned = true
              val (slice, operations) = performSlice(asset, ancestors(i), ancestorClone)
              operations.foreach(op => listBuilder += op)

              updateVersion(ancestorClone)
              // Get assetClone using slice-property, i.e. until the assetClone there can only be a single child on each layer
              val assetclone = tryGetAssetClone(asset,target).get
              listBuilder += MapAssetToFeature(assetclone,feature)
            }
          }
        }
        if (cloned == false) {
          val (slice, operations) = performSlice(asset, ancestors.reverse.last, target)
          operations.foreach(op => listBuilder += op)

          updateVersion(target)
          val assetclone = tryGetAssetClone(asset,target).get
          listBuilder += MapAssetToFeature(assetclone,feature)
        }
      }
    }
    else{
      listBuilder += MapAssetToFeature(assetClone.get, feature)
    }
    val timestampEnd = System.nanoTime
    val duration = timestampEnd - timestampBegin
    println("Duration of cloneASTSlice: " + duration)
    listBuilder.toList
  }

  def calculateInsertionPoint(preSiblings: List[(Asset, Int)], parentClone: Asset) : Int = {
    // get last sibling in original which is cloned inside parentClone and insert it afterwards
    preSiblings match {
      case Nil => 0
      case _
        if tryGetAssetClone(preSiblings.last._1, parentClone).isDefined =>
        preSiblings.last._2 + 1
      case _   => calculateInsertionPoint(preSiblings.dropRight(1), parentClone)
    }
  }

  override def createMetadata(): Unit = {
    try {
      val s_assetpath = sourceFeature.path.collect { case a: AssetPathElement => a }
      val sourceassetpath = resolveAssetPath(sourceFeature.root, s_assetpath)
      val srcfeatureModel = sourceassetpath.last.featureModel.get
      val s_featurepath = sourceFeature.path.collect { case a: FeaturePathElement => a }
      val srcFeat = resolveFeaturePath(srcfeatureModel, s_featurepath).get

      val sourceFeatureMD = MetadataConverter.convertFeatureToMetadata(srcFeat)

      val t_assetpath = targetParentFeature.path.collect { case a: AssetPathElement => a }
      val targetassetpath = resolveAssetPath(targetParentFeature.root, t_assetpath)
      val tgtfeatureModel = targetassetpath.last.featureModel.get
      val t_featurepath = targetParentFeature.path.collect { case a: FeaturePathElement => a }
      val targetFeat = resolveFeaturePath(tgtfeatureModel, t_featurepath).get

      val targetFeatureMD = MetadataConverter.convertFeatureToMetadata(targetFeat)

      metadata = Some(CloneFeatureAutomaticResolutionMetadata(clonedFeature = sourceFeatureMD, newParentFeature = targetFeatureMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }

  def tryGetAssetClone(asset: Asset, targetAST: Asset) : Option[Asset] = {
    val result = TraceDatabase.traces.par.find(trace => trace.source == asset && Utilities.astContainsAsset(trace.target,targetAST))
    if (result.isDefined) {
      Some(result.get.target)
    } else { None }
  }
}

object CloneFeatureAutomaticResolution {
  def apply(sourceFeature: Path, targetParentFeature: Path, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneFeatureAutomaticResolution = {
    if (runImmediately && createMetadata) {
      new CloneFeatureAutomaticResolution(sourceFeature, targetParentFeature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneFeatureAutomaticResolution(sourceFeature, targetParentFeature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneFeatureAutomaticResolution(sourceFeature, targetParentFeature) with CreateMetadata
    } else {
      new CloneFeatureAutomaticResolution(sourceFeature, targetParentFeature)
    }
  }
}

