package vp.bench.bg.operations

import java.nio.file
import java.nio.file.{Files, Path => FilePath}

import vp.bench.bg.FileSystemToVPOpConverter.checkCreateVPOpsWithCloning
import vp.bench.bg.HandleJavaDependencies.{addNewDependency, addSubprojectsToSettings, copyGradleProject}
import vp.bench.bg.Utilities.{cleanDirectory, findRepoOnUpPath, getImmediateAncestorProjectInRepo}
import vp.bench.bg.model.Project
import se.gu.vp.model._
import se.gu.vp.operations.{CloneFeature, CreateMetadata, MapAssetToFeature, Operation, RunImmediately, SerialiseAssetTree}
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations.metadata.MetadataConverter
import vp.bench.bg.metadata.CloneFeatureWProjectInitializationMetadata
import vp.bench.bg.model.{Project, ProjectDependency}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class CloneFeatureWProjectInitialization(val sourceFPath: Path, val targetFPath: Path, rootProjects: Seq[Project], targetPathString: String) extends ICloneFeatureWithIntegration {
  val targetPath = file.Paths.get(targetPathString)

  def findEntryPoints(asset: Asset) = {
    asset.children.groupBy(as => as.name).filter(pair => pair._2.length == 2)
  }

  def moveInParallel(original: Asset, clone: Asset, targetFeature: Feature, operationListBuilder: ListBuffer[Operation]) : Unit = {
    if(original.assetType == FileType) {
      operationListBuilder += MapAssetToFeature(original, targetFeature)
    } else {
      for (child <- clone.children) {
        val origChild = original.children.find(as => as.name == child.name)
        if(origChild.isDefined) {
          moveInParallel(origChild.get, child, targetFeature, operationListBuilder)
        } else {
          original.children = original.children ++ List(child)
          child.parent = Some(original)
        }
      }
    }
    // remove clone from parent
    val cloneParent = clone.parent.get
    val cloneIndex = cloneParent.children.indexOf(clone)
    cloneParent.children = cloneParent.children.take(cloneIndex) ++ cloneParent.children.drop(cloneIndex + 1)
    clone.parent = None
  }

  def removeDuplicates(currentAsset: Asset, targetFeature: Feature, operationListBuilder: ListBuffer[Operation]): Unit = {
    val entryPoints = findEntryPoints(currentAsset)
    entryPoints.foreach(entryPoint => moveInParallel(entryPoint._2(0), entryPoint._2(1), targetFeature, operationListBuilder))
    currentAsset.children
      .filter(ch => ch.assetType != FileType && (entryPoints.isEmpty || !entryPoints.values.toList.flatten.contains(ch)))
      .foreach(nodup => removeDuplicates(nodup, targetFeature, operationListBuilder))
  }

  override def perform : Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    operationListBuilder += CloneFeatureAutomaticResolution(sourceFeature = sourceFPath, targetParentFeature = targetFPath)
    val root = sourceFPath.root
    val sourceRepo = resolveAssetPath(sourceFPath.root, sourceFPath.path.takeWhile(pe => pe.isInstanceOf[AssetPathElement]).map(pe => pe.asInstanceOf[AssetPathElement])).last
    val targetRepo = resolveAssetPath(targetFPath.root, targetFPath.path.takeWhile(pe => pe.isInstanceOf[AssetPathElement]).map(pe => pe.asInstanceOf[AssetPathElement])).last

    val sourceFeature = resolveFeaturePath(sourceRepo.featureModel.get, sourceFPath.path.dropWhile(pe => pe.isInstanceOf[AssetPathElement]).map(pe => pe.asInstanceOf[FeaturePathElement])).last
    val sourceMappedAssets = sourceFeature.mappedAssets(sourceRepo)//.map(as => getImmediateAncestorProjectInRepo())

    val targetFeature = FeatureTraceDatabase.traces
      .filter(ft => try {
      ft.source == sourceFeature && findRepoOnUpPath(ft.target.getFeatureModelRoot.get.featureModel.get.containingAsset.get).get == targetRepo
    } catch {
      case ex: Exception => false
    })(0).target

    removeDuplicates(targetRepo, targetFeature, operationListBuilder)

    cleanDirectory(targetPath, false)
    SerialiseAssetTree(sourceFPath.root, targetPathString)

    // Get all projects, that need to be active in targetRepo
    val sourceMappedRequiredProjects = for {
      rootProj <- rootProjects
      proj <- rootProj.listAllProjectsRecursively.map(proj => (proj, proj.getAbsolutePathTarget(sourceRepo)))
      if proj._2.isDefined
      if sourceMappedAssets.exists(asset => asset.name.startsWith(proj._2.get.toString))
    } yield {
      val project = proj._1

      // This is done for all projects from which source elements are required and their ancestors,
      // since its not possible to trace back the testcase defining project from the feature.
      // This can be optimized to only use the immediate ancestors for project dependencies, but it should not matter at the moment
      // since dependency circles cannot be introduced, as no project can depend on the main project of a repository.

      // Add subproject dependency to root build-gradle.
      val rootBuildFile = file.Paths.get(targetRepo.name).resolve("build.gradle")
      val projectDependency = ProjectDependency("implementation", project.getProjectPathIncludingRoot(), "'")
      addNewDependency(rootBuildFile, projectDependency)

      project
    }

    // We are assuming no cross-dependencies between project beneath different root projects and
    // due to insertion point limitations these dependencies cannot be introduced differently
    assert(sourceMappedRequiredProjects.map(proj => proj.getRootProject).distinct.length <= 1)

    for {
      reqProject <- sourceMappedRequiredProjects
      if reqProject.getAbsolutePathTarget(targetRepo).isEmpty
    } yield {
      val (targetPath, targetDir) = if (reqProject.getRootProject == reqProject) {
        val target = file.Paths.get(targetRepo.name).resolve(reqProject.name)
        (target, target)
      } else {
        (file.Paths.get(reqProject.name), file.Paths.get(targetRepo.name).resolve(reqProject.getRootProject.name))
      }
      reqProject.pathToGenTargetInRepo = reqProject.pathToGenTargetInRepo + ((targetRepo,targetPath))

      // Initialize project (including project dependencies)
      copyGradleProject(project = reqProject, targetPath = targetDir, dirName = None, targetRepo)
    }

    val requiredRootProject = sourceMappedRequiredProjects.map(proj => proj.getRootProject).distinct(0)

    // Using sourceMappedRequiredProjects here does not suffice as project dependencies might also have been added additionally
    // Identify initialised subprojects that need to be in settings.gradle
    val initProjects = requiredRootProject.listAllProjectsRecursively.filter(proj => proj.pathToGenTargetInRepo.keySet.contains(targetRepo))

    // Add new subprojects from GitProject to TargetRootProject -> needs to happen at every addition of a new feature (at least check for necessity)
    val settingsFile = file.Paths.get(targetRepo.name).resolve("settings.gradle")
    addSubprojectsToSettings(settingsFile, initProjects)

    val targetPathsToAssets = transformASTToList(root)
      .filter(asset => List(VPRootType, RepositoryType, FolderType, FileType).contains(asset.assetType))
      .map(asset => {
        val absPath = file.Paths.get(asset.name)
        (absPath, asset)
      }).toMap
    val assetToTargetPath = targetPathsToAssets.map(elem => elem.swap)

    val rootPath = assetToTargetPath(root)
    // this assumes no cross-root-project dependencies (which should be a reasonable constraint) <- related to passing only the selected project here
    val assetTreeAdditions = checkCreateVPOpsWithCloning(rootPath, targetPathsToAssets, requiredRootProject, None)
    assetTreeAdditions.foreach(elem => operationListBuilder += elem)

    // Add Project Dependencies to AT
    val rootBuildFile = file.Paths.get(targetRepo.name).resolve("build.gradle")
    val rootBuildFAsset = getassetbyname(rootBuildFile.toString, targetRepo).get
    val rootBuildFContent = Files.readString(rootBuildFile).split('\n').toList

    operationListBuilder += ChangeAssetContent(rootBuildFAsset, rootBuildFContent)

    // Change Settings File
    val rootSettingsFile = file.Paths.get(targetRepo.name).resolve("settings.gradle")
    val rootSettingsFAsset = getassetbyname(rootSettingsFile.toString, targetRepo).get
    val rootSettingsFContent = Files.readString(rootSettingsFile).split('\n').toList

    operationListBuilder += ChangeAssetContent(rootSettingsFAsset, rootSettingsFContent)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata

    true
  }

  override def createMetadata(): Unit = {
    try {
      val s_assetpath = sourceFPath.path.collect { case a: AssetPathElement => a }
      val sourceassetpath = resolveAssetPath(sourceFPath.root, s_assetpath)
      val srcfeatureModel = sourceassetpath.last.featureModel.get
      val s_featurepath = sourceFPath.path.collect { case a: FeaturePathElement => a }
      val srcFeat = resolveFeaturePath(srcfeatureModel, s_featurepath).get

      val sourceFeatureMD = MetadataConverter.convertFeatureToMetadata(srcFeat)

      val t_assetpath = targetFPath.path.collect { case a: AssetPathElement => a }
      val targetassetpath = resolveAssetPath(targetFPath.root, t_assetpath)
      val tgtfeatureModel = targetassetpath.last.featureModel.get
      val t_featurepath = targetFPath.path.collect { case a: FeaturePathElement => a }
      val targetFeat = resolveFeaturePath(tgtfeatureModel, t_featurepath).get

      val targetFeatureMD = MetadataConverter.convertFeatureToMetadata(targetFeat)

      metadata = Some(CloneFeatureWProjectInitializationMetadata(source = sourceFeatureMD, target = targetFeatureMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object CloneFeatureWProjectInitialization {
  def apply(sourceFPath: Path, targetFPath: Path, rootProjects: Seq[Project], targetPathString: String, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneFeatureWProjectInitialization = {
    if (runImmediately && createMetadata) {
      new CloneFeatureWProjectInitialization(sourceFPath, targetFPath, rootProjects, targetPathString) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneFeatureWProjectInitialization(sourceFPath, targetFPath, rootProjects, targetPathString) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneFeatureWProjectInitialization(sourceFPath, targetFPath, rootProjects, targetPathString) with CreateMetadata
    } else {
      new CloneFeatureWProjectInitialization(sourceFPath, targetFPath, rootProjects, targetPathString)
    }
  }
}