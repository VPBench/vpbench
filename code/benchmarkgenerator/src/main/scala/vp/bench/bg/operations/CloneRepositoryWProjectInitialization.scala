package vp.bench.bg.operations

import se.gu.vp.model.Asset
import se.gu.vp.operations.{CloneRepository, CreateMetadata, Operation, RunImmediately}
import java.nio.file

import se.gu.vp.operations.metadata.CloneRepositoryMetadata
import vp.bench.bg.metadata.CloneRepositoryWProjectInitializationMetadata
import vp.bench.bg.model.Project

import scala.collection.immutable.HashMap
import scala.collection.mutable

class CloneRepositoryWProjectInitialization(source: Asset, cloneName: String, rootProjects: Seq[Project]) extends CloneRepository(source = source, cloneName = Some(cloneName), targetContainer = Some(source.parent.get)) {
  override def perform : Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()
    val rootAsset = source.parent.get

    operationListBuilder += CloneRepository(source = source, cloneName = Some(cloneName), targetContainer = Some(rootAsset), createMetadata = true)
    val clone = rootAsset.children.filter(a => a.name.split('\\').last == cloneName)(0)

    for {
      rootProj <- rootProjects
      proj <- rootProj.listAllProjectsRecursively
      if proj.getAbsolutePathTarget(source).isDefined
    } yield {
      // analogue to setting in copyGradleProjectWrapper, where initialization normally takes place (here via deep copy, initialization already happened -> only repos need to be set in projects)
      val targetPath = if (rootProj == proj) {
        file.Paths.get(clone.name).resolve(rootProj.name)
      } else {
        file.Paths.get(proj.name)
      }
      proj.pathToGenTargetInRepo = proj.pathToGenTargetInRepo + ((clone,targetPath))
    }

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata

    true
  }

  override def createMetadata(): Unit = {
    try {
      super.createMetadata
      val sourceMD = metadata.get.asInstanceOf[CloneRepositoryMetadata].source

      val createdMetadata = CloneRepositoryWProjectInitializationMetadata(clonedRepo = sourceMD, cloneName = cloneName)
      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object CloneRepositoryWProjectInitialization {
  def apply(source: Asset, cloneName: String, rootProjects: Seq[Project], runImmediately: Boolean = true, createMetadata: Boolean = true): CloneRepositoryWProjectInitialization = {
    if (runImmediately && createMetadata) {
      new CloneRepositoryWProjectInitialization(source, cloneName, rootProjects) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneRepositoryWProjectInitialization(source, cloneName, rootProjects) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneRepositoryWProjectInitialization(source, cloneName, rootProjects) with CreateMetadata
    } else {
      new CloneRepositoryWProjectInitialization(source, cloneName, rootProjects)
    }
  }
}
