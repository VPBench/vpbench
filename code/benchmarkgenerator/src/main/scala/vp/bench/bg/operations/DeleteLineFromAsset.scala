package vp.bench.bg.operations

import se.gu.vp.model.Asset
import se.gu.vp.operations.{ChangeAsset, CreateMetadata, Operation, RunImmediately}
import se.gu.vp.operations.Utilities.addToOptionalSeq
import se.gu.vp.operations.metadata.MetadataConverter
import vp.bench.bg.metadata.DeleteLineFromAssetMetadata

import scala.collection.mutable

class DeleteLineFromAsset(val assetToChange:Asset, val lineNumber: Int) extends ChangeAsset {

  override def perform(): Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    val content = assetToChange.content.get
    val indexedContent = for {
      index <- 0 until content.length
      line = content(index)
    } yield {
      (index,line)
    }

    val newContent = indexedContent
      .filter(tuple => tuple._1 != lineNumber)
      .sortBy(tuple => tuple._1)
      .toList
      .map(tuple => tuple._2)

    operationListBuilder += ChangeAssetContent(assetToChange, newContent, createMetadata = false)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val createdMetadata = DeleteLineFromAssetMetadata(assetToChange = MetadataConverter.convertAssetToMetadata(assetToChange), lineNumber = lineNumber)
      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object DeleteLineFromAsset {
  def apply(assetToChange: Asset, lineNumber: Int, runImmediately: Boolean = true, createMetadata: Boolean = true): DeleteLineFromAsset = {
    if (runImmediately && createMetadata) {
      new DeleteLineFromAsset(assetToChange, lineNumber) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new DeleteLineFromAsset(assetToChange, lineNumber) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new DeleteLineFromAsset(assetToChange, lineNumber) with CreateMetadata
    } else {
      new DeleteLineFromAsset(assetToChange, lineNumber)
    }
  }
}