package vp.bench.bg.operations

import se.gu.vp.model.Path
import se.gu.vp.operations.Operation

trait ICloneFeatureWithIntegration extends Operation {
  val sourceFPath: Path
  val targetFPath: Path
}
