package vp.bench.bg.operations

import vp.bench.bg.model.Project
import se.gu.vp.model.Asset
import se.gu.vp.operations.Operation
import vp.bench.bg.model.{Project, TestCase}

trait ITransplantFeatureViaTest extends Operation {
  val insertionPoint: Tuple2[Asset,Int]
  val project: Project
  val testCase: TestCase
}
