package vp.bench.bg.operations

import se.gu.vp.model.Asset
import se.gu.vp.operations.{ChangeAsset, CreateMetadata, Operation, RunImmediately}
import se.gu.vp.operations.Utilities.addToOptionalSeq
import se.gu.vp.operations.metadata.MetadataConverter
import vp.bench.bg.metadata.ReplaceLineInAssetMetadata

import scala.collection.mutable

class ReplaceLineInAsset(val assetToChange:Asset, val lineNumber: Int, val newLine: String) extends ChangeAsset {

  override def perform(): Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    val content = assetToChange.content.get
    val indexedContent = for {
      index <- 0 until content.length
      line = content(index)
    } yield {
      (index,line)
    }

    val newPreContent = indexedContent
      .filter(elem => elem._1 < lineNumber)
      .sortBy(tuple => tuple._1)
      .toList
      .map(elem => elem._2)
    val newPostContent = indexedContent
      .filter(elem => elem._1 > lineNumber)
      .sortBy(tuple => tuple._1)
      .toList
      .map(elem => elem._2)

    val newContent = newPreContent ++ (newLine :: newPostContent)

    operationListBuilder += ChangeAssetContent(assetToChange, newContent, createMetadata = false)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val createdMetadata = ReplaceLineInAssetMetadata(assetToChange = MetadataConverter.convertAssetToMetadata(assetToChange), lineNumber = lineNumber, newLine = newLine)
      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object ReplaceLineInAsset {
  def apply(assetToChange: Asset, lineNumber: Int, newLine: String, runImmediately: Boolean = true, createMetadata: Boolean = true): ReplaceLineInAsset = {
    if (runImmediately && createMetadata) {
      new ReplaceLineInAsset(assetToChange, lineNumber, newLine) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new ReplaceLineInAsset(assetToChange, lineNumber, newLine) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new ReplaceLineInAsset(assetToChange, lineNumber, newLine) with CreateMetadata
    } else {
      new ReplaceLineInAsset(assetToChange, lineNumber, newLine)
    }
  }
}