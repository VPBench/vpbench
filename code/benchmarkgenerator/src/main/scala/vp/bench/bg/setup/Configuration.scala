package vp.bench.bg.setup

import vp.bench.bg.model.{JavaProject, Project}

case class Configuration(targetPath: String, workingDirectory: String, initialProjects: List[ProjectConfiguration], generatorConfig: GeneratorConfig, resrecConf: ResultRecordingConfiguration)
case class ProjectConfiguration(projectPath: String, insertionPointFilters: List[String])
case class ResultRecordingConfiguration(clocPath: String, sqlitePath: String)

// not clean -> remake this into more concrete generator configuration
case class GeneratorConfig(discardUselessChangesProbAL: Double, discardUselessChangesProbDL: Double, discardUselessChangesProbRL: Double, cloneFromOtherRepos: Boolean, unitTestScribePathString: String, initialisationPathString: String, srcmlPathString: String, jdepsPathString: String, maxMappedAssetCount: Option[Int] = None)