package vp.bench.bg.setup

import java.nio.file.{Files, Path}

import net.liftweb.json.Serialization.{read, write}
import net.liftweb.json.{FieldSerializer, NoTypeHints, Serialization, ShortTypeHints}
import vp.bench.bg.TestConfigurationProvider.createStructurizrProj
import vp.bench.bg.model.{JavaProject, Project}

object ConfigurationReader {
  implicit val formats = Serialization.formats(NoTypeHints)

  def readConfiguration(path: Path): Configuration = {
    val configFileContent = Files.readString(path)
    val config = read[Configuration](configFileContent)
    config
  }
}
