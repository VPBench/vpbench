package vp.bench.bg.transaction

import se.gu.vp.operations.Operation
import vp.bench.bg.transaction.exceptions.ASTPotentiallyCorruptException

case class SimpleTransaction(override val operations: List[Operation]) extends Transaction(operations: List[Operation]) {
  override def execute: Unit = {
    try {
      operations.foreach(op => op.perform)
    } catch {
      case _ : Exception =>
        throw ASTPotentiallyCorruptException("Transaction crashed during execution. System is likely inconsistent.")
    }
  }
}
