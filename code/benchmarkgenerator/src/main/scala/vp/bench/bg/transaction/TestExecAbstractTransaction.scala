package vp.bench.bg.transaction

import java.nio.file.Path
import java.util

import com.rits.cloning.Cloner
import se.gu.vp.model.AssetType
import se.gu.vp.operations.Operation

abstract class TestExecAbstractTransaction(operations: List[Operation]) extends Transaction(operations) {
  def safeCopyMap : Tuple2[List[Operation], util.IdentityHashMap[Object,Object]] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.dontCloneInstanceOf(classOf[Path])
    cloner.registerImmutable(None.getClass)
    val cloneResult = cloner.deepClone(operations)

    (cloneResult.getClone, cloneResult.getMap)
  }
}
