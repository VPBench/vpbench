package vp.bench.bg.transaction

import java.nio.file
import java.nio.file.Path
import java.util

import com.rits.cloning.Cloner
import vp.bench.bg.Utilities.{cleanDirectory, findRepoOnUpPath, rebaseAssetPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.model.{Asset, AssetType, Feature, FeatureTrace, FeatureTraceDatabase, RepositoryType, Trace, TraceDatabase}
import se.gu.vp.operations.Utilities.{isFeatureBeneathRootAsset, transformASTToList}
import se.gu.vp.operations.{Operation, SerialiseAssetTree}
import vp.bench.bg.transaction.exceptions.TransactionFailureException
import vp.bench.bg.CompilationChecker
import vp.bench.bg.transaction.exceptions.{ASTPotentiallyCorruptException, TransactionFailureException}

case class TestExecCompilationTransaction(rootAsset: Asset, override val operations: List[Operation], targetPath: Path, compilationChecker: CompilationChecker) extends TestExecAbstractTransaction(operations: List[Operation]) {
  def serializeAndCompile(ast: Asset) = {
    cleanDirectory(targetPath, false)
    SerialiseAssetTree(ast, targetPath.toString)

    // TODO: Just assumes the repos names are in targetPath, i.e. where the changes were serialized -> ideally add some path rebasing here or something similar
    val compilationResults = transformASTToList(ast)
      .filter(a => a.assetType == RepositoryType)
      .map(repo => file.Paths.get(repo.name))
      .map(repo => compilationChecker.checkCompile(repo.toString))

    if(compilationResults.contains(false)) {
      throw new Exception("Compilation failed.")
    }
  }

  def safeCopy : Tuple2[Tuple2[Asset, List[Operation]], util.IdentityHashMap[Object,Object]] = {
    val cloner = new Cloner
    cloner.dontCloneInstanceOf(classOf[AssetType])
    cloner.dontCloneInstanceOf(classOf[Path])
    cloner.registerImmutable(None.getClass)
    cloner.registerImmutable(Nil.getClass)
    val cloneResult = cloner.deepClone(Tuple2(rootAsset, operations))

    (cloneResult.getClone, cloneResult.getMap)
  }

  override def execute = {
    val preTATraceDB_shallow = TraceDatabase.traces.map(trace => trace.doClone)
    val preTAFeatTraceDB_shallow = FeatureTraceDatabase.traces.map(trace => trace.doClone)

    val ((astCopy, operationsCopy), cloneMap) = safeCopy

    // How to handle if there exists a trace of an element, that is no longer part of the asset tree?
    // Can this be skipped here, i.e. the trace removed for testing?
    // This should be fine as long as we only require traces relating to both elements being part of the MAT
    // Intuitively this should be fine?
    // => Apart from CloneFeature existing traces are only required by AddFeature (when cloning Assets existing in other repos).
    //    This operation only looks for FeatureTraces and these are, by definition of the way we add features in our benchmark generator,
    //    necessarily added during its execution using features and feature models that are mapped to assets in the MAT.

    TraceDatabase.traces = TraceDatabase.traces.filter(trace => trace.source.getRoot.isDefined && trace.target.getRoot.isDefined)
      .map(trace => Trace(
        source = cloneMap.get(trace.source).asInstanceOf[Asset],
        target = cloneMap.get(trace.target).asInstanceOf[Asset],
        relationship = trace.relationship,
        versionAt = trace.versionAt))

    FeatureTraceDatabase.traces = FeatureTraceDatabase.traces.filter(trace => isFeatureBeneathRootAsset(trace.source) && isFeatureBeneathRootAsset(trace.target))
      .map(trace => FeatureTrace(
        source = cloneMap.get(trace.source).asInstanceOf[Feature],
        target = cloneMap.get(trace.target).asInstanceOf[Feature],
        relationship = trace.relationship,
        versionAt = trace.versionAt))

    try {
      operationsCopy.foreach(op => op.perform)
      TraceDatabase.traces = preTATraceDB_shallow
      FeatureTraceDatabase.traces = preTAFeatTraceDB_shallow
    } catch {
      case e : Exception =>
        println("Failure during testexec.")
        TraceDatabase.traces = preTATraceDB_shallow
        FeatureTraceDatabase.traces = preTAFeatTraceDB_shallow
        throw TransactionFailureException(e.getMessage)
    }

    try {
      serializeAndCompile(astCopy)
    } catch {
      case e : Exception =>
        println("Failure during serialization and compilation")
        throw TransactionFailureException("Serialization or compilation failed.")
    }

    try {
      operations.foreach(op => op.perform)
      println("Transaction completed successfully on real system.")
    } catch {
      case _ : Exception =>
        println("Critical error!")
        throw ASTPotentiallyCorruptException("Transaction crashed after testrun. System is likely inconsistent.")
    }
  }
}
