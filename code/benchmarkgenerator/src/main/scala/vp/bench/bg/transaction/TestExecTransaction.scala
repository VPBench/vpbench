package vp.bench.bg.transaction

import se.gu.vp.operations.Operation
import vp.bench.bg.transaction.exceptions.TransactionFailureException
import vp.bench.bg.transaction.exceptions.{ASTPotentiallyCorruptException, TransactionFailureException}

case class TestExecTransaction(override val operations: List[Operation]) extends TestExecAbstractTransaction(operations: List[Operation]) {
  override def execute: Unit = {
    val (operationsCopy, cloneMap) = safeCopyMap

    try {
      operationsCopy.foreach(op => op.perform)
    } catch {
      case _ : Exception =>
        throw TransactionFailureException("Transaction failed in testrun.")
    }

    try {
      operations.foreach(op => op.perform)
    } catch {
      case _ : Exception =>
        throw ASTPotentiallyCorruptException("Transaction crashed after testrun. System is likely inconsistent.")
    }
  }
}
