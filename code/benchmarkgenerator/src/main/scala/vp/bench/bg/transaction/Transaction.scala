package vp.bench.bg.transaction

import java.nio.file.Path
import java.util

import com.rits.cloning.Cloner
import se.gu.vp.model.{Asset, AssetType}
import se.gu.vp.operations.Operation
import se.gu.vp.operations.Utilities.transformASTToList
import vp.operations.AddExternalAsset

// TODO: In long run, perhaps remove val again, since the metadata is supposed to be accessed then -> This is for faster progress now
abstract class Transaction (val operations: List[Operation]) {
  // This system creates exponential runtime based on depth, might need a system to make sure, if transaction needs to be run in test- or productionmode?
  // Could in theory use a type of switch in each operation to toggle, which kind of transaction needs to be created -> would probably be need to be persisted in operation though
  def execute : Unit
}
