package vp.bench.bg.transaction.exceptions

case class ASTPotentiallyCorruptException(message: String) extends Exception
