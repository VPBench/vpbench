package vp.bench.bg.transaction.exceptions

case class TransactionFailureException(message: String) extends Exception
