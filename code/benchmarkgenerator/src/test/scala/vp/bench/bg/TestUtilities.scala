package vp.bench.bg

import java.nio.file
import java.nio.file.Paths

import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Utilities.getassetbyname
import vp.bench.bg.RunEvaluation.postprocessJavaParsing
import vp.bench.bg.TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.Utilities.{clearAndCopyInto, findRepoOnUpPath}
import vp.bench.bg.setup.ConfigurationReader

object TestUtilities {
  // Only uses first provided project to get readInPath and filterInsertionPoints
  def setupTest() = {
    val configFile = file.Paths.get("configTest.json")
    val config = ConfigurationReader.readConfiguration(configFile)

    val initialProjects = config.initialProjects.map(proj => (proj.projectPath, proj.insertionPointFilters.map(filter => Paths.get(config.workingDirectory).resolve(filter).toString)))
    val readInPath = initialProjects(0)._1
    val filterInsertionPoints = initialProjects(0)._2

    val workingDirectory = config.workingDirectory

    val compilationChecker = GradleCompilationChecker()

    clearAndCopyInto(workingDirectory, readInPath)
    val relPathToRoot = readInPath.split("\\\\").last
    val ast = readInJavaAssetTree(workingDirectory + "\\" + relPathToRoot).get

    val relFilterInsertionPaths = for {
      filterInsPoint <- filterInsertionPoints
    } yield {
      val insPointAsset = getassetbyname(filterInsPoint, ast).get
      val insPointPath  = file.Paths.get(filterInsPoint)
      val insPointRepoPath  = file.Paths.get(findRepoOnUpPath(insPointAsset).get.name)
      val relRepoPath = insPointRepoPath.relativize(insPointPath)
      relRepoPath
    }

    val postProcessingPathAssets = filterInsertionPoints.map(name => getassetbyname(name, ast).get)
    postprocessJavaParsing(ast, postProcessingPathAssets)

    // Initialise Transplantation Projects
    val structurizrProject = createStructurizrProj
    val disruptorProject = createDisruptorProj
    val rootProjects = List(structurizrProject, disruptorProject)

    (configFile, config, readInPath, filterInsertionPoints, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, postProcessingPathAssets, rootProjects)
  }
}
