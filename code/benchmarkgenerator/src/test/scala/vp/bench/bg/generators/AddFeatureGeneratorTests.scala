package vp.bench.bg.generators

import java.nio.file
import java.nio.file.Paths

import org.scalatest.FunSuite
import vp.bench.bg.RunEvaluation.postprocessJavaParsing
import vp.bench.bg.TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.Utilities.{clearAndCopyInto, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import se.gu.vp.model.{FeatureTraceDatabase, FileType, TraceDatabase}
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import vp.bench.bg.{GradleCompilationChecker, TestUtilities}
import vp.bench.bg.model.TestCase
import vp.bench.bg.operations.CloneRepositoryWProjectInitialization
import vp.bench.bg.setup.ConfigurationReader
import vp.bench.bg.transaction.TestExecCompilationTransaction

class AddFeatureGeneratorTests extends FunSuite {
  test ("Transaction execution works for AddFeature after a certain amount of repositories was introduced") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    // Iteration 2
    val testcase2 = TestCase("com.structurizr.view", "ColorTests", "test_isHexColorCode_ReturnsFalse_WhenPassedAnInvalidString")
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase2, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    // Iteration 3
    val testcase3 = TestCase("com.structurizr.view", "DefaultLayoutMergeStrategyTests", "test_copyLayoutInformation_WhenCanonicalNamesHaveNotChanged")
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute

    // Iteration 4
    val op4 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_1", rootProjects, runImmediately = false)
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute

    val repo1 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_1", ast).get

    // Iteration 5
    val op5 = CloneRepositoryWProjectInitialization(repo1, "CalculatorRepo_1_2", rootProjects, runImmediately = false)
    val ta5 = TestExecCompilationTransaction(ast, List(op5), Paths.get(workingDirectory), compilationChecker)
    ta5.execute

    // Iteration 6
    val testcase6 = TestCase("com.structurizr.view", "ViewSetTests", "test_createContainerView_ThrowsAnException_WhenAnEmptyKeyIsSpecified")
    val op6 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase6, parentFeature).get
    val ta6 = TestExecCompilationTransaction(ast, List(op6), Paths.get(workingDirectory), compilationChecker)
    ta6.execute

    val repo1_2 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_1_2", ast).get
    val fileAsset1_2 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_1_2\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint1_2 = (fileAsset1_2.children(4).children(0), 0)

    // Iteration 7
    val testcase7 = TestCase("com.structurizr.view", "ViewSetTests", "test_createContainerView_ThrowsAnException_WhenAnEmptyKeyIsSpecified")
    val op7 = addFeatureGenerator.extractTestcase(insertionPoint1_2, subprojectStructCore, testcase7, parentFeature).get
    val ta7 = TestExecCompilationTransaction(ast, List(op7), Paths.get(workingDirectory), compilationChecker)
    ta7.execute

    // Iteration 8
    val op8 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_3", rootProjects, runImmediately = false)
    val ta8 = TestExecCompilationTransaction(ast, List(op8), Paths.get(workingDirectory), compilationChecker)
    ta8.execute

    val repo3 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_3", ast).get

    // Iteration 9
    val op9 = CloneRepositoryWProjectInitialization(repo3, "CalculatorRepo_3_4", rootProjects, runImmediately = false)
    val ta9 = TestExecCompilationTransaction(ast, List(op9), Paths.get(workingDirectory), compilationChecker)
    ta9.execute

    // Iteration 10
    val testcase10 = TestCase("com.structurizr.view", "ViewSetTests", "test_createCustomView_ThrowsAnException_WhenANullKeyIsSpecified")
    val op10 = addFeatureGenerator.extractTestcase(insertionPoint1_2, subprojectStructCore, testcase10, parentFeature).get
    val ta10 = TestExecCompilationTransaction(ast, List(op10), Paths.get(workingDirectory), compilationChecker)
    ta10.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get
    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)

    // Iteration 10.2
    val testcase10_2 = TestCase("com.structurizr.util", "StringUtilsTests", "test_isNullOrEmpty_ReturnsTrue_WhenPassedNull")
    val op10_2 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase10_2, parentFeature).get
    val ta10_2 = TestExecCompilationTransaction(ast, List(op10_2), Paths.get(workingDirectory), compilationChecker)
    ta10_2.execute
  }

  test("Short test -> did not cause the problem") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    // TODO: GetAssetByName also uses hardcoded paths
    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)

    // Iteration 10.2
    val testcase10_2 = TestCase("com.structurizr.util", "StringUtilsTests", "test_isNullOrEmpty_ReturnsTrue_WhenPassedNull")
    val op10_2 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase10_2, parentFeature).get
    val ta10_2 = TestExecCompilationTransaction(ast, List(op10_2), Paths.get(workingDirectory), compilationChecker)
    ta10_2.execute
  }

  test("CloneAsset is successfully incorporated into AddFeature") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    TraceDatabase.traces = Nil

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructClient, testcase2, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    val repoStructAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr", ast).get
    val repoStructAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr", ast).get

    assert(TraceDatabase.traces.length > 83)
    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType && trace.target.assetType == FileType).length == 83)
    assert(TraceDatabase.traces.forall(trace => trace.source.getRepository.get == repo && trace.target.getRepository.get == repo0))
    // 83 Code files + 2 build.gradle or 87 Code files + 3 build.gradle
    assert(transformASTToList(repoStructAsset).filter(asset => asset.assetType == FileType).length == 85)
    assert(transformASTToList(repoStructAsset0).filter(asset => asset.assetType == FileType).length == 90)
  }

  test("CloneAsset is used everywhere on identical import") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    TraceDatabase.traces = Nil

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase1, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    val repoStructAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr", ast).get
    val repoStructAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr", ast).get

    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType && trace.target.assetType == FileType).length == 83)
    assert(TraceDatabase.traces.forall(trace => trace.source.getRepository.get == repo && trace.target.getRepository.get == repo0))
    // 83 Code files + 2 build.gradle
    assert(transformASTToList(repoStructAsset).filter(asset => asset.assetType == FileType).length == 85)
    assert(transformASTToList(repoStructAsset0).filter(asset => asset.assetType == FileType).length == 85)
  }

  test("ClonedAssetMap is accurate") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    FeatureTraceDatabase.traces.foreach(trace => println(trace.source))
    TraceDatabase.traces = Nil

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 3
    val op3 = CloneRepositoryWProjectInitialization(repo0, "CalculatorRepo_0_1", rootProjects, runImmediately = false)
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute

    val repo01 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0_1", ast).get

    TraceDatabase.traces = Nil

    // Iteration 4
    val op4 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")
    executeGradle(repo01.name, "compileJava")

    val repoStructAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr", ast).get
    val repoStructAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr", ast).get
    val repoStructAsset01 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0_1\\structurizr", ast).get

    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType && trace.target.assetType == FileType).length == 83)
    // 83 Code files + 2 build.gradle
    assert(transformASTToList(repoStructAsset).filter(asset => asset.assetType == FileType).length == 85)
    assert(transformASTToList(repoStructAsset0).filter(asset => asset.assetType == FileType).length == 85)
    assert(transformASTToList(repoStructAsset01).filter(asset => asset.assetType == FileType).length == 85)

    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType).forall(trace => trace.source == op4.clonedAssetMap(Paths.get(trace.target.name))))
    assert(op4.clonedAssetMap.size == 83)
  }

  test("CloneAsset is used nowhere when cloneFromOtherRepos == false") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = false, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    TraceDatabase.traces = Nil

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase1, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    val repoStructAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr", ast).get
    val repoStructAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr", ast).get

    assert(TraceDatabase.traces.length == 0)
    assert(op3.clonedAssetMap.isEmpty)
    // 83 Code files + 2 build.gradle
    assert(transformASTToList(repoStructAsset).filter(asset => asset.assetType == FileType).length == 85)
    assert(transformASTToList(repoStructAsset0).filter(asset => asset.assetType == FileType).length == 85)
  }
}
