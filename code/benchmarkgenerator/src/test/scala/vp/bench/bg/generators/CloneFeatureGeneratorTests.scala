package vp.bench.bg.generators

import java.nio.file
import java.nio.file.Paths

import org.scalatest.FunSuite
import vp.bench.bg.RunEvaluation.postprocessJavaParsing
import vp.bench.bg.TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.Utilities.{cleanDirectory, clearAndCopyInto, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.operations.CloneFeatureWProjectInitialization
import se.gu.vp.model.{Feature, FileType, FolderType, RepositoryType, VPRootType}
import se.gu.vp.operations.ASTSliceHelpers.tryGetAssetClone
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.{MapAssetToFeature, SerialiseAssetTree}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, getFeatureByNameFromFeature, getassetbyname, transformASTToList}
import vp.bench.bg.{GradleCompilationChecker, TestUtilities}
import vp.bench.bg.model.TestCase
import vp.bench.bg.operations.{CloneFeatureWProjectInitialization, CloneRepositoryWProjectInitialization}
import vp.bench.bg.transaction.TestExecCompilationTransaction

class CloneFeatureGeneratorTests extends FunSuite {
  test("CloneFeature works in a simple scenario") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")

    val repo = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(!structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    val op3 = someOp3.get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)
  }

  test("CloneFeatureGenerator does not remove valid candidates when limiting maxMappedAssets") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory, maxMappedAssetCount = Some(10))

    val asset = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")

    val repo = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(!structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    assert(someOp3.isDefined)
  }

  test("CloneFeature works in simple scenario for large feature") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(!structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    val op3 = someOp3.get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)

    val timestampBegin = System.nanoTime
    ta3.execute
    val timestampEnd = System.nanoTime
    val duration = timestampEnd - timestampBegin

    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    println(s"Time: $duration")
  }

  test("CloneFeatureGenerator can limit candidates for copying if specified") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory, maxMappedAssetCount = Some(20))

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(!structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    assert(someOp3.isEmpty)
  }

  test("CloneFeature works with initialising non-source project-dependencies") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructClient, testcase4, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(!structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(!subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    val op3 = someOp3.get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    // Iteration 4
    val op4 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 5
    // Source
    val srcfmDefAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get
    val srcFeature = getFeatureByNameFromFeature("com.structurizr.util.ImageUtilsTests.test_getContentType_ThrowsAnException_WhenANullFileIsSpecified", srcfmDefAsset.featureModel.get.rootfeature).get
    val srcfmAssetPath = computeAssetPath(srcfmDefAsset)
    val srcoriginalFeaturePath = computeFeaturePath(srcFeature)
    val srcfeaturePath = se.gu.vp.model.Path(ast, srcfmAssetPath ++ srcoriginalFeaturePath)

    // Target
    val tgtfmDefAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get
    val tgtFeature = getFeatureByNameFromFeature("Calculator", tgtfmDefAsset.featureModel.get.rootfeature).get
    val tgtfmAssetPath = computeAssetPath(tgtfmDefAsset)
    val tgtoriginalFeaturePath = computeFeaturePath(tgtFeature)
    val tgtfeaturePath = se.gu.vp.model.Path(ast, tgtfmAssetPath ++ tgtoriginalFeaturePath)

    val op5 = CloneFeatureWProjectInitialization(sourceFPath = srcfeaturePath, targetFPath = tgtfeaturePath, rootProjects = rootProjects, targetPathString = workingDirectory, runImmediately = false)
    val ta5 = TestExecCompilationTransaction(ast, List(op5), Paths.get(workingDirectory), compilationChecker)
    ta5.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")
  }

  test("CloneFeature works over changed assets already contained in target") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 4
    val op4 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructClient, testcase2, parentFeature).get
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    // Iteration 4
    val op6 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta6 = TestExecCompilationTransaction(ast, List(op6), Paths.get(workingDirectory), compilationChecker)
    ta6.execute

    val editAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr\\structurizr-core\\src\\com\\structurizr\\Workspace.java", ast).get
    editAsset.content = Some("" :: editAsset.content.get)

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    // Iteration 3
    val someOp3 = clnFeatGenerator.generate
    val op3 = someOp3.get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    // Iteration 3
    val someOp7 = clnFeatGenerator.generate
    val op7 = someOp7.get
    val ta7 = TestExecCompilationTransaction(ast, List(op7), Paths.get(workingDirectory), compilationChecker)
    ta7.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    assert(structurizrProject.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructCore.getAbsolutePathTarget(repo0).isDefined)
    assert(subprojectStructClient.getAbsolutePathTarget(repo0).isDefined)

    assert(transformASTToList(ast).filter(asset => asset.name == config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr\\structurizr-core\\src\\com\\structurizr\\Workspace.java").length == 1)
  }

  test("alreadyCloned") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val editAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr\\structurizr-core\\src\\com\\structurizr\\Workspace.java", ast).get
    val editAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr\\structurizr-core\\src\\com\\structurizr\\Workspace.java", ast).get

    assert(tryGetAssetClone(editAsset, editAsset0).isDefined)
    assert(tryGetAssetClone(editAsset0, editAsset).isEmpty)

    editAsset.content = Some("otherContent0" :: editAsset.content.get)
    editAsset0.content = Some("otherContent1" :: "otherContent2" :: editAsset0.content.get)

    assert(tryGetAssetClone(editAsset, editAsset0).isDefined)
    assert(tryGetAssetClone(editAsset0, editAsset).isEmpty)
  }

  test("CloneFeature works with other asset clones") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    // Set cloneFromOtherRepos to false to allow only generation of a specific operation
    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = false, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")
    val testcase5 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAFile")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get
    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val featureModel0 = repo0.featureModel.get
    val parentFeature0 = featureModel0.rootfeature.UNASSIGNED

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructCore, testcase5, parentFeature0).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase3, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    val editAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr\\structurizr-core\\src\\com\\structurizr\\util\\ImageUtils.java", ast).get
    val newFeature = Feature("AddedFeatureInRepo")
    MapAssetToFeature(editAsset, newFeature)

    val editAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr\\structurizr-core\\src\\com\\structurizr\\util\\ImageUtils.java", ast).get
    val newFeature0 = Feature("AddedFeatureInRepo0")
    MapAssetToFeature(editAsset0, newFeature0)

    // Iteration 4
    val op4 = clnFeatGenerator.generate.get
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)

    val duplicateAssets = transformASTToList(ast).filter(asset => List(VPRootType, RepositoryType, FolderType, FileType).contains(asset.assetType)).groupBy(as => as.name).filter(pair => pair._2.length > 1)

    assert(duplicateAssets.size == 0)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))
  }

  // Expected to fail
  test("CloneFeature does not work over back traces") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get
    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(3).children(0),0)

    // Iteration 4
    val op4 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructClient, testcase2, parentFeature).get
    val ta4 = TestExecCompilationTransaction(ast, List(op4), Paths.get(workingDirectory), compilationChecker)
    ta4.execute

    cleanDirectory(file.Paths.get(workingDirectory), false)
    SerialiseAssetTree(ast, workingDirectory)
    assert(executeGradle(repo.name, "compileJava"))
    assert(executeGradle(repo0.name, "compileJava"))

    insertionPoint._1.content = Some("" :: insertionPoint._1.content.get)

    // Source
    val srcfmDefAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get
    val srcFeature = getFeatureByNameFromFeature("com.structurizr.io.json.JsonWriterTests.test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified", srcfmDefAsset.featureModel.get.rootfeature).get
    val srcfmAssetPath = computeAssetPath(srcfmDefAsset)
    val srcoriginalFeaturePath = computeFeaturePath(srcFeature)
    val srcfeaturePath = se.gu.vp.model.Path(ast, srcfmAssetPath ++ srcoriginalFeaturePath)

    // Target
    val tgtfmDefAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get
    val tgtFeature = getFeatureByNameFromFeature("Calculator", tgtfmDefAsset.featureModel.get.rootfeature).get
    val tgtfmAssetPath = computeAssetPath(tgtfmDefAsset)
    val tgtoriginalFeaturePath = computeFeaturePath(tgtFeature)
    val tgtfeaturePath = se.gu.vp.model.Path(ast, tgtfmAssetPath ++ tgtoriginalFeaturePath)

    val op5 = CloneFeatureWProjectInitialization(sourceFPath = srcfeaturePath, targetFPath = tgtfeaturePath, rootProjects = rootProjects, targetPathString = workingDirectory, runImmediately = false)
    val ta5 = TestExecCompilationTransaction(ast, List(op5), Paths.get(workingDirectory), compilationChecker)
    ta5.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")
  }

  test("CloneFeature does not work over direct clone traces") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)
    val clnFeatGenerator = CloneFeatureGenerator(ast, rootProjects = rootProjects, targetPathString = workingDirectory)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\vp\\bench\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")
    val testcase3 = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getContentType_ThrowsAnException_WhenANullFileIsSpecified")
    val testcase4 = TestCase("com.structurizr.encryption", "AesEncryptionStrategyTests", "test_decrypt_decryptsTheCiphertext_WhenTheSameConfigurationIsUsed")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    //       - Repo_0
    //  Repo         attempt to clone from Repo1 to Repo0
    //       - Repo_1

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute

    // Iteration 2
    val op2 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_1", rootProjects, runImmediately = false)
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute

    val repo1 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_1", ast).get

    val assetToEdit  = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr\\structurizr-core\\src\\com\\structurizr\\AbstractWorkspace.java", ast).get
    val assetToEdit1 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_1\\structurizr\\structurizr-core\\src\\com\\structurizr\\AbstractWorkspace.java", ast).get

    assetToEdit.content  = Some("" :: assetToEdit.content.get)
    assetToEdit1.content = Some("" :: "" :: "" :: assetToEdit1.content.get)

    // Iteration 5
    // Source
    val srcFeature = getFeatureByNameFromFeature("com.structurizr.view.ViewSetTests.test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified", repo1.featureModel.get.rootfeature).get
    val srcfmAssetPath = computeAssetPath(repo1)
    val srcoriginalFeaturePath = computeFeaturePath(srcFeature)
    val srcfeaturePath = se.gu.vp.model.Path(ast, srcfmAssetPath ++ srcoriginalFeaturePath)

    // Target
    val tgtFeature = getFeatureByNameFromFeature("Calculator", repo0.featureModel.get.rootfeature).get
    val tgtfmAssetPath = computeAssetPath(repo0)
    val tgtoriginalFeaturePath = computeFeaturePath(tgtFeature)
    val tgtfeaturePath = se.gu.vp.model.Path(ast, tgtfmAssetPath ++ tgtoriginalFeaturePath)

    val op5 = CloneFeatureWProjectInitialization(sourceFPath = srcfeaturePath, targetFPath = tgtfeaturePath, rootProjects = rootProjects, targetPathString = workingDirectory, runImmediately = false)
    val ta5 = TestExecCompilationTransaction(ast, List(op5), Paths.get(workingDirectory), compilationChecker)
    ta5.execute
  }
}
