package vp.bench.bg.generators

import java.nio.file
import java.nio.file.Paths

import org.scalatest.FunSuite
import vp.bench.bg.RunEvaluation.postprocessJavaParsing
import vp.bench.bg.TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.Utilities.{clearAndCopyInto, findRepoOnUpPath}
import vp.bench.bg.gradlehandler.GradleExecutor.executeGradle
import vp.bench.bg.operations.CloneRepositoryWProjectInitialization
import se.gu.vp.model.{FileType, TraceDatabase}
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Utilities.{getassetbyname, transformASTToList}
import vp.bench.bg.{GradleCompilationChecker, TestUtilities}
import vp.bench.bg.model.TestCase
import vp.bench.bg.operations.{ChangeAssetContent, CloneRepositoryWProjectInitialization}
import vp.bench.bg.transaction.TestExecCompilationTransaction

class TestTraceDBResetting extends FunSuite {
  test("TraceDB ResettingWorks") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)
    val clnRepoGenerator = CloneRepositoryGenerator(ast, rootProjects)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subprojectStructCore = structurizrProject.getSubprojectByName("structurizr-core").get
    val subprojectStructClient = structurizrProject.getSubprojectByName("structurizr-client").get

    val testcase1 = TestCase("com.structurizr.view", "ViewSetTests", "test_createSystemLandscapeView_ThrowsAnException_WhenANullKeyIsSpecified")
    val testcase2 = TestCase("com.structurizr.io.json" ,"JsonWriterTests", "test_write_ThrowsAnIllegalArgumentException_WhenANullWorkspaceIsSpecified")

    val repo = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get

    // Iteration 1
    val op1 = CloneRepositoryWProjectInitialization(repo, "CalculatorRepo_0", rootProjects, runImmediately = false)
    val ta1 = TestExecCompilationTransaction(ast, List(op1), Paths.get(workingDirectory), compilationChecker)
    ta1.execute

    TraceDatabase.traces = Nil

    val fileAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast).get
    val insertionPoint0 = (fileAsset0.children(2).children(0),0)
    val repo0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0", ast).get

    // Iteration 2
    val op2 = addFeatureGenerator.extractTestcase(insertionPoint, subprojectStructCore, testcase1, parentFeature).get
    val ta2 = TestExecCompilationTransaction(ast, List(op2), Paths.get(workingDirectory), compilationChecker)
    ta2.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    // Iteration 3
    val op3 = addFeatureGenerator.extractTestcase(insertionPoint0, subprojectStructClient, testcase2, parentFeature).get
    val ta3 = TestExecCompilationTransaction(ast, List(op3), Paths.get(workingDirectory), compilationChecker)
    ta3.execute
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    val repoStructAsset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\structurizr", ast).get
    val repoStructAsset0 = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo_0\\structurizr", ast).get

    assert(TraceDatabase.traces.length > 83)
    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType && trace.target.assetType == FileType).length == 83)
    assert(TraceDatabase.traces.forall(trace => trace.source.getRepository.get == repo && trace.target.getRepository.get == repo0))

    val op4 = ChangeAssetContent(assetToChange = fileAsset0, newContent = List("This will surely work. {}"), runImmediately = false)
    val op5 = CloneRepositoryWProjectInitialization(repo0, "CalculatorRepo_0_1", rootProjects, runImmediately = false)
    val ta45 = TestExecCompilationTransaction(ast, List(op4, op5), Paths.get(workingDirectory), compilationChecker)
    try {
      ta45.execute
    } catch {
      case e: Exception =>
    }
    executeGradle(repo.name, "compileJava")
    executeGradle(repo0.name, "compileJava")

    assert(TraceDatabase.traces.length > 83)
    assert(TraceDatabase.traces.filter(trace => trace.source.assetType == FileType && trace.target.assetType == FileType).length == 83)
    assert(TraceDatabase.traces.forall(trace => trace.source.getRepository.get == repo && trace.target.getRepository.get == repo0))
  }
}
