package vp.bench.bg.metadata

import java.nio.file

import org.scalatest.FunSuite
import vp.bench.bg.RunEvaluation.postprocessJavaParsing
import vp.bench.bg.TestConfigurationProvider.{createDisruptorProj, createStructurizrProj}
import vp.bench.bg.Utilities.{clearAndCopyInto, findRepoOnUpPath}
import se.gu.vp.model.{Asset, BlockType, FeatureModel, FileType, FolderType, MethodType, RepositoryType, RootFeature, VPRootType}
import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.Utilities.getassetbyname
import se.gu.vp.operations.metadata.{AddAssetMetadata, AddExternalAssetMetadata, ExternalAssetMetadata, ExternalFeatureMetadata, InAssetFeatureMetadata, InternalAssetMetadata, MapAssetToFeatureMetadata}
import se.gu.vp.operations.{AddAsset, AddFeatureModelToAsset}
import vp.bench.bg.TestUtilities
import vp.bench.bg.generators.AddFeatureGenerator
import vp.bench.bg.model.TestCase

class OperationMetadataConversionTest extends FunSuite {
  test("AddExternalFeature Metadata Conversion works") {
    val (_, config, _, _, workingDirectory, compilationChecker, ast, relFilterInsertionPaths, _, rootProjects) = TestUtilities.setupTest
    val structurizrProject = rootProjects.filter(p => p.name == "structurizr")(0)

    val addFeatureGenerator = AddFeatureGenerator(ast, rootProjects, targetPathString = workingDirectory, unitTestScribePathString = config.generatorConfig.unitTestScribePathString, initialisationPathString = config.generatorConfig.initialisationPathString, srcmlPathString = config.generatorConfig.srcmlPathString, filterInsertionPoints = Some(relFilterInsertionPaths), cloneFromOtherRepos = config.generatorConfig.cloneFromOtherRepos, jdepsPathString = config.generatorConfig.jdepsPathString)

    val asset = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java", ast)
    val insertionPoint = (asset.get.children(2).children(0),0)
    val featureModel = getassetbyname(config.workingDirectory + "\\CalculatorRoot\\CalculatorRepo", ast).get.featureModel.get
    val parentFeature = featureModel.rootfeature.UNASSIGNED

    val subproject = structurizrProject.getSubprojectByName("structurizr-core").get
    val testcase = TestCase("com.structurizr.util", "ImageUtilsTests", "test_getImageAsBase64_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAnImage")

    val op = addFeatureGenerator.extractTestcase(insertionPoint, subproject, testcase, parentFeature).get
    op.perform
    val sut = op.metadata.get

    assert(sut.getClass == classOf[AddExternalFeatureMetadata])
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].insertionPoint._1.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].insertionPoint._1.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java")
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].insertionPoint._1.asInstanceOf[InternalAssetMetadata].indexPath.get == List(2,0))
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].insertionPoint._2 == 0)
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].donor == "structurizr-core")
    assert(sut.asInstanceOf[AddExternalFeatureMetadata].testCase.toString == "com.structurizr.util.ImageUtilsTests.test_getImageAsBase64_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAnImage")
    assert(sut.subOperations.get.length == 16)

    assert(sut.subOperations.get(0).getClass == classOf[AddAssetMetadata])
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].asset.getClass == classOf[ExternalAssetMetadata])
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].asset.asInstanceOf[ExternalAssetMetadata].name == "testcase")
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java")
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].indexPath.get == List(2,0))
    assert(sut.subOperations.get(0).asInstanceOf[AddAssetMetadata].insertionPoint.get == 0)

    assert(sut.subOperations.get(1).getClass == classOf[AddAssetMetadata])
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].asset.getClass == classOf[ExternalAssetMetadata])
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].asset.asInstanceOf[ExternalAssetMetadata].name == "testcaseImport")
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java")
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(sut.subOperations.get(1).asInstanceOf[AddAssetMetadata].insertionPoint.get == 1)

    assert(sut.subOperations.get(2).getClass == classOf[MapAssetToFeatureMetadata])
    assert(sut.subOperations.get(2).asInstanceOf[MapAssetToFeatureMetadata].asset.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(2).asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java")
    assert(sut.subOperations.get(2).asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].indexPath.get == List(3,0,0))
    assert(sut.subOperations.get(2).asInstanceOf[MapAssetToFeatureMetadata].feat.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.subOperations.get(2).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[ExternalFeatureMetadata].name == "test_getImageAsBase64_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAnImage")
    assert(sut.subOperations.get(2).subOperations.get.length == 1)

    assert(sut.subOperations.get(3).getClass == classOf[MapAssetToFeatureMetadata])
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].asset.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\AdvancedCalculator.java")
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].indexPath.get == List(1))
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].feat.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo")
    assert(sut.subOperations.get(3).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].lpq == "test_getImageAsBase64_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAnImage")
    assert(sut.subOperations.get(3).subOperations.get.length == 0)

    assert(sut.subOperations.get(4).getClass == classOf[AddExternalAssetMetadata])
    assert(sut.subOperations.get(4).asInstanceOf[AddExternalAssetMetadata].pathToAsset == "D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java")
    assert(sut.subOperations.get(4).asInstanceOf[AddExternalAssetMetadata].assetName.get == "structurizr")

    assert(sut.subOperations.get(5).getClass == classOf[AddModifiedExternalFileAssetMetadata])
    assert(sut.subOperations.get(5).asInstanceOf[AddModifiedExternalFileAssetMetadata].pathToAsset == "D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java\\build.gradle")

    assert(sut.subOperations.get(6).getClass == classOf[AddExternalAssetMetadata])
    assert(sut.subOperations.get(6).asInstanceOf[AddExternalAssetMetadata].pathToAsset == "D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java\\structurizr-core")

    assert(sut.subOperations.get(7).getClass == classOf[AddModifiedExternalFileAssetMetadata])
    assert(sut.subOperations.get(7).asInstanceOf[AddModifiedExternalFileAssetMetadata].pathToAsset == "D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java\\structurizr-core\\build.gradle")

    // ...

    assert(sut.subOperations.get(12).getClass == classOf[AddExternalAssetMetadata])
    assert(sut.subOperations.get(12).asInstanceOf[AddExternalAssetMetadata].pathToAsset == "D:\\Dokumente\\_Studium\\masterarbeit\\gitClones\\Structurizr\\java\\structurizr-core\\src\\com\\structurizr\\util\\ImageUtils.java")

    assert(sut.subOperations.get(13).getClass == classOf[MapAssetToFeatureMetadata])
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].asset.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\structurizr\\structurizr-core\\src\\com\\structurizr\\util\\ImageUtils.java")
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].feat.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo")
    assert(sut.subOperations.get(13).asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].lpq == "test_getImageAsBase64_ThrowsAnException_WhenAFileIsSpecifiedButItIsNotAnImage")

    assert(sut.subOperations.get(14).getClass == classOf[ChangeAssetContentMetadata])
    assert(sut.subOperations.get(14).asInstanceOf[ChangeAssetContentMetadata].assetToChange.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(14).asInstanceOf[ChangeAssetContentMetadata].assetToChange.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\build.gradle")
    assert(sut.subOperations.get(14).subOperations.get.length == 1)

    assert(sut.subOperations.get(15).getClass == classOf[ChangeAssetContentMetadata])
    assert(sut.subOperations.get(15).asInstanceOf[ChangeAssetContentMetadata].assetToChange.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(15).asInstanceOf[ChangeAssetContentMetadata].assetToChange.asInstanceOf[InternalAssetMetadata].path.get == "CalculatorRoot\\CalculatorRepo\\settings.gradle")
    assert(sut.subOperations.get(15).subOperations.get.length == 1)

    //Import
    //import com.structurizr.util.*;
    //import org.junit.Test;
    //
    //import java.io.File;
    //
    //import static org.junit.Assert.assertEquals;
    //import static org.junit.Assert.assertTrue;
    //import static org.junit.Assert.fail;

    //TC
    //try {
    //
    //        try {
    //            ImageUtils.getImageAsBase64(new File("../build.gradle"));
    //            fail();
    //        } catch (IllegalArgumentException iae) {
    //            iae.printStackTrace();
    //            assertTrue(iae.getMessage().endsWith("build.gradle is not a supported image file."));
    //        }
    //
    //} catch (Exception e) {}
  }
}
