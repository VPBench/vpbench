// Changed by authors of paper "Generating Evolving and Variant-Rich Software Systems"

package com.rits.cloning;

import java.util.IdentityHashMap;
import java.util.Map;

public class CloneIncludingMap<T> {
    private T clone;
    private IdentityHashMap<Object,Object> map;

    public T getClone() {
        return clone;
    }

    public IdentityHashMap<Object, Object> getMap() {
        return map;
    }

    public CloneIncludingMap(T clone, IdentityHashMap<Object,Object> map) {
        this.clone = clone;
        this.map = map;
    }
}
