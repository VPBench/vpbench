﻿using ABB.SrcML.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WM.UnitTestScribe.CallGraphNS
{
    public class EntireCGManager : AbstractCGManager
    {
        public override void Build(IEnumerable<MethodDefinition> methods)
        {
            foreach (MethodDefinition method in methods)
            {
                //Console.WriteLine("method  {0}", method.GetFullName());
                var mdCalls = from statments in method.GetDescendantsAndSelf()
                              from expression in statments.GetExpressions()
                              from call in expression.GetDescendantsAndSelf<MethodCall>()
                              select call;
                //Console.WriteLine("calls count : {0}" , mdCalls.Count());
                foreach (MethodCall call in mdCalls)
                {
                    MethodDefinition calleeM = FindMatchedMd(call);
                    if (calleeM == null) { continue; }
                    if (!CG.ContainsMethod(calleeM) || !CG.ContainsMethod(method))
                    {
                        Console.WriteLine("ERROR: Did not find the callee in the call graph");
                        throw new Exception("Did not find the callee");
                    }
                    CG.AddCalleeEdge(method, calleeM);
                    CG.AddCallerEdge(method, calleeM);
                }
            }
        }

        public override void StoreCallGraph(string path)
        {
            File.Create(path);
            using (StreamWriter fw = new StreamWriter(path))
            {
                //fw.Write(CG.CgPrint());
            }
        }
    }
}
