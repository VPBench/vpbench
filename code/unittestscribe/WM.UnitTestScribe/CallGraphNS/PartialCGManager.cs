﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using ABB.SrcML.Data;

namespace WM.UnitTestScribe.CallGraphNS {
    public class PartialCGManager : AbstractCGManager {

        // TODO: Needs to be reworked in case the partialcgmanager gets used when the hierarchical slicing idea should be incorporated (requires additional hierarchy information (level? or callee) and in which line it is called (can the callgraph be reworked to store this information as tuples perhaps?))
        private List<MethodDefinition> includedMethods;

        public PartialCGManager() {
            includedMethods = new List<MethodDefinition>();
        }

        /// <summary>
        /// Builds partial call graph, based on the set of passed methods (aka methods of interest).
        /// </summary>
        /// <param name="methods"></param>
        public override void Build(IEnumerable<MethodDefinition> methods)
        {
            IList<MethodDefinition> encounteredMethods = new List<MethodDefinition>();
            IList<MethodDefinition> queue = methods.ToList();

            while (queue.Count > 0) {
                var head = queue.First();
                if (!encounteredMethods.Contains(head)) {
                    var methodCalls = handleMethod(head);
                    
                    foreach (MethodDefinition method in methodCalls) {
                        queue.Add(method);
                    }
                    
                    encounteredMethods.Add(head);
                }
                queue.RemoveAt(0);
            }
        }

        public override void StoreCallGraph(string path)
        {
            using (StreamWriter fw = new StreamWriter(path)) {
                foreach (MethodDefinition method in includedMethods) {
                    fw.WriteLine(method.GetFullName());
                }
            }
        }

        /// <summary>
        /// Get MethodDefinitions of methods that are called inside method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        private IList<MethodDefinition> handleMethod(MethodDefinition method) {
            // TODO: probably temp (see comment for the attribute definition)
            includedMethods.Add(method);

            //Console.WriteLine("method  {0}", method.GetFullName());
            var mdCalls = from statments in method.GetDescendantsAndSelf()
                          from expression in statments.GetExpressions()
                          from call in expression.GetDescendantsAndSelf<MethodCall>()
                          select call;
            //Console.WriteLine("calls count : {0}" , mdCalls.Count());

            var calleeMs = mdCalls.Select(call => FindMatchedMd(call))
                .Where(calleeM => calleeM != null)
                .Select(calleeM => {
                    if (!CG.ContainsMethod(calleeM) || !CG.ContainsMethod(method))
                    {
                        Console.WriteLine("ERROR: Did not find the callee in the call graph");
                        throw new Exception("Did not find the callee");
                    }
                    CG.AddCalleeEdge(method, calleeM);
                    CG.AddCallerEdge(method, calleeM);

                    return calleeM;
                });

            //    (call => {
            //    MethodDefinition calleeM = FindMatchedMd(call);
            //    if (calleeM == null) { continue; }
            //    if (!CG.ContainsMethod(calleeM) || !CG.ContainsMethod(method))
            //    {
            //        Console.WriteLine("ERROR: Did not find the callee in the call graph");
            //        throw new Exception("Did not find the callee");
            //    }
            //    CG.AddCalleeEdge(method, calleeM);
            //    CG.AddCallerEdge(method, calleeM);
            //})
            //foreach (MethodCall call in mdCalls)
            //{
            //    MethodDefinition calleeM = FindMatchedMd(call);
            //    if (calleeM == null) { continue; }
            //    if (!CG.ContainsMethod(calleeM) || !CG.ContainsMethod(method))
            //    {
            //        Console.WriteLine("ERROR: Did not find the callee in the call graph");
            //        throw new Exception("Did not find the callee");
            //    }
            //    CG.AddCalleeEdge(method, calleeM);
            //    CG.AddCallerEdge(method, calleeM);
            //}

            return calleeMs.ToList();
        }
    }
}
