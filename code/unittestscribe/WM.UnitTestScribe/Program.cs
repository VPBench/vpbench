﻿using System.Configuration;
using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABB.Swum;
using ABB.Swum.Nodes;
using Antlr3.ST;
using CommandLine.Text;
using WM.UnitTestScribe.Summary;
using WM.UnitTestScribe.TestCaseDetector;
using ABB.SrcML.Data;
using TeaCap.TestPropagator;
using TeaCap.GitMining;
using System.Threading;
using System.Globalization;
using System.Xml.Linq;
using ABB.SrcML;
using WM.UnitTestScribe.CallGraphNS;
using System.Xml;
using System.Xml.XPath;

namespace WM.UnitTestScribe {
    public class Program {
        //Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
        
        /// <summary> Subject application location </summary>
        //public static readonly string LocalProj = @"D:\Research\myTestSubjects\Callgraph\CallgraphSubject";
        //public static readonly string LocalProj = @"D:\Research\Subjects\google-api-dotnet-client-master";
        //public static readonly string LocalProj = @"D:\Research\Subjects\Sando-master";
        public static readonly string LocalProj = ConfigurationManager.AppSettings["sourceProjectFolder"];
        public static readonly string targetProject = ConfigurationManager.AppSettings["targetProjectFolder"];
        
        //public static readonly string LocalProj = @"D:\Research\Subjects\Glimpse-master";
        /// <summary> SrcML directory location </summary>
        public static readonly string SrcmlLoc = ConfigurationManager.AppSettings["srcMLexeFolder"];
        public static readonly string outputLoc = ConfigurationManager.AppSettings["srcMLCallGraphOutputFolder"];

        public static readonly string testcaseOutput = "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases\\testcases.txt";
        public static readonly string testcaseExtract = "D:\\Dokumente\\_Studium\\masterarbeit\\entwicklung\\TestcaseExtraction\\Test\\GetTestcases";


        /// <summary>
        /// Command line testing
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {
            DateTime dt = DateTime.Now;
            //args = new string[] { "fid_testcase_save", "--loc", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\testcaseextractions\\structurizr\\structurizr-client", "--srcmlPath", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\privunittestscribe\\SrcML", "--outputLoc", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\testcaseextractions\\structurizr\\structurizr-client\\idtestcases.txt" };
            //args = new string[] { "fid_testcase_save", "--loc", "D:\\Dokumente\\Promotion\\vpbench\\projects\\LMAXDisruptor\\disruptor\\src\\test\\java", "--srcmlPath", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\privunittestscribe\\SrcML", "--outputLoc", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\testcaseextractions\\disruptor\\idtestcases.txt" };
            //args = new string[] { "fid_testcase_save", "--loc", "D:\\Dokumente\\Promotion\\vpbench\\projects\\Structurizr\\java\\structurizr-client\\test\\unit", "--srcmlPath", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\privunittestscribe\\SrcML", "--outputLoc", "D:\\Dokumente\\Promotion\\vpbench\\unittestscribe\\testcaseextractions\\structurizr\\structurizr-client\\idtestcases.txt" };


            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.api", "--class", "HashBasedMessageAuthenticationCodeTests", "--test", "test_generate" };
            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.api", "--class", "StructurizrClientTests", "--test", "test_getWorkspace_ThrowsAnException_WhenTheWorkspaceIdIsNotValid" };
            //args = new string[] { "extractTestcase", "--loc", LocalProj, "--srcmlPath", SrcmlLoc, "--outputLoc", testcaseExtract, "--namespace", "com.structurizr.encryption", "--class", "AesEncryptionStrategyTests", "--test", "test_encrypt_EncryptsPlaintext" };

            var options = new Options();
            string invokedVerb = null;
            object invokedVerbOptions = null;


            if (!CommandLine.Parser.Default.ParseArguments(args, options,
                (verb, verbOptions) => {
                    invokedVerb = verb;
                    invokedVerbOptions = verbOptions;
                })) {
                Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
            }
            if (invokedVerb == "callgraph") {
                var callGraphOp = (CallgraphOptions)invokedVerbOptions;
                var methodsOfInterest = callGraphOp.MethodsOfInterestString != "" ? callGraphOp.MethodsOfInterestString.Split(',').AsEnumerable() : null;
                var generator = new InvokeCallGraphGenerator(callGraphOp.LocationsPath, callGraphOp.SrcMLPath, callGraphOp.OutputPath, methodsOfInterest);
                generator.run();
            } else if (invokedVerb == "testcases") {
                var testCaseOp = (TestCaseDetectOptions)invokedVerbOptions;
                var detector = new TestCaseDetector.TestCaseDetector(testCaseOp.LocationsPath, testCaseOp.SrcMLPath);
                detector.AnalyzeTestCases();
                Console.WriteLine("print testcases");
                foreach (var testCaseId in detector.AllTestCases) {
                    Console.WriteLine(testCaseId.NamespaceName + "  "+ testCaseId.ClassName + "  " + testCaseId.MethodName);
                }
            }
            else if (invokedVerb == "fid_testcase_save")
            {
                var testCaseOp = (SaveIdTestCaseDetectOptions)invokedVerbOptions;
                var detector = new TestCaseDetector.TestCaseDetector(testCaseOp.LocationsPath, testCaseOp.SrcMLPath);
                detector.AnalyzeTestCases();
                Console.WriteLine("print testcases");
                var identifiedTests = detector.AllTestCases.Where(tc => tc.NamespaceName != String.Empty && tc.ClassName != String.Empty && tc.MethodName != String.Empty);
                var idTestStrings = identifiedTests
                    .Select(idt => idt.NamespaceName + "  " + idt.ClassName + "  " + idt.MethodName);
                
                File.WriteAllLines(testCaseOp.OutputLoc, idTestStrings);
            }
            else if (invokedVerb == "extractTestcase")
            {
                var extractionOp = (TestcaseExtractOptions)invokedVerbOptions;
                var fullTestPath = extractionOp.Namespace + "." + extractionOp.ClassName + "." + extractionOp.TestName;
                
                if (!Directory.Exists(extractionOp.OutputLoc))
                {
                    Directory.CreateDirectory(extractionOp.OutputLoc);
                }

                var fileName = Path.Combine(extractionOp.LocationsPath, extractionOp.Namespace.Replace('.', '\\'), extractionOp.ClassName + ".java");
                var xmlOutput = Path.Combine(extractionOp.OutputLoc, "tmpxmloutput.xml");
                var xmlFilterOutput = Path.Combine(extractionOp.OutputLoc, "tmpxmloutput2.xml");
                var src2srcmlexe = Path.Combine(extractionOp.SrcMLPath, "src2srcml.exe");
                var srcml2srcexe = Path.Combine(extractionOp.SrcMLPath, "srcml2src.exe");

                Src2XML sx = new Src2XML();
                XML2Src xs = new XML2Src();
                sx.SourceFileToXml(fileName, xmlOutput, src2srcmlexe);
                Thread.Sleep(2000);

                var docNav = new XPathDocument(xmlOutput, XmlSpace.Preserve);
                var nav = docNav.CreateNavigator();
                var namespaceManager = new XmlNamespaceManager(new NameTable());
                namespaceManager.AddNamespace("src", "http://www.sdml.info/srcML/src");

                // Get Functions' Prefix for later Callgraph Calculation
                //var packageName = nav.SelectSingleNode("//src:package/src:name").Value;
                var functionPrefix = extractionOp.Namespace + "." + extractionOp.ClassName;
                // Currently assuming that package maps to directory structure 1-to-1 and class name maps to filename 1-to-1

                var generalCallgraphInsertionPoints = new List<string>();
                generalCallgraphInsertionPoints.Add(functionPrefix + "." + extractionOp.TestName);

                // Get Import statements
                var importNodes = nav.Select("/src:unit/src:import", namespaceManager);
                var importStatements = new List<string>();
                while (importNodes.MoveNext()) {
                    importStatements.Add(importNodes.Current.Value);
                }
                var importString = String.Join("", importStatements);

                // Write Import to file
                var importExtractFile = Path.Combine(extractionOp.OutputLoc, "imports.java");
                File.WriteAllText(importExtractFile, importString);

                // Get Testcase itself
                string strExpressionTestcase = String.Format("/src:unit//src:function[src:name='{0}']/src:block", extractionOp.TestName);
                var testcaseClosure = nav.SelectSingleNode(strExpressionTestcase, namespaceManager).Value;
                var testcaseContent = testcaseClosure.Substring(1, testcaseClosure.Length - 2);

                // Write Testcase to file
                var testcaseExtractFile = Path.Combine(extractionOp.OutputLoc, "testcase.java");
                File.WriteAllText(testcaseExtractFile, testcaseContent);

                // Get Annotated Functions
                string strExpressionAnnotationSearch = "/src:unit//src:function[src:type/src:annotation[src:name='{0}']]";

                // Get Before function
                var beforeNode = nav.SelectSingleNode(String.Format(strExpressionAnnotationSearch, "Before"), namespaceManager);
                if (beforeNode != null) 
                {
                    var beforeResult = Util.ExtractMethod(beforeNode, namespaceManager, false);
                    var beforeExtractFile = Path.Combine(extractionOp.OutputLoc, String.Format("before_{0}.java", beforeResult.Item1));

                    generalCallgraphInsertionPoints.Add(beforeResult.Item1);
                    File.WriteAllText(beforeExtractFile, beforeResult.Item2);
                }

                // Get BeforeEach function
                var beforeEachNode = nav.SelectSingleNode(String.Format(strExpressionAnnotationSearch, "BeforeEach"), namespaceManager);
                if (beforeEachNode != null)
                {
                    var result = Util.ExtractMethod(beforeEachNode, namespaceManager, false);
                    var beforeEachExtractFile = Path.Combine(extractionOp.OutputLoc, String.Format("beforeEach_{0}.java", result.Item1));

                    generalCallgraphInsertionPoints.Add(result.Item1);
                    File.WriteAllText(beforeEachExtractFile, result.Item2);
                }
                //var beforeEachNode = Util.SelectSingleElement(nav, String.Format(strExpressionAnnotationSearch, "BeforeEach"), namespaceManager);
                //var testcaseContent = testcaseClosure.Substring(1, testcaseClosure.Length - 2);

                // Get BeforeAll function
                var beforeAllNode = nav.SelectSingleNode(String.Format(strExpressionAnnotationSearch, "BeforeAll"), namespaceManager);
                if (beforeAllNode != null)
                {
                    var result = Util.ExtractMethod(beforeAllNode, namespaceManager, false);
                    var beforeAllExtractFile = Path.Combine(extractionOp.OutputLoc, String.Format("beforeAll_{0}.java", result.Item1));

                    generalCallgraphInsertionPoints.Add(result.Item1);
                    File.WriteAllText(beforeAllExtractFile, result.Item2);
                }

                //// Get BeforeClass function
                var beforeClassNode = nav.SelectSingleNode(String.Format(strExpressionAnnotationSearch, "BeforeClass"), namespaceManager);
                if (beforeClassNode != null)
                {
                    var result = Util.ExtractMethod(beforeClassNode, namespaceManager, false);
                    var beforeClassExtractFile = Path.Combine(extractionOp.OutputLoc, String.Format("beforeClass_{0}.java", result.Item1));

                    generalCallgraphInsertionPoints.Add(result.Item1);
                    File.WriteAllText(beforeClassExtractFile, result.Item2);
                }

                // Get Constructor
                var constructorNode = nav.SelectSingleNode("/src:unit/src:class/src:block/src:constructor", namespaceManager);
                if (constructorNode != null)
                {
                    var result = Util.ExtractMethod(constructorNode, namespaceManager, false);
                    var constructorExtractFile = Path.Combine(extractionOp.OutputLoc, "constructor.java");

                    generalCallgraphInsertionPoints.Add(result.Item1);
                    File.WriteAllText(constructorExtractFile, result.Item2);
                }

                // Get Local Attributes
                var attributeNodes = nav.Select("/src:unit//src:class/src:block/src:decl_stmt", namespaceManager);
                var attributes = new List<string>();
                while (attributeNodes.MoveNext())
                {
                    attributes.Add(attributeNodes.Current.Value);
                    var attDefMethodCallsIt = attributeNodes.Current.Select(".//src:call", namespaceManager);
                    while (attDefMethodCallsIt.MoveNext()) 
                    {
                        // In progress (needs further work, as name needs to be resolved with its namespace)
                        generalCallgraphInsertionPoints.Add(attDefMethodCallsIt.Current.SelectSingleNode("./src:name", namespaceManager).Value);
                    }
                }

                // If Attributes exist -> Write them to file
                if (attributes.Count > 0) 
                {
                    var attributeString = String.Join("\n", attributes);
                    var attributeExtractFile = Path.Combine(extractionOp.OutputLoc, "attributes.java");
                    File.WriteAllText(attributeExtractFile, attributeString);
                }

                // Get Local Functions

                // Get Superclass -> priority lower for now, as resolving the superclass is nontrivial
                // var superclassName = nav.SelectSingleNode("/src:unit//src:class//src:extends/src:name", namespaceManager)?.Value;
                return;
            }
            else if (invokedVerb == "summary") {
                var SummaryOp = (SummarizeTestOptions)invokedVerbOptions;
                //ProjectCloner projectCloner = new ProjectCloner(SrcmlLoc);
                //projectCloner.cloneRepos();

                DataAnalyzer dataAnalyzer = new DataAnalyzer();
                dataAnalyzer.CountEcoSystems();
                //TestPropagator testPropagator = new TestPropagator(LocalProj, targetProject, SrcmlLoc);
                //testPropagator.propagate();
                //var summary = new SummaryGenerator(SummaryOp.LocationsPath, SummaryOp.SrcMLPath);
                //Console.WriteLine("This is summary");
                //summary.AnalyzeSummary();
                //summary.GenerateSummary(SummaryOp.OutputLoc);
                //Console.WriteLine("Done!!!!!!  Thanks.");

            } else if (invokedVerb == "hello") {

                Console.WriteLine("Hello");
                
                //string proPath = @"C:\Users\boyang.li@us.abb.com\Documents\RunningTest\Input\ConsoleApplication1";
                //string proPath = @"C:\Users\boyang.li@us.abb.com\Documents\RunningTest\Input\SrcML\ABB.SrcML";
                using (var project = new DataProject<CompleteWorkingSet>(LocalProj, LocalProj, SrcmlLoc))
                {
                    project.Update();
                    NamespaceDefinition globalNamespace;
                    project.WorkingSet.TryObtainReadLock(5000, out globalNamespace);
                    try
                    {

                        // Step 1.   Build the call graph
                        Console.WriteLine("{0,10:N0} files", project.Data.GetFiles().Count());
                        Console.WriteLine("{0,10:N0} namespaces", globalNamespace.GetDescendants<NamespaceDefinition>().Count());
                        Console.WriteLine("{0,10:N0} types", globalNamespace.GetDescendants<TypeDefinition>().Count());
                        Console.WriteLine("{0,10:N0} methods", globalNamespace.GetDescendants<MethodDefinition>().Count());
                        Console.ReadLine();
                        var methods = globalNamespace.GetDescendants<MethodDefinition>();

                        // Step 2.   Testing
                        Console.WriteLine("======  test 1 ========= ");
                        foreach (MethodDefinition m in methods)
                        {
                            Console.WriteLine("Method Name : {0}", m.GetFullName());

                        }
                       
                    }
                    finally
                    {
                        project.WorkingSet.ReleaseReadLock();
                    }


                }
              

                Console.ReadLine();
                Console.WriteLine("print hello");

            }
            TimeSpan ts = DateTime.Now - dt;
            Console.WriteLine(ts.ToString());
            Console.ReadLine();
        }

        // TODO: Figure out how options work in C#, especially with these Verbs and InvokedVerbs
        private class Options {
            [VerbOption("callgraph", HelpText = "Analyze stereotype of methods in the project")]
            public CallgraphOptions StereotypeVerb { get; set; }

            [VerbOption("testcases", HelpText = "find all test cases in a project")]
            public TestCaseDetectOptions FindAllTestCaseVerb { get; set; }

            [VerbOption("fid_testcase_save", HelpText = "find and save all test cases in a project")]
            public SaveIdTestCaseDetectOptions SaveAllIdTestCaseVerb { get; set; }

            [VerbOption("extractTestcase", HelpText = "extract a given testcase")]
            public TestcaseExtractOptions ExtractTestCaseVerb { get; set; }

            [VerbOption("summary", HelpText = "summarize test cases in a project")]
            public SummarizeTestOptions SummaryTestCaseVerb { get; set; }

            
            [VerbOption("hello", HelpText = "Print hello for testing")]
            public HelloOptions HelloVerb { get; set; }

            [HelpVerbOption]
            public string GetUsage(string verb) {
                return HelpText.AutoBuild(this, verb);
            }
        }




        /// <summary>
        /// Stereotype detector
        /// </summary>
        private class CallgraphOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }

            [Option("methodsOfInterest", Required = false, HelpText = "The full names for the methods of interest to slice the callgraph, separated by ','", DefaultValue = "")]
            public string MethodsOfInterestString { get; set; }

            [Option("outputPath", Required = true, HelpText = "The path to the output file")]
            public string OutputPath { get; set; }
        }



        /// <summary>
        /// Options for findAllTestCases
        /// </summary>
        private class TestCaseDetectOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }
        }


        /// <summary>
        /// Options for findAndSaveAllIdTestCases
        /// </summary>
        private class SaveIdTestCaseDetectOptions
        {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }

            [Option("outputLoc", Required = true, HelpText = "The path to the .txt-file, where to store the identified testcases")]
            public string OutputLoc { get; set; }
        }

        /// <summary>
        /// Options for extractTestCase
        /// </summary>
        private class TestcaseExtractOptions
        {
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }

            [Option("outputLoc", Required = true, HelpText = "The path, where to store the extracted testcase")]
            public string OutputLoc { get; set; }

            [Option("namespace", Required = true)]
            public string Namespace { get; set; }

            // ClassName due to Location fields, which can get the filename
            [Option("class", Required = true)]
            public string ClassName { get; set; }

            [Option("test", Required = true)]
            public string TestName { get; set; }
        }

        /// <summary>
        /// Stereotype detector
        /// </summary>
private class SummarizeTestOptions {
            /// <summary> Subject application location </summary>
            [Option("loc", Required = true, HelpText = "The subject project folder")]
            public string LocationsPath { get; set; }

            [Option("srcmlPath", Required = true, HelpText = "The path to Srcml.exe")]
            public string SrcMLPath { get; set; }



            [Option("outputLoc", Required = true, HelpText = "Summary Output location")]
            public string OutputLoc { get; set; }
        }
      


        /// <summary>
        /// print hello for testing 
        /// </summary>
        private class HelloOptions {
        }
    }

}
