﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using ABB.SrcML.Data;

namespace WM.UnitTestScribe {
    class Util {

        /// <summary>
        /// Creates and returns a tempral directory
        /// </summary>
        /// <returns></returns>
        public static string CreateTemporalDir() {
            var tmpDir = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tmpDir);
            return tmpDir;
        }
        public static string CreateTemporalDir(string directory)
        {
            var tmpDir = directory;
            if (Directory.Exists(directory)) {
                DeleteDirectory(tmpDir);
            }
            Directory.CreateDirectory(tmpDir);
            return tmpDir;
        }

        /// <summary>
        /// Deletes a directory's contents
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteDirectory(string path)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(path);
            foreach (FileInfo file in di.EnumerateFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.EnumerateDirectories())
            {
                dir.Delete(true);
            }
        }

        /// <summary>
        /// Writes content to the given file
        /// </summary>
        /// <param name="content"></param>
        /// <param name="filePath"></param>
        public static void WriteToFile(string content, string filePath) {
            using (StreamWriter sw = new StreamWriter(filePath)) {
                sw.WriteLine(content);
            }
        }



        private static Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
        public static string RandomString(int size) {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++) {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static bool IsFileInUse(string path)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentException("'path' cannot be null or empty.", "path");

            try
            {
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read)) { }
            }
            catch (IOException)
            {
                return true;
            }

            return false;
        }

        public static string ExtractStatementFromFile(string filePath, int startingLine, int startingColumn, int endingLine, int endingColumn)
        {
            if (endingLine == int.MaxValue || endingColumn == int.MaxValue)
            {
                throw new ArgumentException("Ending line or column is not parsed correctly. Test cannot get extracted.");
            }

            // Column indexing begins with index 1 -> as string operations use indexing starting with 0 -> subtract 1 where necessary
            var indexedLines = File.ReadAllLines(filePath).Zip(Enumerable.Range(1, int.MaxValue), (line, index) => (line, index));
            var relevantLines = indexedLines.SkipWhile(indLine => indLine.index < startingLine)
                .TakeWhile(indLine => indLine.index <= endingLine)
                .Select(tuple => tuple.line)
                .ToArray<string>();

            var tailLine = relevantLines
                .Last()
                .Replace("\t", "        ");
            var trimmedTail = tailLine
                .Substring(0, endingColumn - 1);
            relevantLines[relevantLines.Length - 1] = trimmedTail;

            var headLine = relevantLines
                .First()
                .Replace("\t", "        ");
            var trimmedHead = headLine
                .Substring(startingColumn - 1);
            relevantLines[0] = trimmedHead;
 
            return String.Join("\n", relevantLines);
        }

        public static IEnumerable<Statement> GetChildStatementsRecursively(Statement statement)
        {
            // Does not get every sub statement, e.g. catch statement in try statements
            var recursiveChildStatements = statement.ChildStatements
                .SelectMany(st => GetChildStatementsRecursively(st))
                .ToList();

            recursiveChildStatements.Add(statement);
            return recursiveChildStatements;
        }

        public static Tuple<string, string> ExtractMethod(XPathNavigator node, IXmlNamespaceResolver namespaceManager, bool asFunction = false) {
            var methodName = node.SelectSingleNode("./src:name", namespaceManager).Value;

            if (!asFunction)
            {
                var methodClosure = node.SelectSingleNode("./src:block", namespaceManager).Value;
                var methodBody = methodClosure.Substring(1, methodClosure.Length - 2);
                return new Tuple<string, string>(methodName, methodBody);
            }
            else 
            {
                throw new NotImplementedException("Not yet supported.");
            }
        }

        //public static XPathNavigator SelectSingleElement(XPathNavigator nav, string strExpression, XmlNamespaceManager namespaceManager) {
        //    var NodeIter = nav.Select(strExpression, namespaceManager);

        //    if (NodeIter.Count != 1)
        //    {
        //        throw new Exception("Unexcepted XPath result when extracting the testcase method block");
        //    }

        //    NodeIter.MoveNext();
        //    return NodeIter.Current;
        //}
    }
}
