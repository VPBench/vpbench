# Setup for donor projects #

Description of used repository versions and performed preprocessing steps.

* Algorithms
	* located in donor_repos.zip/Algorithms/Algorithms
	* cloned from https://github.com/williamfiset/Algorithms
	* checked out commit: a8df87a49622423ba1457dd615ded1c6842fa037
	* preprocessing: removed multi-line comments from test source set (src/test/java)
	* run: gradle jar compileTestJava
* Disruptor
	* located in donor_repos.zip/LMAXDisruptor/disruptor
	* cloned from https://github.com/LMAX-Exchange/disruptor
	* checked out commit: d50135a6980d86a0d59d750036e86fe7be29c163
	* preprocessing: removed multi-line comments from test source set (src/test/java)
	* run: gradle jar compileTestJava
* Jadx
	* located in donor_repos.zip/skylot/jadx
	* cloned from https://github.com/skylot/jadx
	* checked out commit: c28e8142f479fd7785749ca934cabda2fee4605b
	* preprocessing: removed multi-line comments from test source set (jadx-cli/src/test/java;jadx-core/src/test/java;jadx-gui/src/test/java;jadx-plugins/jadx-dex-input/src/test/java)
	* run: gradle jar compileTestJava
* Structurizr
	* located in donor_repos.zip/Structurizr/java
	* cloned from https://github.com/structurizr/java
	* checked out commit: bd48c2ab1a04b9f6741b582d9be4dc7554a50ff2
	* preprocessing: 
		* removed multi-line comments from test source set (structurizr-client/test/unit;structurizr-core/test/unit) -> none
		* removed wrapper task from build.gradle: check structurizr_preprocessing.txt for the detailed delta
	* run: gradle jar compileTestJava

We performed the preprocessing using the search and replace functionality of eclipse. We used the search RegEx: "\/\\\*([\\S\\s]\+?)\\\*\\/"
We deleted the .git-folders inside the folders contained in here.

### Note: ###
This also removed parts of the contents of some strings inside the Jadx project. This includes two cases removing access to a variable and a function. This does however not impact our evaluation and research results.
We list those two cases in jadx_preprocessing.txt (both ocurring in the same file)