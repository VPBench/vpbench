package vp.bench.calculatorexample.calculator;

public class AdvancedCalculator {
	public double power(int a, int b) {
		double result = Math.pow(a,b);
		return result;
	}
	
	public int factorialize(int num) {
		int result = 1;
		for (int i = 1; i <= num; i++) {
			result = result * i;
		}		
		return result;
	}
	
	public int modulus(int a, int b) {
		int result = a % b;
		return result;
	}
}