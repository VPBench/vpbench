package vp.bench.calculatorexample.calculator;

public class BasicCalculator {	
	public int add(int a, int b) {
		int result = a + b;
		return result;
	}
	
	public int subtract(int a, int b) {
		int result = a - b;
		return result;
	}
	
	public int multiply(int a, int b) {
		int result = a * b;
		return result;
	}
	
	public float divide(float a, float b) {
		float result = a / b;
		return result;
	}
}