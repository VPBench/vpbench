package vp.bench.calculatorexample.calculator;

public class Main {	
	public static void main(String[] args){
		System.out.println("Begin execution.");		
		BasicCalculator basicCalc = new BasicCalculator();
		AdvancedCalculator advCalc = new AdvancedCalculator();

		int addResult = basicCalc.add(1,2);
		System.out.println(addResult);

		int subResult = basicCalc.subtract(3,4);
		System.out.println(subResult);

		int mulResult = basicCalc.multiply(5,6);
		System.out.println(mulResult);

		float divResult = basicCalc.divide(7,8);
		System.out.println(divResult);

		double powResult = advCalc.power(4,2);
		System.out.println(powResult);

		int facResult1 = advCalc.factorialize(0);
		int facResult2 = advCalc.factorialize(5);
		System.out.println(facResult1);
		System.out.println(facResult2);

		int modResult = advCalc.modulus(5,2);
		System.out.println(modResult);

		System.out.println("Everything was executed");
	}
}