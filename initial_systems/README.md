# Initial projects #

Description of used repository versions and performed preprocessing steps.

* CalculatorRoot: implemented ourselves
* JSON-javaRoot:
	* cloned from https://github.com/stleary/JSON-java
	* checked out commit: 50d619698f1c05c32014981371f9f82cc520b07e
	* preprocessing: check preprocessing_json_java.txt
* RxJava:
	* cloned from https://github.com/ReactiveX/RxJava
	* checked out commit: adc3a5be7ab276297a2907f3e2f1581fb5b4179f
	* preprocessing: check preprocessing_rxjava.txt
* VanillaTestbed: implemented ourselves